#!/bin/bash
#
# Update a rule
#
# 1 - Rule ID 
# 2 - Rule Name
# 3 - Rule Description
# 4 - Rule Text

if `test -z "$3"`; then
	echo "USAGE: updateRule.sh RULE_ID RULE_NAME RULE_DESCRIPTION RULE_TEXT"
	exit 1
fi

RULE_ID=$1

RULE_NAME=`python -c "import sys, urllib as ul; print ul.quote_plus(sys.argv[1])" "$2"`

RULE_DESC=`python -c "import sys, urllib as ul; print ul.quote_plus(sys.argv[1])" "$3"`

RULE_TEXT=`python -c "import sys, urllib as ul; print ul.quote_plus(sys.argv[1])" "$4"`

wget -qO- --header "Content-Type: application/json" --post-data "{ \"ID\" : \"$RULE_ID\", \"ruleName\": \"$RULE_NAME\", \"ruleText\": \"$RULE_TEXT\", \"ruleDesc\": \"$RULE_DESC\" }" http://localhost:8084/api/rules/update

