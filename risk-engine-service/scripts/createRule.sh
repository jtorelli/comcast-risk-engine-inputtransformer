#!/bin/bash
#
# Create a rule
# 
# 1 - Rule Name
# 2 - Rule Description
# 3 - Rule Text

if `test -z "$3"`; then
	echo "USAGE: createRule.sh RULE_NAME RULE_DESCRIPTION RULE_TEXT"
	exit 1
fi

RULE_NAME=`python -c "import sys, urllib as ul; print ul.quote_plus(sys.argv[1])" "$1"`

RULE_DESC=`python -c "import sys, urllib as ul; print ul.quote_plus(sys.argv[1])" "$2"`

RULE_TEXT=`python -c "import sys, urllib as ul; print ul.quote_plus(sys.argv[1])" "$3"`

# TODO FIXME
wget -qO- --header "Content-Type: application/json" --post-data "{ \"ruleInputParameters\":\" \", \"ruleName\": \"$RULE_NAME\", \"ruleText\": \"$RULE_TEXT\", \"ruleDesc\": \"$RULE_DESC\" }" http://localhost:8084/api/rules

