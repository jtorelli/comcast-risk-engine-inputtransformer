#!/bin/bash
#
# Create metadata for a rule
# 
# 1 - Rule ID
# 2 - Key
# 3 - Value

if `test -z "$3"`; then
	echo "USAGE: createMetaData.sh RULE_ID KEY VALUE"
	exit 1
fi

RULE_ID=`python -c "import sys, urllib as ul; print ul.quote_plus(sys.argv[1])" "$1"`

KEY=`python -c "import sys, urllib as ul; print ul.quote_plus(sys.argv[1])" "$2"`

VALUE=`python -c "import sys, urllib as ul; print ul.quote_plus(sys.argv[1])" "$3"`

wget -qO- --header "Content-Type: application/json" --post-data "{ \"rule_id\": \"$RULE_ID\", \"key\": \"$KEY\", \"value\": \"$VALUE\" }" http://localhost:8084/api/metadata/create

