/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intersourcellc.riskengine.Utilities;
//import com.intersourcellc.riskengine.Utilities.MockWebApplicationContextLoader;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;

/**
 *
 * @author uadmin
 */
@ContextConfiguration(
        locations={"classpath:/TestSpringConfiguration/applicationContext.xml",
								 "classpath:/TestSpringConfiguration/applicationContext-security.xml",
								 "classpath:/TestSpringConfiguration/appServlet/servlet-context.xml"},
		loader=MockWebApplicationContextLoader.class)

@MockWebApplication(name="MockWebApplication")
public class RiskEngineTestNgBase extends AbstractTestNGSpringContextTests{
    
    //Empty test base class to load application context
}
