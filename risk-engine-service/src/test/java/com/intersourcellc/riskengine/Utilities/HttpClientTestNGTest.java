/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intersourcellc.riskengine.Utilities;

import static org.testng.Assert.*;
import org.springframework.beans.factory.annotation.Autowired;
import com.intersourcellc.riskengine.utilities.HTTPClient;
import java.util.ArrayList;
import java.util.List;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author davis
 */
public class HttpClientTestNGTest extends RiskEngineTestNgBase{

    @Autowired
    private HTTPClient httpClient;
    
    public HttpClientTestNGTest() {
    }

    
    @Test (enabled = true)
    public void testSendPost() {
        com.intersourcellc.Header header = new com.intersourcellc.Header();
        List<com.intersourcellc.Header> headers = new ArrayList();
        header.addParameter("authuser", "authuser", "somelogin");
        header.addParameter("authpass", "authpass", "password");
        headers.add(header);
        String url = "http://httpbin.org/post";
        String postBody = "{ \"result\": [\n  ]}";
        String testReult = testSendPostImpl(url, headers, postBody);
    }

    private String testSendPostImpl(String url, List<com.intersourcellc.Header> headers, String postBody){
        logger.debug(Self.methodName());
        String result = httpClient.sendPost(url, headers, postBody);
        assertNotEquals(result, null); // adding more result check later
        logger.debug(result);
        return result;
    }
    
    @Test (enabled = true)
    public void testSendPostHTTPS() {
        com.intersourcellc.Header header = new com.intersourcellc.Header();
        List<com.intersourcellc.Header> headers = new ArrayList();
        header.addParameter("authuser", "authuser", "somelogin");
        header.addParameter("authpass", "authpass", "fakepassword");
        headers.add(header);
        String url = "https://httpbin.org/post";
        String postBody = "{ \"result\": [\n  ]}";
        String testReult = testSendPostImpl(url, headers, postBody);
    }
        
    /**
    * @return the httpClient
    */
    public HTTPClient getHttpClient() {
        return httpClient;
    }

    /**
     * @param httpClient the rules to set
     */
    public void setHttpClient(HTTPClient httpClient) {
        this.httpClient = httpClient;
    }
    
    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }
    
    
}