/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intersourcellc.riskengine.Test.engines;

import com.intersourcellc.riskengine.Utilities.RiskEngineTestNgBase;
import com.intersourcellc.riskengine.Utilities.Self;
import com.intersourcellc.riskengine.controllers.RulesController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotEquals;

/**
 *
 * @author uadmin
 */
public class RiskEngineRuleExecutorNGTest extends RiskEngineTestNgBase{
    
    @Autowired
    private RulesController rulesController;
    /**
     * Test of executeRule method, of class RiskEngineRuleExecutor.
     */
    @Test(enabled = false) // FIXME: Cannot have hard dependency on database data (rmb)
    public void testExecuteRule () throws Exception{
    
     //   testExecuteRuleImpl ( 1L);
        // testExecuteRuleImpl ( 1002L);
        // testExecuteRuleImpl ( 1003L);   
    }
    
   /* private void testExecuteRuleImpl(Long ruleId ) throws Exception {
        logger.debug(Self.methodName());
        
       MockHttpServletRequest request = new MockHttpServletRequest("GET", "/rules/execute");
       request.addParameter("ID", ruleId.toString());
        
        Object result = rulesController.executeRules(ruleId, request);
        assertNotEquals(result, null); // adding more result check later
    
    }
    */
   
    /**
    * @return the rulesController
    */
    public RulesController getRulesController() {
        return rulesController;
    }

    /**
     * @param rulesController the rules to set
     */
    public void setRulesController(RulesController rulesController) {
        this.rulesController = rulesController;
    }
}