/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intersourcellc.riskengine.Test.Controllers;

import com.intersourcellc.riskengine.Utilities.MockWebApplication;
import com.intersourcellc.riskengine.Utilities.Self;
import com.intersourcellc.riskengine.controllers.RulesController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerAdapter;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

/**
 *
 * @author Admin
 */
//@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/TestSpringConfiguration/applicationContext.xml",
                                    "classpath:/TestSpringConfiguration/applicationContext-security.xml",
                                    "classpath:/TestSpringConfiguration/apiServlet/servlet-context.xml",
                                    "classpath:/TestSpringConfiguration/appServlet/servlet-context.xml"})
@MockWebApplication(name="MockWebApplication")

public class DispatcherServletTest extends AbstractTestNGSpringContextTests
{
    @Autowired
    private RulesController rulesController;
    @Test(enabled=false)
    //@Transactional
    public void testRuleControllerGetAll() throws Exception {
        logger.debug(Self.methodName());
        MockHttpServletRequest request = new MockHttpServletRequest("GET", "/api/rules");
        MockHttpServletResponse response = new MockHttpServletResponse();
        AnnotationMethodHandlerAdapter handlerAdpt = new AnnotationMethodHandlerAdapter();
        //request.setRequestURI("/rules");
        ModelAndView mav = handlerAdpt.handle(request, response, this.rulesController);
        String viewname = mav.getViewName();
        assertEquals("Incorrect view name returned", "myexpectedviewname", viewname );
    }
        /**
     * @return the rulesController
     */
    public RulesController getRulesController() {
       return rulesController;
    }

    /**
     * @param rulesController the rules to set
     */
    public void setRulesController(RulesController rulesController) {
       this.rulesController = rulesController;
    }
}