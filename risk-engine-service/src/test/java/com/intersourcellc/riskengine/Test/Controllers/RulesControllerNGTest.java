/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intersourcellc.riskengine.Test.Controllers;

import com.intersourcellc.riskengine.Utilities.Self;
import com.intersourcellc.riskengine.Utilities.RiskEngineTestNgBase;
import com.intersourcellc.riskengine.models.WebServiceResponse;
import com.intersourcellc.riskengine.repositories.Rules;
import java.util.List;
import org.springframework.test.context.ContextConfiguration;

import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.springframework.beans.factory.annotation.Autowired;
import com.intersourcellc.riskengine.controllers.RulesController;
import java.util.Map;
import java.util.HashMap;
/*
 * @author Admin
 */
public class RulesControllerNGTest extends RiskEngineTestNgBase{

    @Autowired
    private Rules rules;

    @Autowired
    private RulesController rulesController;

    public RulesControllerNGTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }

    /**
     * Test of findAll method, of class RulesController.
     */
    @Test(enabled = true) // FIXME: Cannot have hard dependency on database (rmb)
    public void testFindAll() {
        logger.debug(Self.methodName());
        List expResult = null;
        RulesController controller = new RulesController();
        controller.setRules ( rules); 
        List result = controller.findAll();
        assertNotEquals(result, expResult);
    }
    @Test(enabled = true) // FIXME: Cannot have hard dependency on database (rmb)
    public void testFindAll2() {
        logger.debug(Self.methodName());
        List expResult = null;
        //RulesController controller = new RulesController();
        //controller.setRules ( rules); 
        List result = rulesController.findAll();
        assertNotEquals(result, expResult);
    }
    /**
     * Test of create method, of class RulesController.
     */
    @Test(enabled = false) // FIXME: Cannot have hard dependency on database (rmb)
    public void testCreate() {
        logger.debug(Self.methodName());
        Map<String, String> request = new HashMap< >(); 
        request.put( "ruleName","RuleNameTest12");
        request.put ("ruleDescription","RuleDescriptionTest");
        request.put ( "ruleText", "RuleTextText");
        request.put ("ruleInputParameters", "RuleImputparammetersText");
        WebServiceResponse expResult = null;
        RulesController controller = new RulesController();
        controller.setRules ( rules); 
        try
        {
            Map<String, String> result = controller.create( request);
            assertNotEquals(result, expResult);
            controller.deleteById(Long.parseLong( result.get("ID"), 10));
        }
        catch(Exception e)
        {
          fail("RuleController Create Test failed. Error:" + e.toString());    
        }
                
    }       

    /**
     * @return the rules
     */
    public Rules getRules() {
        return rules;
    }

    /**
     * @param rules the rules to set
     */
    public void setRules(Rules rules) {
        this.rules = rules;
    }
    /**
     * @return the rulesController
     */
    public RulesController getRulesController() {
        return rulesController;
    }

    /**
     * @param rulesController the rules to set
     */
    public void setRulesController(RulesController rulesController) {
        this.rulesController = rulesController;
    }
}