package com.intersourcellc.riskengine.controllers;

import com.intersourcellc.Utility;
import com.intersourcellc.riskengine.engines.RiskEngineRuleExecutor;
import com.intersourcellc.riskengine.models.Rule;
import com.intersourcellc.riskengine.models.RuleTest;
import com.intersourcellc.riskengine.models.WebServiceResponse;
import com.intersourcellc.riskengine.repositories.*;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

/**
 * Created by Ryan on 7/31/13.
 */
public class RulesControllerTest {
    final static Logger logger = LoggerFactory.getLogger(RuleTest.class);

    @Mock private RiskEngineRuleExecutor mockedRulesEngine;
    @Mock private Rules mockedRules;
    //@Mock private EnvironmentConfig MockedEnvironmentConfig; // Mockito cannot mock final classes
    @Mock private ProcedureRepository mockedProcRepo;
    @Mock private RequestRepository mockedReqRepo;
    @Mock private RuleVariableRepository mockedRuleVariableRepo;
    @Mock private VariableRepository mockedVariableRepo;
    @Mock private ParameterRepository mockedParamRepo;
    @Mock private RequestTypeRepository mockedRequestTypeRepo;
    @Mock private HeaderRepository mockedHeaderRepo;

    @InjectMocks
    private RulesController mockedRulesController = null;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @SuppressWarnings("unchecked") // See explanation in test method body
    public void testFindAll() throws Exception {
        logger.debug("Running Test: {}", Utility.methodName());

        // findAll() returns List<Rules> which involves an unchecked cast from List to List<Rules>
        when(mockedRules.findAll()).thenReturn(mock(List.class));

        mockedRulesController.findAll();
        verify(mockedRules, times(1)).findAll(); // Observes the call from mockedRulesController verifying behavior
    }

    @Test
    public void testFindById() throws Exception {
        logger.debug("Running Test: {}", Utility.methodName());

        final Long expectedId = 1L;
        ArgumentCaptor<Long> actualId = ArgumentCaptor.forClass(Long.class);

        // When the rule id is requested, return the expected id
        Rule mockedRule = mock(Rule.class);
        when(mockedRule.getId()).thenReturn(expectedId);

        // When finding a rule by id, return the mocked rule
        when(mockedRules.findById(expectedId)).thenReturn(mockedRule);

        // Then the expected id should match the actual returned id
        mockedRulesController.findById(expectedId);
        verify(mockedRules, times(1)).findById(actualId.capture()); // Observes the call from mockedRulesController verifying behavior
        assertEquals(actualId.getValue(), expectedId, "Rule ids do not match.");
    }

    @Test // FIXME: There should be additional negative test cases whereby the ID doesn't exist or is otherwise invalid (rmb)
    public void testDeleteById() throws Exception {
        logger.debug("Running Test: {}", Utility.methodName());

        //Given
        final Long expectedId = 1L;
        final WebServiceResponse.Status expectedStatus = WebServiceResponse.Status.OK;
        final WebServiceResponse.ErrorType expectedErrorType = WebServiceResponse.ErrorType.NONE;
        ArgumentCaptor<Rule> actualRule = ArgumentCaptor.forClass(Rule.class);
        Rule mockedRule = mock(Rule.class);

        when(mockedRule.getId()).thenReturn(expectedId);    // Long Rule.getId()
        doNothing().when(mockedRules).delete(mockedRule);   // void Rules.delete(Rule)

        //When
        WebServiceResponse webServiceResponse = mockedRulesController.deleteById(expectedId);

        //Then
        verify(mockedRules, times(1)).delete(actualRule.capture()); // Observes the call from mockedRulesController verifying behavior
        assertEquals(actualRule.getValue().getId(), expectedId, "Rule ids do not match.");
        assertEquals(webServiceResponse.getStatus(), expectedStatus, "WebServiceResponse message wasn't OK");
        assertEquals(webServiceResponse.getErrorType(), expectedErrorType, "WebServiceResponse errorType wasn't NONE");
    }

    @Test(enabled = false) // FIXME: The corresponding method is severely broken (rmb)
    public void testCreate() throws Exception {
        logger.debug("Running Test: {}", Utility.methodName());

        //Given
        Long expectedId = 1L;
        Rule mockedRule = mock(Rule.class);
        // requestObject is a generic which cannot be described by Mockito without causing Lint warnings
        @SuppressWarnings("unchecked") Map<String, String> mockedRequestObject = mock(Map.class);

        when(mockedRules.create(mockedRule)).thenReturn(mockedRule);    //Rule Rules.create(Rule)

        when(mockedRequestObject.get("ruleName")).thenReturn("ruleName");
        when(mockedRequestObject.get("procedureName")).thenReturn("procedureName");
        when(mockedRequestObject.get("procedureScript")).thenReturn("procedureScript");
        when(mockedRequestObject.get("procedureEngineType")).thenReturn("1");
        when(mockedRequestObject.get("variable")).thenReturn("???");

        when(mockedRule.getId()).thenReturn(expectedId);

        //When
        Map<String, String> webServiceResponse = mockedRulesController.create(mockedRequestObject);

        //Then
        verify(mockedRule, times(1)).getId();
        assertEquals(webServiceResponse.get("id"), "1", "Rule ids do not match.");
    }

    @Test(enabled = false) // FIXME: Most of the corresponding method is commented out. (rmb)
    public void testUpdate() throws Exception {

    }

    @Test
    public void testExecuteRules() throws Exception {

    }

    @Test
    public void testGetRules() throws Exception {

    }

    @Test
    public void testSetRules() throws Exception {

    }

    @Test
    public void testGetRulesEngine() throws Exception {

    }

    @Test
    public void testSetRulesEngine() throws Exception {

    }

    @Test
    public void testGetProcRepo() throws Exception {

    }

    @Test
    public void testSetProcRepo() throws Exception {

    }

    @Test
    public void testGetReqRepo() throws Exception {

    }

    @Test
    public void testSetReqRepo() throws Exception {

    }

    @Test
    public void testGetRuleVariableRepo() throws Exception {

    }

    @Test
    public void testSetRuleVariableRepo() throws Exception {

    }

    @Test
    public void testGetVariableRepo() throws Exception {

    }

    @Test
    public void testSetVariableRepo() throws Exception {

    }

    @Test
    public void testGetParamRepo() throws Exception {

    }

    @Test
    public void testSetParamRepo() throws Exception {

    }

    @Test
    public void testGetRuleIdForTheRuleName() throws Exception {

    }

    @Test
    public void testGetRequestsGivenRuleID() throws Exception {

    }

    @Test
    public void testGetRequestsGivenRuleName() throws Exception {

    }

    @Test
    public void testGetProcedureGivenProcedureID() throws Exception {

    }

    @Test
    public void testGetProcedureGivenRuleName() throws Exception {

    }

    @Test
    public void testIsFetchable() throws Exception {

    }

    @Test
    public void testGetVarType() throws Exception {

    }

    @Test
    public void testGetEnumFromString() throws Exception {

    }

    @Test
    public void testGetParametersGivenVariable() throws Exception {

    }

    @Test
    public void testGetRequestsGivenVariable() throws Exception {

    }

    @Test
    public void testGetHeaderRepo() throws Exception {

    }

    @Test
    public void testSetHeaderRepo() throws Exception {

    }

    @Test
    public void testGetHeadersForTheGivenRequest() throws Exception {

    }
}
