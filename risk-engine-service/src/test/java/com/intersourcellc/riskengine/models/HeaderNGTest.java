/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intersourcellc.riskengine.models;

import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author davis
 */
public class HeaderNGTest {
    
    public HeaderNGTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }

    /**
     * Test of getId method, of class Header.
     */
    @Test (enabled = true)
    public void testGetId() {
        System.out.println("getId");
        Header instance = new Header();
        instance.setId(1L);
        Long expResult = 1L;
        Long result = instance.getId();
        assertEquals(result, expResult);

    }

    /**
     * Test of getRequest_id method, of class Header.
     */
    @Test
    public void testGetRequest_id() {
        System.out.println("getRequest_id");
        Header instance = new Header();
        instance.setRequest_id(1L);
        Long expResult = 1L;
        Long result = instance.getRequest_id();
        assertEquals(result, expResult);
    }

    /**
     * Test of getValue method, of class Header.
     */
    @Test (enabled = true)
    public void testGetValue() {
        System.out.println("getValue");
        Header instance = new Header();
        instance.setValue("Test");
        String expResult = "Test";
        String result = instance.getValue();
        assertEquals(result, expResult);
    }

    /**
     * Test of getRequest method, of class Header.
     */
    @Test (enabled = true)
    public void testGetRequest() {
        System.out.println("getRequest");
        Header instance = new Header();
        Request req = new Request(1L,1L,1L,"http://httpbin.org/Get");
        instance.setRequest(req);
        Request expResult = req;
        Request result = instance.getRequest();
        assertEquals(result, expResult);
    }

    /**
     * Test of setId method, of class Header.
     */
    @Test (enabled = true)
    public void testSetId() {
        System.out.println("setId");
        Long id = 1L;
        Header instance = new Header();
        instance.setId(id);
        Long expResult = id;
        Long result = instance.getId();
        assertEquals(result, expResult);
    }

    /**
     * Test of setRequest_id method, of class Header.
     */
    @Test (enabled = true)
    public void testSetRequest_id() {
        System.out.println("setRequest_id");
        Long request_id = 1L;
        Header instance = new Header();
        instance.setRequest_id(request_id);
        Long expResult = request_id;
        Long result = instance.getRequest_id();
        assertEquals(result, expResult);
    }

    /**
     * Test of setValue method, of class Header.
     */
    @Test (enabled = true)
    public void testSetValue() {
        System.out.println("setValue");
        String value = "Test setValue";
        Header instance = new Header();
        instance.setValue(value);
        String expResult = value;
        String result = instance.getValue();
        assertEquals(result, expResult);
    }

    /**
     * Test of setRequest method, of class Header.
     */
    @Test (enabled = true)
    public void testSetRequest() {
        System.out.println("setRequest");
        Request req = new Request(1L,1L,1L,"http://httpbin.org/Get");
        Header instance = new Header();
        instance.setRequest(req);
        Request expResult = req;
        Request result = instance.getRequest();
        assertEquals(result, expResult);
    }

    /**
     * Test of getToken method, of class Header.
     */
    @Test (enabled = true)
    public void testGetToken() {
        System.out.println("getToken");
        Header instance = new Header();
        instance.setToken("token get test");
        String expResult = "token get test";
        String result = instance.getToken();
        assertEquals(result, expResult);
    }

    /**
     * Test of setToken method, of class Header.
     */
    @Test (enabled = true)
    public void testSetToken() {
        System.out.println("setToken");
        String token = "set token test";
        Header instance = new Header();
        instance.setToken(token);
        String expResult = token;
        String result = instance.getToken();
        assertEquals(result, expResult);
    }

    /**
     * Test of getKey method, of class Header.
     */
    @Test (enabled = true)
    public void testGetKey() {
        System.out.println("getKey");
        Header instance = new Header();
        instance.setKey("get Key test");
        String expResult = "get Key test";
        String result = instance.getKey();
        assertEquals(result, expResult);

    }

    /**
     * Test of setKey method, of class Header.
     */
    @Test (enabled = true)
    public void testSetKey() {
        System.out.println("setKey");
        String key = "Set Key test";
        Header instance = new Header();
        instance.setKey(key);
        String expResult = key;
        String result = instance.getKey();
        assertEquals(result, expResult);
    }
}