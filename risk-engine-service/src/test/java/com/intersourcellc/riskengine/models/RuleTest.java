package com.intersourcellc.riskengine.models;

import com.intersourcellc.Utility;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

/**
 * Created by Ryan on 7/31/13.
 */
public class RuleTest {
    final static Logger logger = LoggerFactory.getLogger(RuleTest.class);

    @Mock
    private Rule mockedRule;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSetId() throws Exception {
        logger.debug("Running Test: {}", Utility.methodName());

        final Long expectedId = 1L;
        ArgumentCaptor<Long> actualId = ArgumentCaptor.forClass(Long.class);

        mockedRule.setId(expectedId);

        verify(mockedRule).setId(actualId.capture());

        assertEquals(actualId.getValue(), expectedId, "Rule ids do not match.");
    }

    @Test
    public void testGetId() throws Exception {
        logger.debug("Running Test: {}", Utility.methodName());

        final Long expectedId = 1L;
        Long actualId;

        when(mockedRule.getId()).thenReturn(expectedId);
        actualId = mockedRule.getId();

        verify(mockedRule).getId();
        assertEquals(actualId, expectedId, "Rule ids do not match.");
    }

    @Test
    public void testSetName() throws Exception {
        logger.debug("Running Test: {}", Utility.methodName());

        final String expectedName = "ruleName";
        ArgumentCaptor<String> actualName = ArgumentCaptor.forClass(String.class);

        mockedRule.setName(expectedName);

        verify(mockedRule).setName(actualName.capture());

        assertEquals(actualName.getValue(), expectedName, "Rule names do not match.");
    }

    @Test
    public void testGetName() throws Exception {
        logger.debug("Running Test: {}", Utility.methodName());

        final String expectedName = "ruleName";
        String actualName;

        when(mockedRule.getName()).thenReturn(expectedName);
        actualName = mockedRule.getName();

        verify(mockedRule).getName();
        assertEquals(actualName, expectedName, "Rule names do not match.");
    }

    @Test
    public void testSetProcedureId() throws Exception {
        logger.debug("Running Test: {}", Utility.methodName());

        final Long expectedProcedureId = 1L;
        ArgumentCaptor<Long> actualProcedureId = ArgumentCaptor.forClass(Long.class);

        mockedRule.setProcedureId(expectedProcedureId);

        verify(mockedRule).setProcedureId(actualProcedureId.capture());

        assertEquals(actualProcedureId.getValue(), expectedProcedureId, "Rule procedure ids do not match.");
    }

    @Test
    public void testGetProcedureId() throws Exception {
        logger.debug("Running Test: {}", Utility.methodName());

        final Long expectedProcedureId = 1L;
        Long actualProcedureId;

        when(mockedRule.getProcedureId()).thenReturn(expectedProcedureId);
        actualProcedureId = mockedRule.getProcedureId();

        verify(mockedRule).getProcedureId();
        assertEquals(actualProcedureId, expectedProcedureId, "Rule procedure ids do not match.");
    }

    @Test
    public void testSetProcedure() throws Exception {
        logger.debug("Running Test: {}", Utility.methodName());

        final Procedure expectedProcedure = mock(Procedure.class);
        ArgumentCaptor<Procedure> actualProcedure = ArgumentCaptor.forClass(Procedure.class);

        mockedRule.setProcedure(expectedProcedure);

        verify(mockedRule).setProcedure(actualProcedure.capture());

        assertEquals(actualProcedure.getValue(), expectedProcedure, "Rule procedures do not match.");
    }

    @Test
    public void testGetProcedure() throws Exception {
        logger.debug("Running Test: {}", Utility.methodName());

        final Procedure expectedProcedure = mock(Procedure.class);
        Procedure actualProcedure;

        when(mockedRule.getProcedure()).thenReturn(expectedProcedure);
        actualProcedure = mockedRule.getProcedure();

        verify(mockedRule).getProcedure();
        assertEquals(actualProcedure, expectedProcedure, "Rule procedures do not match.");
    }

}
