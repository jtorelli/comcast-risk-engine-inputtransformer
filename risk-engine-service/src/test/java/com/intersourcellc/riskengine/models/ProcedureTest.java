package com.intersourcellc.riskengine.models;

import com.intersourcellc.Utility;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

/**
 * Created by Ryan on 7/31/13.
 */
public class ProcedureTest {
    final static Logger logger = LoggerFactory.getLogger(RuleTest.class);

    @Mock
    private Procedure mockedProcedure;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSetId() throws Exception {
        logger.debug("Running Test: {}", Utility.methodName());

        final Long expectedId = 1L;
        ArgumentCaptor<Long> actualId = ArgumentCaptor.forClass(Long.class);

        mockedProcedure.setId(expectedId);

        verify(mockedProcedure).setId(actualId.capture());

        assertEquals(actualId.getValue(), expectedId, "Procedure ids do not match.");
    }

    @Test
    public void testGetId() throws Exception {
        logger.debug("Running Test: {}", Utility.methodName());

        final Long expectedId = 1L;
        Long actualId;

        when(mockedProcedure.getId()).thenReturn(expectedId);
        actualId = mockedProcedure.getId();

        verify(mockedProcedure).getId();
        assertEquals(actualId, expectedId, "Procedure ids do not match.");
    }

    @Test
    public void testSetName() throws Exception {
        logger.debug("Running Test: {}", Utility.methodName());

        final String expectedName = "procedureName";
        ArgumentCaptor<String> actualName = ArgumentCaptor.forClass(String.class);

        mockedProcedure.setName(expectedName);

        verify(mockedProcedure).setName(actualName.capture());

        assertEquals(actualName.getValue(), expectedName, "Procedure names do not match.");
    }

    @Test
    public void testGetName() throws Exception {
        logger.debug("Running Test: {}", Utility.methodName());

        final String expectedName = "ruleName";
        String actualName;

        when(mockedProcedure.getName()).thenReturn(expectedName);
        actualName = mockedProcedure.getName();

        verify(mockedProcedure).getName();
        assertEquals(actualName, expectedName, "Procedure names do not match.");
    }

    @Test
    public void testSetScript() throws Exception {
        logger.debug("Running Test: {}", Utility.methodName());

        final String expectedScript = "procedureScript";
        ArgumentCaptor<String> actualScript = ArgumentCaptor.forClass(String.class);

        mockedProcedure.setScript(expectedScript);

        verify(mockedProcedure).setScript(actualScript.capture());

        assertEquals(actualScript.getValue(), expectedScript, "Procedure scripts do not match.");
    }

    @Test
    public void testGetScript() throws Exception {
        logger.debug("Running Test: {}", Utility.methodName());

        final String expectedScript = "procedureScript";
        String actualScript;

        when(mockedProcedure.getScript()).thenReturn(expectedScript);
        actualScript = mockedProcedure.getScript();

        verify(mockedProcedure).getScript();
        assertEquals(actualScript, expectedScript, "Procedure scripts do not match.");
    }

    @Test
    public void testSetEngineTypeId() throws Exception {
        logger.debug("Running Test: {}", Utility.methodName());

        final Long expectedEngineTypeId = 1L;
        ArgumentCaptor<Long> actualEngineTypeId = ArgumentCaptor.forClass(Long.class);

        mockedProcedure.setEngineTypeId(expectedEngineTypeId);

        verify(mockedProcedure).setEngineTypeId(actualEngineTypeId.capture());

        assertEquals(actualEngineTypeId.getValue(), expectedEngineTypeId, "Procedure engine type ids do not match.");
    }

    @Test
    public void testGetEngineTypeId() throws Exception {
        logger.debug("Running Test: {}", Utility.methodName());

        final Long expectedEngineTypeId = 1L;
        Long actualEngineTypeId;

        when(mockedProcedure.getEngineTypeId()).thenReturn(expectedEngineTypeId);
        actualEngineTypeId = mockedProcedure.getEngineTypeId();

        verify(mockedProcedure).getEngineTypeId();
        assertEquals(actualEngineTypeId, expectedEngineTypeId, "Procedure engine type ids do not match.");
    }

    @Test
    public void testSetEngineTypes() throws Exception {
        logger.debug("Running Test: {}", Utility.methodName());

        final EngineTypes expectedEngineTypes = mock(EngineTypes.class);
        ArgumentCaptor<EngineTypes> actualEngineTypes = ArgumentCaptor.forClass(EngineTypes.class);

        mockedProcedure.setEngineTypes(expectedEngineTypes);

        verify(mockedProcedure).setEngineTypes(actualEngineTypes.capture());

        assertEquals(actualEngineTypes.getValue(), expectedEngineTypes, "Procedure engine types do not match.");
    }

    @Test
    public void testGetEngineTypes() throws Exception {
        logger.debug("Running Test: {}", Utility.methodName());

        final EngineTypes expectedEngineTypes = mock(EngineTypes.class);
        EngineTypes actualEngineTypes;

        when(mockedProcedure.getEngineTypes()).thenReturn(expectedEngineTypes);
        actualEngineTypes = mockedProcedure.getEngineTypes();

        verify(mockedProcedure).getEngineTypes();
        assertEquals(actualEngineTypes, expectedEngineTypes, "Procedure engine types do not match.");
    }

}
