--liquibase formatted sql

--changeset dzf:re_engine_type dbms:mysql
CREATE TABLE `re_engine_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `re_engine_type` (`id`, `name`) VALUES
	(1, 'drools'),
	(2, 'javascript');
--rollback DROP TABLE `re_engine_type`;

--changeset dzf:re_procedure dbms:mysql
CREATE TABLE `re_procedure` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `engine_type_id` int(11) NOT NULL,
  `script` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `fk_procedure_engine_type_idx` (`engine_type_id`),
  CONSTRAINT `fk_procedure_engine_type` FOREIGN KEY (`engine_type_id`) REFERENCES `re_engine_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--rollback DROP TABLE `re_procedure`;

--changeset dzf:re_variable_type dbms:mysql
CREATE TABLE `re_variable_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

INSERT INTO `re_variable_type` (`id`, `name`) VALUES
	(1, 'BOOLEAN'),
	(2, 'DOUBLE'),
	(3, 'FETCHABLE'),
	(4, 'INT'),
	(5, 'JSON'),
	(6, 'STRING');
--rollback DROP TABLE `re_variable_type`;

--changeset dzf:re_variable dbms:mysql
CREATE TABLE `re_variable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `variable_name` varchar(45) DEFAULT NULL,
  `variable_value` varchar(500) DEFAULT NULL,
  `variable_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  --UNIQUE KEY `variable_name_UNIQUE` (`variable_name`),
  KEY `fk_variable_variable_type_idx` (`variable_type_id`),
  CONSTRAINT `fk_variable_variable_type` FOREIGN KEY (`variable_type_id`) REFERENCES `re_variable_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--rollback DROP TABLE `re_variable`;

--changeset dzf:re_request_type dbms:mysql
CREATE TABLE IF NOT EXISTS `re_request_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO `re_request_type` (`id`, `name`) VALUES
	(1, 'GET'),
	(3, 'POST'),
	(2, 'PUT');
--rollback DROP TABLE `re_request_type`;

--changeset dzf:re_request dbms:mysql
CREATE TABLE `re_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `variable_id` int(11) NOT NULL,
  `request_type_id` int(11) NOT NULL,
  `url` varchar(2048) DEFAULT NULL,
  `body` mediumtext DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_request_variable_idx` (`variable_id`),
  KEY `fk_request_request_type_idx` (`request_type_id`),
  CONSTRAINT `fk_request_request_type` FOREIGN KEY (`request_type_id`) REFERENCES `re_request_type` (`id`) ,
  CONSTRAINT `fk_request_variable` FOREIGN KEY (`variable_id`) REFERENCES `re_variable` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--rollback DROP TABLE `re_request`;

--changeset dzf:re_parameter dbms:mysql
CREATE TABLE `re_parameter` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `request_id` INT(11) NOT NULL,
  `token` VARCHAR(50) NOT NULL,
  `key` VARCHAR(50) NOT NULL,
  `value` VARCHAR(500) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_parameter_request_idx` (`request_id`),
  CONSTRAINT `fk_parameter_request` FOREIGN KEY (`request_id`) REFERENCES `re_request` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--rollback DROP TABLE `re_parameter`;

--changeset dzf:re_header dbms:mysql
CREATE TABLE `re_header` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `request_id` INT(11) NOT NULL,
  `token` VARCHAR(50) NOT NULL,
  `key` VARCHAR(50) NOT NULL,
  `value` VARCHAR(500) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_header_request_idx` (`request_id`),
  CONSTRAINT `fk_header_request` FOREIGN KEY (`request_id`) REFERENCES `re_request` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--rollback DROP TABLE `re_header`;

--changeset dzf:re_rule dbms:mysql
CREATE TABLE `re_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rule_name` varchar(45) DEFAULT NULL,
  `procedure_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rule_name_UNIQUE` (`rule_name`),
  KEY `fk_rule_procedure_idx` (`procedure_id`),
  CONSTRAINT `fk_rule_procedure` FOREIGN KEY (`procedure_id`) REFERENCES `re_procedure` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--rollback DROP TABLE `re_rule`;

--changeset dzf:re_rule_variable dbms:mysql
CREATE TABLE `re_rule_variable` (
  `rule_id` int(11) NOT NULL,
  `variable_id` int(11) NOT NULL,
  PRIMARY KEY (`rule_id`,`variable_id`),
  CONSTRAINT `fk_rule_variable_rule` FOREIGN KEY (`rule_id`) REFERENCES `re_rule` (`id`),
  CONSTRAINT `fk_rule_variable_variable` FOREIGN KEY (`variable_id`) REFERENCES `re_variable` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--rollback DROP TABLE `re_rule_variable`;
