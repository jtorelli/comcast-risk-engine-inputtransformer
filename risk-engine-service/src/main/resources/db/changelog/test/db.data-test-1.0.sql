--liquibase formatted sql

--changeset rmb:addProcedureTestData dbms:mysql
INSERT INTO `re_procedure` (`id`, `name`, `engine_type_id`, `script`) VALUES
        (1, 'Weather.js', 2, 'var log = JsLogger.getLogger(ruleName);\n\nvar weather = JSON.parse(weatherJson);\n\nDebug.printContext(this);\n\nlog.info(\'City: \' + weather.name);\nlog.info(\'Temp: \' + kToC(weather.main.temp) + \' Deg C\');\nlog.info(\'Temp: \' + cToF(kToC(weather.main.temp)) + \' Deg F\');\n\n// Convert the temperature, but also throw the printStackTrace()\nvar currentTemp = kToF(weather.main.temp);\n\nif (currentTemp < 62) {\n   factoid.setFact(\'temperature\', \'Cold\', DataTypeSTRING);\n} else if (currentTemp >= 62 && currentTemp < 75) {\n   factoid.addFact(\'temperature\', \'Cool\', DataTypeSTRING);\n} else if (currentTemp >= 75 && currentTemp < 90) {\n   factoid.addFact(\'temperature\', \'Warm\', DataTypeSTRING);\n} else {\n   factoid.addFact(\'temperature\', \'Hot\', DataTypeSTRING);\n};\n\nfunction kToC(k, opt) {\n   opt = opt || {};\n   opt.params = opt.params || {};\n   opt.debug = opt.debug || false;\n\n   if(opt.debug) Debug.printStackTrace();\n   return k - 273.15;\n};\n\nfunction cToF(c) {\n   return 32 + (9.0/5.0) * c;\n};\n\nfunction kToF(k) {\n   return 32 + (9.0/5.0) * kToC(k, {debug:true});\n};'),
        (2, 'CompareStrings.js', 2, 'var logger = JsLogger.getLogger(ruleName);\n\nvar requestBody = JSON.parse(requestBodyJson);\n\nlogger.info(\'requestBodyJson: \' + requestBodyJson);\nlogger.info(\'requestBody.parent.data.string1: \' + requestBody.parent.data.string1);\n\nif (requestBody.parent.data.string1 === \'hello\' || requestBody.parent.data.string1 === \'world\' ) {\n 	factoid.addFact(\'result\', \'true\', DataTypeBOOLEAN);\n	factoid.addTrace(\'matches\');\n} else {\n	factoid.addFact(\'result\', \'false\', DataTypeBOOLEAN);\n	factoid.addTrace(\'different\');\n};'),
        (3, 'Levenshtein.js', 2, 'var distance = levenshtein(string1.toLowerCase(), string2.toLowerCase()); \n\nfactoid.addTrace(\'distance = \' + distance.toString()); \nfactoid.addFact(\'distance\', distance.toString(), DataTypeINT);\n\nvar similarity = 1.0 - distance / Math.max(string1.length, string2.length); \n\nfactoid.addTrace(\'similarity = \' + similarity.toString()); \nfactoid.addFact(\'similarity\', similarity.toString(), DataTypeDOUBLE);\n\nfunction levenshtein (s1, s2) {\n	if (s1 == s2) {\n		return 0;\n	}\n\n	var s1_len = s1.length;\n	var s2_len = s2.length;\n\n	if (s1_len === 0) {\n		return s2_len;\n	}\n\n	if (s2_len === 0) {\n		return s1_len;\n	}\n	\n	// BEGIN STATIC\n	var split = false;\n	try {\n		split = !(\'0\')[0];\n	} catch (e) {\n		split = true; \n	}\n	// END STATIC\n\n	if (split) {\n		s1 = s1.split(\'\');\n		s2 = s2.split(\'\');\n	}\n	\n	var v0 = new Array(s1_len + 1);\n	var v1 = new Array(s1_len + 1);\n	\n	var s1_idx = 0,	s2_idx = 0,	cost = 0;\n\n	for (s1_idx = 0; s1_idx < s1_len + 1; s1_idx++) {\n		v0[s1_idx] = s1_idx;\n	}\n	\n	var char_s1 = \'\', char_s2 = \'\';\n	\n	for (s2_idx = 1; s2_idx <= s2_len; s2_idx++) {\n		v1[0] = s2_idx;\n		char_s2 = s2[s2_idx - 1];\n	\n		for (s1_idx = 0; s1_idx < s1_len; s1_idx++) {\n			char_s1 = s1[s1_idx];\n			cost = (char_s1 == char_s2) ? 0 : 1;\n			var m_min = v0[s1_idx + 1] + 1;\n			var b = v1[s1_idx] + 1;\n			var c = v0[s1_idx] + cost;\n			if (b < m_min) {\n				m_min = b;\n			}\n			if (c < m_min) {\n				m_min = c;\n			}\n			v1[s1_idx + 1] = m_min;\n		}\n		\n		var v_tmp = v0;\n		v0 = v1;\n		v1 = v_tmp;\n	}\n\n	return v0[s1_len];\n}');
--rollback DELETE FROM `re_procedure` WHERE `id` IN (1, 2, 3);

--changeset rmb:addVariableTestData dbms:mysql
INSERT INTO `re_variable` (`id`, `variable_name`, `variable_value`, `variable_type_id`) VALUES
        (1, 'weatherJson', '', 3),
        (2, 'string1', 'defaultString1', 6),
        (3, 'string2', 'defaultString2', 6);
--rollback DELETE FROM `re_variable` WHERE `id` IN (1, 2, 3);

--changeset rmb:addRequestTestData dbms:mysql
INSERT INTO `re_request` (`id`, `variable_id`, `request_type_id`, `url`, `body`) VALUES
        (1, 1, 1, 'http://api.openweathermap.org/data/2.5/weather', null);
--rollback DELETE FROM `re_request` WHERE `id` IN (1);

--changeset rmb:addHeaderTestData dbms:mysql
INSERT INTO `re_header` (`id`, `request_id`, `token`, `key`, `value`) VALUES
        (1, 1, 'HeaderToken1', 'authuser', 'nt username'),
        (2, 1, 'HeaderToken1', 'authpass', 'nt password');
--rollback DELETE FROM `re_header` WHERE `id` IN (1, 2);

--changeset rmb:addParameterTestData dbms:mysql
INSERT INTO `re_parameter` (`id`, `request_id`, `token`, `key`, `value`) VALUES
        (1, 1, 'city', 'q', 'seattle,us');
--rollback DELETE FROM `re_parameter` WHERE `id` IN (1);

--changeset rmb:addRuleTestData dbms:mysql
INSERT INTO `re_rule` (`id`, `rule_name`, `procedure_id`) VALUES
        (1, 'Weather in city', 1),
        (2, 'String comparison', 2),
        (3, 'Levenshtein distance comparison', 3);
--rollback DELETE FROM `re_rule` WHERE `id` IN (1, 2, 3);

--changeset rmb:addRuleVariableTestData dbms:mysql
INSERT INTO `re_rule_variable` (`rule_id`, `variable_id`) VALUES
        (1, 1),
        (2, 2),
        (2, 3),
        (3, 2),
        (3, 3);
--rollback DELETE FROM `re_rule_variable` WHERE (`rule_id` = 1 AND `variable_id` = 1) OR (`rule_id` = 2 AND `variable_id` = 2) OR (`rule_id` = 2 AND `variable_id` = 3) OR (`rule_id` = 3 AND `variable_id` = 2) OR (`rule_id` = 3 AND `variable_id` = 3);
