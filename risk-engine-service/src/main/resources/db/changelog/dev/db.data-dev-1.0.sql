--liquibase formatted sql

--changeset rmb:addProcedureData dbms:mysql
INSERT INTO `re_procedure` (`id`, `name`, `engine_type_id`, `script`) VALUES
        (1, 'Weather.js', 2, 'var log = JsLogger.getLogger(ruleName);\n\nvar weather = JSON.parse(weatherJson);\n\nDebug.printContext(this);\n\nlog.info(\'City: \' + weather.name);\nlog.info(\'Temp: \' + kToC(weather.main.temp) + \' Deg C\');\nlog.info(\'Temp: \' + cToF(kToC(weather.main.temp)) + \' Deg F\');\n\n// Convert the temperature, but also throw the printStackTrace()\nvar currentTemp = kToF(weather.main.temp);\n\nif (currentTemp < 62) {\n   factoid.setFact(\'temperature\', \'Cold\', DataTypeSTRING);\n} else if (currentTemp >= 62 && currentTemp < 75) {\n   factoid.addFact(\'temperature\', \'Cool\', DataTypeSTRING);\n} else if (currentTemp >= 75 && currentTemp < 90) {\n   factoid.addFact(\'temperature\', \'Warm\', DataTypeSTRING);\n} else {\n   factoid.addFact(\'temperature\', \'Hot\', DataTypeSTRING);\n};\n\nfunction kToC(k, opt) {\n   opt = opt || {};\n   opt.params = opt.params || {};\n   opt.debug = opt.debug || false;\n\n   if(opt.debug) Debug.printStackTrace();\n   return k - 273.15;\n};\n\nfunction cToF(c) {\n   return 32 + (9.0/5.0) * c;\n};\n\nfunction kToF(k) {\n   return 32 + (9.0/5.0) * kToC(k, {debug:true});\n};'),
        (2, 'CompareStrings.js', 2, 'var logger = JsLogger.getLogger(ruleName);\n\nvar requestBody = JSON.parse(requestBodyJson);\n\nlogger.info(\'requestBodyJson: \' + requestBodyJson);\nlogger.info(\'requestBody.parent.data.string1: \' + requestBody.parent.data.string1);\n\nif (requestBody.parent.data.string1 === \'hello\' || requestBody.parent.data.string1 === \'world\' ) {\n 	factoid.addFact(\'result\', \'true\', DataTypeBOOLEAN);\n	factoid.addTrace(\'matches\');\n} else {\n	factoid.addFact(\'result\', \'false\', DataTypeBOOLEAN);\n	factoid.addTrace(\'different\');\n};'),
        (3, 'Levenshtein.js', 2, 'var distance = levenshtein(string1.toLowerCase(), string2.toLowerCase()); \n\nfactoid.addTrace(\'distance = \' + distance.toString()); \nfactoid.addFact(\'distance\', distance.toString(), DataTypeINT);\n\nvar similarity = 1.0 - distance / Math.max(string1.length, string2.length); \n\nfactoid.addTrace(\'similarity = \' + similarity.toString()); \nfactoid.addFact(\'similarity\', similarity.toString(), DataTypeDOUBLE);\n\nfunction levenshtein (s1, s2) {\n	if (s1 == s2) {\n		return 0;\n	}\n\n	var s1_len = s1.length;\n	var s2_len = s2.length;\n\n	if (s1_len === 0) {\n		return s2_len;\n	}\n\n	if (s2_len === 0) {\n		return s1_len;\n	}\n	\n	// BEGIN STATIC\n	var split = false;\n	try {\n		split = !(\'0\')[0];\n	} catch (e) {\n		split = true; \n	}\n	// END STATIC\n\n	if (split) {\n		s1 = s1.split(\'\');\n		s2 = s2.split(\'\');\n	}\n	\n	var v0 = new Array(s1_len + 1);\n	var v1 = new Array(s1_len + 1);\n	\n	var s1_idx = 0,	s2_idx = 0,	cost = 0;\n\n	for (s1_idx = 0; s1_idx < s1_len + 1; s1_idx++) {\n		v0[s1_idx] = s1_idx;\n	}\n	\n	var char_s1 = \'\', char_s2 = \'\';\n	\n	for (s2_idx = 1; s2_idx <= s2_len; s2_idx++) {\n		v1[0] = s2_idx;\n		char_s2 = s2[s2_idx - 1];\n	\n		for (s1_idx = 0; s1_idx < s1_len; s1_idx++) {\n			char_s1 = s1[s1_idx];\n			cost = (char_s1 == char_s2) ? 0 : 1;\n			var m_min = v0[s1_idx + 1] + 1;\n			var b = v1[s1_idx] + 1;\n			var c = v0[s1_idx] + cost;\n			if (b < m_min) {\n				m_min = b;\n			}\n			if (c < m_min) {\n				m_min = c;\n			}\n			v1[s1_idx + 1] = m_min;\n		}\n		\n		var v_tmp = v0;\n		v0 = v1;\n		v1 = v_tmp;\n	}\n\n	return v0[s1_len];\n}'),
        (4, 'cnmDB.js', 2, 'return 0'),
        (5, 'simplePost.js', 2, 'return 0'),
        (6, 'DeviceExists.js', 2, 'var logger = JsLogger.getLogger(ruleName);\n\nvar deviceList = JSON.parse(deviceListJson);\n\nlogger.info(\'Found \' + deviceList.result.length + \' device(s):\');\n\nfor (var i = 0; i < deviceList.result.length; i++) {\n    var resultTrace = \'did: \' + deviceList.result[i].did +\n        \', hostname: \' + deviceList.result[i].hostname +\n        \', reachable: \' + Boolean(deviceList.result[i].reachable);\n    logger.info(\'\t\' + resultTrace)\n    factoid.addTrace(resultTrace);\n}\n\nif (deviceList.result.length > 0) {\n    factoid.addFact(\'result\', \'true\', DataTypeBOOLEAN);\n} else {\n    factoid.addFact(\'result\', \'false\', DataTypeBOOLEAN);\n}\n');
--rollback DELETE FROM `re_procedure` WHERE `id` IN (1, 2, 3, 4, 5, 6);

--changeset rmb:addVariableData dbms:mysql
INSERT INTO `re_variable` (`id`, `variable_name`, `variable_value`, `variable_type_id`) VALUES
        (1, 'weatherJson', '', 3),
        (2, 'string1', 'defaultString1', 6),
        (3, 'string2', 'defaultString2', 6),
        (4, 'cnmDbResultJson', '', 3),
        (5, 'simplePostJson', '', 3),
        (6, 'deviceListJson', '', 3),
        (7, 'deviceListJson', '', 3),
        (8, 'deviceListJson', '', 3),
        (9, 'deviceListJson', '', 3),
        (10, 'deviceListJson', '', 3);
--rollback DELETE FROM `re_variable` WHERE `id` IN (1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

--changeset rmb:addRequestData dbms:mysql
INSERT INTO `re_request` (`id`, `variable_id`, `request_type_id`, `url`, `body`) VALUES
        (1, 1, 1, 'http://api.openweathermap.org/data/2.5/weather', null),
        (2, 4, 3, 'https://cnmapi.nads.comcast.net/common/devices/find', null),
        (3, 5, 3, 'http://httpbin.org/post', null),
        (4, 6, 1, 'https://cnmapi.nads.comcast.net/common/devices/find/', null),
        (5, 7, 1, 'http://echojson.com/result/%5B%5D', null),
        (6, 8, 1, 'http://echojson.com/result/%5B%7B%22did%22:%222847%22,%22loopback0%22:%221146423809%22,%22hostname%22:%22ar01.D1stonemtn.ga.atlanta.comcast.net%22,%22reachable%22:%221%22,%22site%22:%2241%22,%22model%22:%225%22,%22online%22:%221%22,%22dev_snmp_desc%22:null,%22dev_zone_sme%22:null,%22dev_zone_fqdn%22:null,%22dev_zone_snmp%22:null,%22dev_zone_final%22:null,%22zone_conflict%22:null,%22dTime%22:%221375675062%22,%22dev_status%22:%220%22,%22discovery_id%22:%2252%22,%22fqdn%22:%22ar01.D1stonemtn.ga.atlanta.comcast.net%22,%22headend_name%22:null,%22headend_address%22:null,%22dialup%22:null,%22serial%22:%22JN119A16AAHB%22,%22replacementstatus%22:%220%22%7D%5D', null),
        (7, 9, 1, 'http://echojson.com/result/%5B%7B%22did%22:%222847%22,%22loopback0%22:%221146423809%22,%22hostname%22:%22ar01.D1stonemtn.ga.atlanta.comcast.net%22,%22reachable%22:%221%22,%22site%22:%2241%22,%22model%22:%225%22,%22online%22:%221%22,%22dev_snmp_desc%22:null,%22dev_zone_sme%22:null,%22dev_zone_fqdn%22:null,%22dev_zone_snmp%22:null,%22dev_zone_final%22:null,%22zone_conflict%22:null,%22dTime%22:%221375675062%22,%22dev_status%22:%220%22,%22discovery_id%22:%2252%22,%22fqdn%22:%22ar01.D1stonemtn.ga.atlanta.comcast.net%22,%22headend_name%22:null,%22headend_address%22:null,%22dialup%22:null,%22serial%22:%22JN119A16AAHB%22,%22replacementstatus%22:%220%22%7D,%7B%22did%22:%225353%22,%22loopback0%22:%221146498135%22,%22hostname%22:%22ar01.dannythomas.tn.malt.comcast.net%22,%22reachable%22:%220%22,%22site%22:%22116%22,%22model%22:%227%22,%22online%22:%220%22,%22dev_snmp_desc%22:null,%22dev_zone_sme%22:null,%22dev_zone_fqdn%22:null,%22dev_zone_snmp%22:null,%22dev_zone_final%22:null,%22zone_conflict%22:null,%22dTime%22:%221338615744%22,%22dev_status%22:%220%22,%22discovery_id%22:%2231%22,%22fqdn%22:%22ar01.dannythomas.tn.malt.comcast.net%22,%22headend_name%22:null,%22headend_address%22:null,%22dialup%22:null,%22serial%22:null,%22replacementstatus%22:%220%22%7D,%7B%22did%22:%226272%22,%22loopback0%22:%221146555008%22,%22hostname%22:%22ar01.dover.de.bad.comcast.net%22,%22reachable%22:%221%22,%22site%22:%22422%22,%22model%22:%227%22,%22online%22:%220%22,%22dev_snmp_desc%22:null,%22dev_zone_sme%22:null,%22dev_zone_fqdn%22:null,%22dev_zone_snmp%22:null,%22dev_zone_final%22:null,%22zone_conflict%22:null,%22dTime%22:%221363237990%22,%22dev_status%22:%220%22,%22discovery_id%22:%2231%22,%22fqdn%22:%22ar01.dover.de.bad.comcast.net%22,%22headend_name%22:null,%22headend_address%22:null,%22dialup%22:null,%22serial%22:%22FOX0823008C%22,%22replacementstatus%22:%220%22%7D%5D', null),
        (8, 10, 1, 'http://echojson.com/result/%5B%7B%22did%22:%222847%22,%22loopback0%22:%221146423809%22,%22hostname%22:%22ar01.D1stonemtn.ga.atlanta.comcast.net%22,%22reachable%22:%221%22,%22site%22:%2241%22,%22model%22:%225%22,%22online%22:%221%22,%22dev_snmp_desc%22:null,%22dev_zone_sme%22:null,%22dev_zone_fqdn%22:null,%22dev_zone_snmp%22:null,%22dev_zone_final%22:null,%22zone_conflict%22:null,%22dTime%22:%221375675062%22,%22dev_status%22:%220%22,%22discovery_id%22:%2252%22,%22fqdn%22:%22ar01.D1stonemtn.ga.atlanta.comcast.net%22,%22headend_name%22:null,%22headend_address%22:null,%22dialup%22:null,%22serial%22:%22JN119A16AAHB%22,%22replacementstatus%22:%220%22%7D%5D', null);

--rollback DELETE FROM `re_request` WHERE `id` IN (1, 2, 3, 4, 5, 6, 7, 8);

--changeset rmb:addHeaderData dbms:mysql
INSERT INTO `re_header` (`id`, `request_id`, `token`, `key`, `value`) VALUES
        (1, 2, 'HeaderToken1', 'authuser', 'nt username'),
        (2, 2, 'HeaderToken1', 'authpass', 'nt password'),
        (3, 3, 'HeaderToken2', 'authuser', 'nt username'),
        (4, 3, 'HeaderToken2', 'authpass', 'nt password'),
        (5, 4, 'authuser', 'authuser', 'nt username'),
        (6, 4, 'authpass', 'authpass', 'nt password');
--rollback DELETE FROM `re_header` WHERE `id` IN (1, 2, 3, 4, 5, 6);

--changeset rmb:addParameterData dbms:mysql
INSERT INTO `re_parameter` (`id`, `request_id`, `token`, `key`, `value`) VALUES
        (1, 1, 'city', 'q', '{city}'),
        (2, 4, 'deviceName', 'where', 'devices.hostname like \'{deviceName}\''),
        (3, 8, 'foo', 'foo', '{foo}');
--rollback DELETE FROM `re_parameter` WHERE `id` IN (1, 2, 3);

--changeset rmb:addRuleData dbms:mysql
INSERT INTO `re_rule` (`id`, `rule_name`, `procedure_id`) VALUES
        (1, 'Weather in city', 1),
        (2, 'String comparison', 2),
        (3, 'Levenshtein distance comparison', 3),
        (4, 'CNM DB device status check', 4),
        (5, 'Simple Post example', 5),
        (6, 'Device Exists', 6),
        (7, 'Device Exists (mocked endpoint: 0 devices)', 6),
        (8, 'Device Exists (mocked endpoint: 1 device)', 6),
        (9, 'Device Exists (mocked endpoint: 3 devices)', 6),
        (10, 'Device Exists (mocked endpoint: DE18564)', 6);
--rollback DELETE FROM `re_rule` WHERE `id` IN (1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

--changeset rmb:addRuleVariableData dbms:mysql
INSERT INTO `re_rule_variable` (`rule_id`, `variable_id`) VALUES
        (1, 1),
        (2, 2),
        (2, 3),
        (3, 2),
        (3, 3),
        (4, 4),
        (5, 5),
        (6, 6),
        (7, 7),
        (8, 8),
        (9, 9),
        (10, 10);
--rollback DELETE FROM `re_rule_variable` WHERE (`rule_id` = 1 AND `variable_id` = 1) OR (`rule_id` = 2 AND `variable_id` = 2) OR (`rule_id` = 2 AND `variable_id` = 3) OR (`rule_id` = 3 AND `variable_id` = 2) OR (`rule_id` = 3 AND `variable_id` = 3) OR (`rule_id` = 4 AND `variable_id` = 4) OR (`rule_id` = 5 AND `variable_id` = 5) OR (`rule_id` = 6 AND `variable_id` = 6) OR (`rule_id` = 7 AND `variable_id` = 7) OR (`rule_id` = 8 AND `variable_id` = 8) OR (`rule_id` = 9 AND `variable_id` = 9) OR (`rule_id` = 10 AND `variable_id` = 10);
