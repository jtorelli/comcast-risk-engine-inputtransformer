/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intersourcellc.riskengine.controllers;

import com.intersourcellc.riskengine.models.WebServiceResponse;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

/**
 * 
 * The base web services class
 * 
 * @author jimt
 */
class ExceptionHandlingController {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * This exception handler should catch any un-handled exceptions
     */
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public final ModelAndView handleAllExceptions(Exception ex, HttpServletRequest request) {

        WebServiceResponse response = new WebServiceResponse(WebServiceResponse.Status.ERROR, WebServiceResponse.ErrorType.UNHANDLED, ex.getMessage(), null, request, ex);

        response.logResponseToClientLog();

        return response.asModelAndView();
    }

    /**
     * This exception handler should catch any un-handled exceptions
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public final ModelAndView handleMethodArgumentNotValidExceptions(MethodArgumentNotValidException ex, HttpServletRequest request) {

        WebServiceResponse response = new WebServiceResponse(WebServiceResponse.Status.ERROR, WebServiceResponse.ErrorType.VALIDATION, ex.getMessage(), null, request, ex);

        response.logResponseToClientLog();

        return response.asModelAndView();
    }

    /**
     * This exception handler should catch any un-handled exceptions
     */
    @ExceptionHandler(MissingServletRequestParameterException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public final ModelAndView handleMissingServletRequestParameterExceptionExceptions(MissingServletRequestParameterException ex, HttpServletRequest request) {

        WebServiceResponse response = new WebServiceResponse(WebServiceResponse.Status.ERROR, WebServiceResponse.ErrorType.VALIDATION, ex.getMessage(), null, request, ex);

        response.logResponseToClientLog();

        return response.asModelAndView();
    }
}
