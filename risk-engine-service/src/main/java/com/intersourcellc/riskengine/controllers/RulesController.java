/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intersourcellc.riskengine.controllers;

import com.intersourcellc.*;
import com.intersourcellc.exception.RiskEngineApplicationException;
import com.intersourcellc.riskengine.config.EnvironmentConfig;
import com.intersourcellc.riskengine.engines.RiskEngineRuleExecutor;
import com.intersourcellc.riskengine.models.Parameter;
import com.intersourcellc.riskengine.models.Procedure;
import com.intersourcellc.riskengine.models.Request;
import com.intersourcellc.riskengine.models.*;
import com.intersourcellc.riskengine.repositories.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.*;
import javax.print.attribute.standard.Severity;
import org.apache.commons.collections.map.MultiValueMap;

/**
 * The Rules CRUD web service
 *
 * @author jimt
 */
@Controller
public class RulesController extends ExceptionHandlingController {

    final static Logger log = LoggerFactory.getLogger(RulesController.class);
    /**
     * The rules engine
     */
    @Autowired
    @Qualifier("ruleEngine")
    private RiskEngineRuleExecutor rulesEngine;
    /**
     * The rules repository
     */
    @Autowired
    private Rules rules;
    /**
     * The main environment configuration
     */
    @Autowired
    @Qualifier("environmentConfig")
    private EnvironmentConfig environmentConfig;
    /**
     * Procedure repository
     */
    @Autowired
    private ProcedureRepository procRepo;
    /**
     * Request repository
     */
    @Autowired
    private RequestRepository reqRepo;
    /**
     * Rule Variable Repository
     */
    @Autowired
    private RuleVariableRepository ruleVariableRepo;
    /**
     * Variable repository
     */
    @Autowired
    private VariableRepository variableRepo;
    /**
     * Parameter repository
     */
    @Autowired
    private ParameterRepository paramRepo;
    /**
     * RequestType repository
     */
    @Autowired
    private RequestTypeRepository requestTypeRepo;
    /**
     * Header repository
     */
    @Autowired
    private HeaderRepository headerRepo;

    /**
     * Returns a enum for a given string
     *
     * @param <T>    Enum Type
     * @param c      Enum Class
     * @param string the string to evaluate and from which to determine the enum value
     * @return Enum
     */
    public static <T extends Enum<T>> T getEnumFromString(Class<T> c, String string) throws RiskEngineApplicationException {
        if (c != null && string != null) {
            try {
                return Enum.valueOf(c, string.trim().toUpperCase());
            } catch (IllegalArgumentException ex) {
                throw new RiskEngineApplicationException("Enumeration information not available", ex);
            }
        } else {
            throw new RiskEngineApplicationException("Class type or the string is empty");
        }
    }

    /**
     * Return all rules
     */
    @RequestMapping(value = "/rules", produces = "application/json", method = RequestMethod.GET)
    @ResponseBody
    public List<Rule> findAll() {

        return rules.findAll();

    }

    /**
     * Return a single rules
     */
    @RequestMapping(value = "/rules/{ID}", produces = "application/json", method = RequestMethod.GET)
    @ResponseBody
    public Rule findById(@PathVariable("ID") Long ID) {

        return rules.findById(ID);

    }

    /**
     * Delete a rules
     */
    @RequestMapping(value = "/rules/{ID}", produces = "application/json", method = RequestMethod.DELETE)
    @ResponseBody
    public WebServiceResponse deleteById(@PathVariable("ID") Long ID) {

        Rule toDelete = new Rule();
        toDelete.setId(ID);
        rules.delete(toDelete);

        return new WebServiceResponse(WebServiceResponse.Status.OK, WebServiceResponse.ErrorType.NONE, "Rule deleted with id " + ID);
    }

    /**
     * Create a new rule object
     */
    @RequestMapping(value = "/rules", produces = "application/json", method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public Map<String, String> create(@RequestBody Map<String, String> requestObject) throws RiskEngineApplicationException {

        Rule rule = new Rule();


       /*Rally -TA352735 : TODO set all the rule attributes once tables are finalised*/
        rule.setName(requestObject.get("ruleName"));

        //Rally -TA352735 :Commented as the data model has been modified
//        rule.setRuleName(requestObject.get("ruleName"));
//        rule.setRuleDescription(requestObject.get("ruleDescription"));
//        rule.setRuleText(requestObject.get("ruleText"));
//        rule.setRuleInputParameters(requestObject.get("ruleInputParameters"));

        rules.create(rule);


        if (rule.getId() == null) throw new RiskEngineApplicationException("ID for new rule returned as null");
        Map<String, String> webServiceResponse = new HashMap<>();
        webServiceResponse.put("id", "" + rule.getId());
        return webServiceResponse;

    }

    /**
     * Update a rule object
     */
    @RequestMapping(value = "/rules", consumes = "application/json", produces = "application/json", method = RequestMethod.PUT)
    @ResponseBody
    public Object update(@RequestBody Map<String, Object> requestObject) {

        Rule rule = new Rule();
/* TODO DISABLE UPDATES FOR NOW!  It breaks fetchables
        Object idFromRequest = requestObject.get("id");
        if ( idFromRequest == null ) throw new Exception ( "ID not provided to update");
        long id;
        try {
            if ( idFromRequest instanceof Integer)
            {
                id = Long.valueOf((Integer)idFromRequest).longValue();
            }
            else  if ( idFromRequest instanceof String)
            {
                id = Long.valueOf((String)idFromRequest).longValue();

            }
            else
            {
                throw new Exception ( "Unknown ID data type " + idFromRequest.getClass().getName());
            }
        } catch (Exception e) {
            return new WebServiceResponse(WebServiceResponse.Status.ERROR, WebServiceResponse.ErrorType.API, "Bad or missing 'id' input parameter");
        }

        rule.setId(id);
        rule.setRuleName((String)requestObject.get("ruleName"));
        rule.setRuleDescription((String)requestObject.get("ruleDescription"));
        rule.setRuleText((String)requestObject.get("ruleText"));
        rule.setRuleInputParameters((String)requestObject.get("ruleInputParameters"));

        rules.update(rule);
*/
        Map<String, String> webServiceResponse = new HashMap<>();
        webServiceResponse.put("id", "" + rule.getId());
        return webServiceResponse;
    }

//    /**
//     *
//     * Execute a rule for a post request
//     *
//     */
//    @RequestMapping(value = "/rules/execute/{ID}",consumes = "application/json", produces = "application/json", method = RequestMethod.POST)
//    @ResponseBody
//    public Object executeRulesForPostRequest( @PathVariable ("ID") Long ID, HttpServletRequest servletRequest) throws Exception{
//        if(ID != null){
//            Factoid factoid = null;
//            StringBuilder sb = new StringBuilder();
//            BufferedReader reader = servletRequest.getReader();
//            try {
//                String line;
//                while ((line = reader.readLine()) != null) {
//                    sb.append(line).append(System.getProperty("line.separator"));
//                }
//            } finally {
//                reader.close();
//            }
//            List<Variable> variablesList = getVariablesGivenRuleId(ID);
//            Fact fact = new Fact(((Variable)variablesList.get(0)).getName(),sb.toString(),DataType.JSON );
//        }
//    }

    /**
     * Execute a rule for a  GET IDrequest
     */
    //FIXME: This method is too complex. It should be refactored into smaller methods to make it more readable (rmb)
    @RequestMapping(value = "/rules/execute/{ID}", consumes = "application/json", produces = "application/json", method = {RequestMethod.GET,
            RequestMethod.POST})
    @ResponseBody
    public Object executeRules(@PathVariable("ID") Long ID, HttpServletRequest servletRequest, @RequestBody Map<String,Object> postedBody) throws RiskEngineApplicationException {
          String result = "Success";
          
        
         Object response = null;
         ArrayList<String> messages = new ArrayList<String>();
           
        if (ID == null && servletRequest == null) {
            throw new RiskEngineApplicationException("The request ID and the request do not exist.");
        } else if (ID == null) {
            throw new RiskEngineApplicationException("The request ID does not exist.");
        } else if (servletRequest == null) {
            throw new RiskEngineApplicationException("The request does not exist.");
        }
        else {
            
            Rule toExecute;
            toExecute = rules.findById(ID);
            HashMap<String, String[]> queryRequestMap = null;
            HashMap<String, String> queryRequestMapToCoreRule = new HashMap<String, String>();
            processHeaders(servletRequest,queryRequestMapToCoreRule);
            
            // HACK THROWAWAY
            Map<String,Object> parent = (Map<String,Object>)postedBody.get("parent");
            if ( parent == null) throw new RiskEngineApplicationException("The parent entity in the posted JSON were not found");
            
            Map<String,Object> data= (Map<String,Object>)parent.get("data");
            if ( data == null) throw new RiskEngineApplicationException("The data entity in the posted JSON were not found");
              
            ArrayList<Map<String,Object>> devices= (ArrayList<Map<String,Object>>)data.get("devices");
            if ( devices == null) throw new RiskEngineApplicationException("The devices entity in the posted JSON were not found");
            if ( devices.size() < 1 ) throw new RiskEngineApplicationException("Enmpty devices array posted to JSON"); 
            
            for ( Map<String,Object> device : devices)
            {
                 Factoid factoid = null; 
                 factoid = new Factoid();
               
                String hostname = (String)device.get ("hostname");
                if ( hostname == null || hostname.length() == 0 ) throw new  RiskEngineApplicationException ( "hostname not defined for device");
                String preppedHostname = "devices.hostname like '" + hostname + "'";
                /*if ( devices == null )
                {
                    throw new RiskEngineApplicationException("The devices array was empty");
                }*/
                logger.debug ( "DEVICE IS: " + preppedHostname);

                com.intersourcellc.Procedure coreProcedure = null;
                
                factoid.addFact(new Fact("deviceName", preppedHostname, DataType.STRING ));
                queryRequestMapToCoreRule.put ("deviceName",preppedHostname);

        /*
                if (servletRequest.getMethod().equalsIgnoreCase("POST")) {
                    processPOSTRequest(servletRequest,factoid);
                }
                if (servletRequest.getParameterMap() != null && servletRequest.getMethod().equalsIgnoreCase("GET")) {
                    processGetRequest(servletRequest, queryRequestMap, queryRequestMapToCoreRule, factoid);

                }
            */    
                //FIXME: Needs work. Creating an Interface, we can make this mapping simple. (rmb)
                coreProcedure = updateCoreProcedure(toExecute, coreProcedure);
                //updates the factoid with the relevant data from database
                updateFactoidWithVariableData(factoid, ID, queryRequestMap);


                com.intersourcellc.CoreRule coreRule = new com.intersourcellc.CoreRule(toExecute.getName(), coreProcedure, factoid);
                if (queryRequestMapToCoreRule.size() > 0) {
                    coreRule.seedDataPoints(queryRequestMapToCoreRule);
                }

                rulesEngine.fulfillRequest(coreRule, servletRequest);
                log.debug("Rules controller executeRules fulfillment passed");
                rulesEngine.executeRule(coreRule);
                log.debug("Rules controller executeRules executed rules");
                
                // TODO Need to add support for multi-results in output
               String message = (String)factoid.getFact("message").value();
                if ( message.equals("Device not found"))
                {
                    messages.add("Device " + hostname + " was not found");
                    result = "Fail";
                }
                else if ( message.equals("Device was found"))
                {
                    messages.add("Device " + hostname + " was found");
                 
                }
                else 
                {
                    throw new  RiskEngineApplicationException("Unknown return message " + message);
                }
                   
               
            } 
        }
        
        // TODO NEED TO CHANGE EXCEPTION HANDLER TO USE PNP-COMPATIBLE FORMAT!
        HashMap<String,Object> toReturn = new HashMap<String,Object>();
        toReturn.put("status", "Finished");
        toReturn.put("result", result);
        toReturn.put("messages", messages);
       /* response = createResponseObject(factoid, ID );
        if(response != null){
            return response;
        }
        else{
            throw new RiskEngineApplicationException("Response from the engine not proper.");
        }*/
        
        return toReturn;
    }

    /**
     * @return the rules
     */
    public Rules getRules() {
        return rules;
    }

    /**
     * @param rules the rules to set
     */
    public void setRules(Rules rules) {
        this.rules = rules;
    }

    /**
     * @return the environmentConfig
     */
    public EnvironmentConfig getEnvironmentConfig() {
        return environmentConfig;
    }

    /**
     * @param environmentConfig the environmentConfig to set
     */
    public void setEnvironmentConfig(EnvironmentConfig environmentConfig) {
        this.environmentConfig = environmentConfig;
    }

    /**
     * @return the rulesEngine
     */
    public RiskEngineRuleExecutor getRulesEngine() {
        return rulesEngine;
    }

    /**
     * @param rulesEngine the rulesEngine to set
     */
    public void setRulesEngine(RiskEngineRuleExecutor rulesEngine) {
        this.rulesEngine = rulesEngine;
    }

    /**
     * @return the procRepo
     */
    public ProcedureRepository getProcRepo() {
        return procRepo;
    }

    /**
     * @param procRepo the procRepo to set
     */
    public void setProcRepo(ProcedureRepository procRepo) {
        this.procRepo = procRepo;
    }

    /**
     * @return the reqRepo
     */
    public RequestRepository getReqRepo() {
        return reqRepo;
    }

    /**
     * @param reqRepo the reqRepo to set
     */
    public void setReqRepo(RequestRepository reqRepo) {
        this.reqRepo = reqRepo;
    }

    /**
     * @return the ruleVariableRepo
     */
    public RuleVariableRepository getRuleVariableRepo() {
        return ruleVariableRepo;
    }

    /**
     * @param ruleVariableRepo the ruleVariableRepo to set
     */
    public void setRuleVariableRepo(RuleVariableRepository ruleVariableRepo) {
        this.ruleVariableRepo = ruleVariableRepo;
    }

    /**
     * @return the variableRepo
     */
    public VariableRepository getVariableRepo() {
        return variableRepo;
    }

    /**
     * @param variableRepo the variableRepo to set
     */
    public void setVariableRepo(VariableRepository variableRepo) {
        this.variableRepo = variableRepo;
    }

    /**
     * @return the paramRepo
     */
    public ParameterRepository getParamRepo() {
        return paramRepo;
    }

    /**
     * @param paramRepo the paramRepo to set
     */
    public void setParamRepo(ParameterRepository paramRepo) {
        this.paramRepo = paramRepo;
    }

    /**
     * Helper method which returns a rule id given a rule name
     *
     * @param ruleName the name of the rule
     * @return the id
     * @throws RiskEngineApplicationException
     */
    public Long getRuleIdForTheRuleName(String ruleName) {
        return getRules().findByName(ruleName);
    }

    /**
     * Returns a list of all requests given a rule ID
     *
     * @param ID rule id
     * @return list of requests
     * @throws RiskEngineApplicationException
     */
    public List<Request> getRequestsGivenRuleID(Long ID) {
        Long variableID;
        List<Request> requests;
        List<Request> requestFinalList = new ArrayList<>();
        List<RuleVariable> ruleVariables = getRuleVariableRepo().findByRuleId(ID);
        for (RuleVariable ruleVariable : ruleVariables) {
            variableID = ruleVariable.getCompoKey().getVariableId();
            requests = getReqRepo().findByVariableId(variableID);
            for (Request request : requests) {
                requestFinalList.add(request);
            }
        }
        return requestFinalList;
    }

    /**
     * Returns a list of all requests given a rule name
     *
     * @param RuleName the rule name
     * @return list of requests
     * @throws RiskEngineApplicationException
     */
    public List<Request> getRequestsGivenRuleName(String RuleName) {
        Long id = getRuleIdForTheRuleName(RuleName);
        log.debug("RulesController - getRequestsGivenRuleName -  request size ", getRequestsGivenRuleID(id).size());
        return getRequestsGivenRuleID(id);
    }

    /**
     * Returns a list of all procedures given a rule id
     *
     * @param ID the id
     * @return procedure
     * @throws RiskEngineApplicationException
     */
    public Procedure getProcedureGivenProcedureID(Long ID) {
        return getProcRepo().findById(ID);
    }

    /**
     * Returns a list of all procedures given a rule name
     *
     * @param RuleName the rule name
     * @return procedure
     * @throws RiskEngineApplicationException
     */
    public Procedure getProcedureGivenRuleName(String RuleName) {
        Long id = getRuleIdForTheRuleName(RuleName);
        return getProcedureGivenProcedureID(id);
    }

    /**
     * Returns list of variable given the rule id
     *
     * @param ID the id
     * @return list of variables
     * @throws RiskEngineApplicationException
     */
    public List<Variable> getVariablesGivenRuleId(Long ID) {
        Long variableID;
        List<Variable> variableFinalList = new ArrayList<>();
        List<RuleVariable> ruleVariables = getRuleVariableRepo().findByRuleId(ID);
        for (RuleVariable ruleVariable : ruleVariables) {
            variableID = ruleVariable.getCompoKey().getVariableId();
            variableFinalList.add(getVariableRepo().findById(variableID));
        }

        return variableFinalList;
    }

    /**
     * Returns list of variable given the rule name
     *
     * @param RuleName the rule name
     * @return list of variables
     * @throws RiskEngineApplicationException
     */
    public List<Variable> getVariablesGivenRuleName(String RuleName) {
        Long id = getRuleIdForTheRuleName(RuleName);
        return getVariablesGivenRuleId(id);
    }

    /**
     * Checks if a variable is a fetchable
     *
     * @param var the variable to evaluate for fetchability
     * @return true if fetchable and false otherwise
     * @throws RiskEngineApplicationException
     */
    public boolean isFetchable(Variable var) {
        VariableTypes varType = var.getVariableType();
        return varType.getName().equalsIgnoreCase(DataType.FETCHABLE.toString());
    }

    /**
     * Returns variable type for a given variable
     *
     * @param var the variable
     * @return the DataType
     * @throws RiskEngineApplicationException
     */
    public DataType getVarType(Variable var) {
        return getEnumFromString(DataType.class, var.getVariableType().getName());
    }

    /**
     * Retrieves list of parameters associated with the variable
     *
     * @param var the variable to evaluate
     * @return a list of parameters associated with the variable
     * @throws RiskEngineApplicationException
     */
    public List<Parameter> getParametersGivenVariable(Variable var) {
        List<Request> requests = getReqRepo().findByVariableId(var.getId());
        List<Parameter> params;
        List<Parameter> paramsFinalList = new ArrayList<>();
        for (Request request : requests) {
            params = getParamRepo().findByRequestId(request.getId());
            for (Parameter param : params) {
                paramsFinalList.add(param);
            }
        }
        return paramsFinalList;
    }

    /**
     * Retrieves list of requests associated with the variable
     *
     * @param var the variable
     * @return a list of requests associated with the variable
     * @throws RiskEngineApplicationException
     */
    public List<Request> getRequestsGivenVariable(Variable var) {
        return getReqRepo().findByVariableId(var.getId());
    }


//    /**
//     * Creates a HashMap with all the variables which are fetchables and the list
//     * of parameters associated with them.
//     * @param variablesList
//     * @return HashMap
//     * @throws RiskEngineApplicationException
//     */
//     public HashMap<Long, List<NameValuePair>> createMapForParamTokens(List<Variable> variablesList) {
//         HashMap<Long, List<NameValuePair>> map = new HashMap();
//         List<Parameter> params;
//         List<NameValuePair> nameValue =  new ArrayList();
//         NameValuePair pair;
//
//         for(Variable variable:variablesList){
//             if(isFetchable(variable)){
//                 log.debug("RulesController -createMapForParamTokens when there exists a fetchable");
//                params = getParametersGivenVariable(variable);
//                for(Parameter param: params){
//                    pair = new NameValuePair(param.getKey().trim(),param.getValue());
//                    nameValue.add(pair);
//                    log.debug("RulesController -createMapForParamTokens "+param.getId()+ "for variable id "+variable.getId());
//                     map.put(variable.getId(), nameValue);
//                }
//             }
//
//         }
//         return map;
//     }

    /**
     * @return the headerRepo
     */
    public HeaderRepository getHeaderRepo() {
        return headerRepo;
    }

    /**
     * @param headerRepo the headerRepo to set
     */
    public void setHeaderRepo(HeaderRepository headerRepo) {
        this.headerRepo = headerRepo;
    }

    /**
     * Creates a new header from the information retrieved from the database for
     * the given request id
     *
     * @param requestID the request id
     * @return a core header
     * @throws RiskEngineApplicationException
     */
    public List<com.intersourcellc.Header> getHeadersForTheGivenRequest(Long requestID) throws RiskEngineApplicationException {
        List<com.intersourcellc.riskengine.models.Header> headers;
        List<com.intersourcellc.Header> coreHeaders = new ArrayList<com.intersourcellc.Header>();
        com.intersourcellc.Header coreHeader = new com.intersourcellc.Header();
        headers = getHeaderRepo().findByRequestId(requestID);
        for (com.intersourcellc.riskengine.models.Header header : headers) {
            if (header.getKey() != null && header.getValue() != null) {
                coreHeader.addParameter(header.getToken(), header.getKey(), header.getValue());
                coreHeaders.add(coreHeader);
            }
        }
        return coreHeaders;
    }

    /**
     * Processes post request and creates a "requestBodyJson" which is then
     * added to a factoid.
     *
     * @param servletRequest
     * @param factoid
     */
    /* TODO NEEDS TO BE FIXED, TRYING TO READ FROM AN ALREADY CLOSED servletRequest.getReader() */
    protected void brokenProcessPOSTRequest(HttpServletRequest servletRequest, Factoid factoid) throws RiskEngineApplicationException {
        return;/*
        log.debug("Rules controller executeRules POST request");
        StringBuilder sb = new StringBuilder();
        try {
            if (servletRequest.getReader() != null) {
                try (BufferedReader reader = servletRequest.getReader()) {
                    String line;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line).append(System.getProperty("line.separator"));
                    }
                    if (!sb.toString().isEmpty()) {
                        Fact fact = new Fact("requestBodyJson", sb.toString(), DataType.JSON);
                        factoid.addFact(fact);
                    }
                } catch (IOException e) {
                    throw new RiskEngineApplicationException("Could not fetch information from the servlet request for a POST call ", e);
                }
            }
        } catch (IOException e) {
            throw new RiskEngineApplicationException("Unable to get servletRequest reader", e);
        }*/
    }

    /**
     * Processes post request and is then added to a factoid.
     *
     * @param servletRequest
     * @param queryRequestMap
     * @param queryRequestMapToCoreRule
     * @param factoid
     */
    protected void processGetRequest(HttpServletRequest servletRequest, HashMap<String, String[]> queryRequestMap, HashMap<String, String> queryRequestMapToCoreRule, Factoid factoid) {
        queryRequestMap = new HashMap<>(servletRequest.getParameterMap());
        //TODO: generate a map which only picks a single value instead of an array
        //This might be updated in the future sprints
        for (Map.Entry<String, String[]> entry : queryRequestMap.entrySet()) {
            String key = entry.getKey();
            String[] values = entry.getValue();
            if (values[0] != null) {
                queryRequestMapToCoreRule.put(key, values[0]);
            }
        }

        //Add fact for the data got from query string
        Fact factForQuery;
        if (queryRequestMap != null && queryRequestMapToCoreRule != null) {
            for (Map.Entry<String, String> entry : queryRequestMapToCoreRule.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                factForQuery = new Fact(key, value, DataType.STRING);
                factoid.addFact(factForQuery);
            }
            log.debug("Rules controller executeRules creating a fact for a GET request");
        }
    }

    /**
     * Creates a core procedure
     *
     * @param rule
     * @return
     * @throws RiskEngineApplicationException
     */
    protected com.intersourcellc.Procedure updateCoreProcedure(Rule rule,com.intersourcellc.Procedure  coreProcedure) throws RiskEngineApplicationException {
        Procedure procedure = rule.getProcedure();
        if (procedure == null) {
            throw new RiskEngineApplicationException("Procedure not available");
        }

        String engineTypeName = procedure.getEngineTypes().getName();
        if (engineTypeName == null) {
            throw new RiskEngineApplicationException("Engine Type not set");
        }

        com.intersourcellc.EngineType coreEngineType = getEnumFromString(EngineType.class, engineTypeName);
        if (coreEngineType == null) {
            throw new RiskEngineApplicationException("Unable to map Engine Type name to Enum");
        }

        coreProcedure = new com.intersourcellc.Procedure(procedure.getName(), procedure.getScript(), coreEngineType);
        return coreProcedure;
    }

    /**
     * Updates factoid for all the variables
     *
     * @param factoid
     * @param id
     * @param queryRequestMap
     * @param headerList
     * @throws RiskEngineApplicationException
     */
    protected void updateFactoidWithVariableData(Factoid factoid, Long id, HashMap<String, String[]> queryRequestMap) {
        //Get all the variables from the database and create factoid.
        //Every variable is added as a fact to the factoid.
        List<Variable> variablesList = getVariablesGivenRuleId(id);

        Fact fact;
        List<Parameter> params;
        com.intersourcellc.Request coreRequest = null;
        com.intersourcellc.Parameter coreParam;
        com.intersourcellc.RequestType coreRequestType;
        RequestTypes requestType;

        //If a variable is not a fetchable create a fact and put name and value
        //If its fetchable then get the request. add request as parameters and set request to a fact

        for (Variable var : variablesList) {
            if (isFetchable(var)) {
                log.debug("Rules controller executeRules creating a fact when fetchable: start");
                params = getParametersGivenVariable(var);
                log.debug("Found {} fetchable parameter(s)", params.size());
                fact = new Fact(var.getName(), var.getValue(), DataType.FETCHABLE);
                List<Request> requests = getRequestsGivenVariable(var);
                for (Request request : requests) {
                    if (request != null) {
                        requestType = requestTypeRepo.findById(request.getRequest_type_id());
                        coreRequestType = getEnumFromString(RequestType.class, requestType.getName());
                        coreRequest = new com.intersourcellc.Request(coreRequestType, "", request.getUrl());

                        //generates a header for the given request id
                        if (request.getId() != null) {
                            List<com.intersourcellc.Header> coreHeaders = getHeadersForTheGivenRequest(request.getId());
                            for (com.intersourcellc.Header coreHeader : coreHeaders) {
                                coreRequest.addHeader(coreHeader);
                            }
                        }
                        fact.setRequest(coreRequest);
                        factoid.addFact(fact);
                    }
                }
                log.debug("Rules controller executeRules creating a fact when fetchable: generating a fact");
                for (Parameter param : params) {
                    coreParam = new com.intersourcellc.Parameter(param.getToken(), param.getKey(), param.getValue());
                    if (queryRequestMap != null && !queryRequestMap.isEmpty()) {
                        coreParam.applyQueryRequestMapTokens(queryRequestMap);
                    }
                    coreRequest.addParameter(coreParam);
                }
            } else {
                fact = new Fact(var.getName(), var.getValue(), getVarType(var));
                factoid.addFact(fact);
                log.debug("Rules controller executeRules creating a fact when not fetchable: generating a fact");
            }
        }
    }

    /**
     * Processes headers
     *
     * @param servletRequest
     * @param headerList
     * @return
     */
    protected HashMap<String, String> processHeaders(HttpServletRequest servletRequest, HashMap<String, String> queryRequestMapToCoreRule) {
        Enumeration headerNames = servletRequest.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = (String) headerNames.nextElement();
            queryRequestMapToCoreRule.put(headerName, servletRequest.getHeader(headerName));
        }
        return queryRequestMapToCoreRule;
    }

    /**
     * Updates the response object to be shown to the client
     *
     * @param factoid
     * @param id
     * @return
     * @throws RiskEngineApplicationException
     */
    protected Object createResponseObject(Factoid factoid, Long id) {
        if(factoid.getFact("output") != null){
            return factoid.getFact("output") ;
        }
        
        //Below code needs to be deleted once the rules engine returns a fact called "output" as response
        HashMap<String, Object> toReturn = new HashMap<>();
        if (factoid != null) {
            if (id == 2) {
                Fact result = factoid.getFact("result");
                Boolean success = Boolean.valueOf("" + result.value());

                toReturn.put("status", "Finished");
                String message;
                if (success) {
                    message = "Success";
                } else {
                    message = "Fail";
                }

                toReturn.put("result", message);
                ArrayList<String> messages = new ArrayList<>();
                messages.add("Strings compared");
                toReturn.put("messages", messages);
                log.debug("Bundling the response");
                return toReturn;
            } else if (id == 6) {
                Fact result = factoid.getFact("result");
                Boolean success = Boolean.valueOf("" + result.value());
                String message;
                ArrayList<String> messages = new ArrayList<>();
                
                message = "Success";
                messages.add((String)factoid.getFact("message").value());
                
                toReturn.put("messages", messages);
                toReturn.put("result", message);
                return toReturn;

            } else {
                return factoid;
            }
        }
        return null;
    }
}
