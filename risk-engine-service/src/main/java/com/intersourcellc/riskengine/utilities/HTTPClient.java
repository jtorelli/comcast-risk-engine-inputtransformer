/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intersourcellc.riskengine.utilities;

import com.intersourcellc.Header;
import com.intersourcellc.Parameter;
import com.intersourcellc.exception.RiskEngineApplicationException;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.slf4j.LoggerFactory;
/**
 *
 * @author davis
 */
@Component
public class HTTPClient {
    final static org.slf4j.Logger logger = LoggerFactory.getLogger(HTTPClient.class);
        
    public String sendGet (String url, List<com.intersourcellc.Header> headers) {
        String content = "";
        try
        {
            //HttpClient client = new DefaultHttpClient();
            //Make sure the client works with SSL
            HttpClient client = getNewHttpClient();
            HttpGet request = new HttpGet(url);
            request.addHeader("accept", "application/json");
            
                // TODO FIXME HACK
                request.setHeader ( "authuser", "jtorel001c");
                request.setHeader ( "authpass","Winter12");
            
                /* THIS IS A BUG!  You can't retransmit the request-specific headers 
                 if (! headers.isEmpty()) {
                    for (Header header : headers) 
                    {
                        for (Map.Entry<String, Parameter> entry : header.entrySet()) {
                            request.addHeader(entry.getValue().getKey(), entry.getValue().getValue());
                        }
                    }
                }*/
                
            HttpResponse response = client.execute(request);

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatusLine().getStatusCode());
            }
            // Get the response
            BufferedReader rd = new BufferedReader
              (new InputStreamReader(response.getEntity().getContent()));
                String line = "";
                while ((line = rd.readLine()) != null) {
                    content += line;
                  }
                logger.debug("Http Get Response: ", content);
        } catch (ClientProtocolException e) {
            throw new RiskEngineApplicationException("HTTP Get client protocol error:", e);
        } catch (IOException e) {
            throw new RiskEngineApplicationException("HTTP Get client IO error:", e);
        }
        return content;
    }
    
    public String buildUrl(String urlBase, List<NameValuePair> nameValuePairs) {
        String finalUrl = "";
        try {
            URIBuilder url = new URIBuilder(urlBase);     
            for (NameValuePair nameValuePair : nameValuePairs) {
                url.addParameter(nameValuePair.getName(), nameValuePair.getValue());
            }
            finalUrl = url.toString();
        } catch (URISyntaxException ex) {
            throw new RiskEngineApplicationException("HTTP url error:", ex);
        }
        return finalUrl;
        }
        
        public String buildUrl(String urlBase, HashMap<String, com.intersourcellc.Parameter> params) {
            String finalUrl = "";
            try {
                URIBuilder url = new URIBuilder(urlBase);   

                Iterator<String> keySetIterator = params.keySet().iterator();
                while (keySetIterator.hasNext()) {
                    String key = keySetIterator.next();
                    com.intersourcellc.Parameter param = params.get(key);
                    url.addParameter(key.toString(), param.getValue());
                }
                finalUrl = url.toString();
            } catch (URISyntaxException ex) {
                throw new RiskEngineApplicationException("HTTP url error:", ex);
            }
            return finalUrl;
        }
        
       public String sendPost (String url, List<com.intersourcellc.Header> headers, String postBody) {
            String result = "";
            try
            {
                //HttpClient client = new DefaultHttpClient();
                //Make sure the client works with SSL
                HttpClient client = getNewHttpClient();
                HttpPost postRequest = new HttpPost(url);
                
                // set the Http Post body
                StringEntity se = new StringEntity(postBody);
                postRequest.setEntity(se);
                
                // add header
                postRequest.addHeader("accept", "application/json");
                postRequest.setHeader("Content-type", "application/json");
                
                // TODO FIXME HACK
                postRequest.setHeader ( "authuser", "jtorel001c");
                postRequest.setHeader ( "authpass","Winter12");
                           /* THIS IS A BUG!  You can't retransmit the request-specific headers 
                if (! headers.isEmpty()) {
                    for (Header header : headers) 
                    {
                        for (Map.Entry<String, Parameter> entry : header.entrySet()) {
                            postRequest.addHeader(entry.getValue().getKey(), entry.getValue().getValue());
                        }
                    }
                }*/
                
                // add parameters
//                if (! header.isEmpty()) {
//                    ArrayList<NameValuePair> postParams = new ArrayList<NameValuePair>();
//                    for (Map.Entry<String, String> entry : header.entrySet()) {
//                        postParams.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
//                    }
//                    postRequest.setEntity(new UrlEncodedFormEntity(postParams));
//                }
                
                HttpResponse response = client.execute(postRequest);

                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new RuntimeException("Failed : HTTP Post error code : "
                            + response.getStatusLine().getStatusCode());
                }
                // Get the response
                BufferedReader rd = new BufferedReader
                  (new InputStreamReader(response.getEntity().getContent()));
                    String line;
                    while ((line = rd.readLine()) != null) {
                        System.out.println(line);
                        result += line;
                      }
            } catch (ClientProtocolException e) {
                throw new RiskEngineApplicationException("Fulfill HTTP request failed on client protocol exception:", e);
            } catch (IOException e) {
              throw new RiskEngineApplicationException("Fulfill HTTP request failed on IO exception:", e);
            }
            return result;
        }
        
    
    public HttpClient getNewHttpClient(int httpPort, int httpsPort) {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);

            SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

            SchemeRegistry registry = new SchemeRegistry();
            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), httpPort));
            registry.register(new Scheme("https", sf, httpsPort));

            return new DefaultHttpClient(ccm, params);
        } catch (Exception e) {
            throw new RiskEngineApplicationException("HTTP Client failed initialization:", e);
        }
    }
    
        public HttpClient getNewHttpClient() {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);

            SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

            SchemeRegistry registry = new SchemeRegistry();
            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            return new DefaultHttpClient(ccm, params);
        } catch (Exception e) {
            throw new RiskEngineApplicationException("HTTP Client failed initialization:", e);
        }
    }
    
        
    public class MySSLSocketFactory extends SSLSocketFactory {
    SSLContext sslContext = SSLContext.getInstance("TLS");

        public MySSLSocketFactory(KeyStore truststore) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
            super(truststore);

            TrustManager tm = new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            };

            sslContext.init(null, new TrustManager[] { tm }, null);
        }

        @Override //deprecated
        public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException, UnknownHostException {
            return sslContext.getSocketFactory().createSocket(socket, host, port, autoClose);
        }
        @Override
        public Socket createSocket() throws IOException {
            return sslContext.getSocketFactory().createSocket();
        }
    }
}
