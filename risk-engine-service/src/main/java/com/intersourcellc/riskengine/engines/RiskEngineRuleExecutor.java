/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intersourcellc.riskengine.engines;

import RiskAssessmentCore.IRuleEngine;
import RiskAssessmentCore.RuleEngineProvider;
import com.intersourcellc.*;
import com.intersourcellc.riskengine.utilities.HTTPClient;
import java.net.URLEncoder;

import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author jimt
 */
@Component
public class RiskEngineRuleExecutor {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    @Qualifier("httpClient")
    private HTTPClient httpClient;

    public Factoid executeRule(CoreRule coreRule) {

        RuleEngineProvider provider = new RuleEngineProvider(coreRule);
        IRuleEngine engine = provider.getEngine();

        return engine.Run();
    }

    public void fulfillRequest(CoreRule rule, HttpServletRequest servletRequest) {

       
        HashMap<String, Fact> facts = rule.getFactoid().getFetchables();
        for (String key : facts.keySet()) {
            Fact fact = facts.get(key);
            Request req = fact.getRequest();
      
            // TODO FIXME HACKING THE DEVICE NAME INTO THE URL BELOW
           // req.setUrl( req.getUrl()  + "?" + URLEncoder.encode("where=devices.hostname like '" + servletRequest.getParameter("deviceName") +"'"));
      
            // TODO HACK FOR HEADER SUPPORT
            String serverHeader = servletRequest.getHeader("cnmdb_url");
            if ( serverHeader != null && serverHeader.length() > 0 )
            {
                logger.warn ( "OVERRIDING DEFAULT CNMAPI SERVER[" + req.getUrl() + "]: Redirecting to " + serverHeader);
                req.setUrl(serverHeader + "/common/devices/find/");
            }
            
            String fetResult = "";
            if (req.getRequestType() == RequestType.GET) {
                String reqUrl = req.getUrl();
                HashMap<String, Parameter> reqParameters = req.getParameters();
                String url = getHttpClient().buildUrl(reqUrl, reqParameters);
                fetResult = getHttpClient().sendGet(url, req.getHeaders());
            } else if (req.getRequestType() == RequestType.POST) {
                fetResult = getHttpClient().sendPost(req.getUrl(), req.getHeaders(), req.getBody());
            }
            logger.debug("Got request result for " + req.getUrl() + " [ " + fetResult + "]");
            //fact.setValue(fetResult);
            fact.setValueType(fetResult, DataType.FETCHABLE);
        }
    }

    /**
     * @return the httpClient
     */
    public HTTPClient getHttpClient() {
        return httpClient;
    }

    /**
     * @param httpClient the httpClient to set
     */
    public void setHttpClient(HTTPClient httpClient) {
        this.httpClient = httpClient;
    }

}
