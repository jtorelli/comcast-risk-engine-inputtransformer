/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intersourcellc.riskengine.models;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author soumya
 */
@Entity
@Table(name = "re_request_type")
public class RequestTypes implements Serializable{
    
    @Id //signifies the primary key
    @Column(name = "id", nullable = false, length = 11)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    
    @Column(name = "name", length = 45)
    private String name;

    public RequestTypes() {
    }

    public RequestTypes(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    
}
