/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intersourcellc.riskengine.models;

/**
 *
 * @author jimt
 */
public interface EntityWithIdentity {
 
    public Long getId();
}
