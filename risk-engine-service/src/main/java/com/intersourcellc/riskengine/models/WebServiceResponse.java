/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intersourcellc.riskengine.models;

import java.util.*;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJacksonJsonView;

/**
 *
 * Used to serialize/provide responses for web services
 *
 * @author jimt
 */
public class WebServiceResponse {

    // TODO: Implement the "client" log in logback.xml
    /**
     * A reference to the client logger
     */
    protected final Logger clientLog = LoggerFactory.getLogger("client");
    /**
     * The status of the operation
     */
    private Status status = Status.UNKNOWN;
    /**
     * Type type of error
     */
    private ErrorType errorType = ErrorType.NONE;
    /**
     * The detail message, if provided
     */
    private List<String> detailMessages = new ArrayList<String>();
    /**
     * The list of key/value pairs
     */
    private Map<String, String> fields = new LinkedHashMap<String, String>();
    /**
     * A reference to the main exception object, which may contain nested exceptions
     */
    private Exception exception = null;
    /**
     * The URL that was called
     */
    private String calledURL;
    /**
     * The original request method
     */
    private String requestMethod;
    /**
     * The logged in user
     */
    private String loggedInUser;
    /**
     * The logged-in user roles
     */
    private List<String> userRoles = new ArrayList<String>();

    /**
     * @return the status
     */
    public Status getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Status status) {
        this.status = status;
    }

    /**
     * @return the detailMessages
     */
    public List<String> getDetailMessages() {
        return detailMessages;
    }

    /**
     * @param detailMessages the detailMessages to set
     */
    public void setDetailMessages(List<String> detailMessages) {
        this.detailMessages = detailMessages;
    }

    /**
     * Add a new detail message
     */
    public void addDetailMessage(String message) {
        if (message != null) {
            this.detailMessages.add(message);
        }
    }

    /**
     * @return the errorType
     */
    public ErrorType getErrorType() {
        return errorType;
    }

    /**
     * @param errorType the errorType to set
     */
    public void setErrorType(ErrorType errorType) {
        this.errorType = errorType;
    }

    /**
     * @return the calledURL
     */
    public String getCalledURL() {
        return calledURL;
    }

    /**
     * @param calledURL the calledURL to set
     */
    public void setCalledURL(String calledURL) {
        this.calledURL = calledURL;
    }

    /**
     * @return the requestMethod
     */
    public String getRequestMethod() {
        return requestMethod;
    }

    /**
     * @param requestMethod the requestMethod to set
     */
    public void setRequestMethod(String requestMethod) {
        this.requestMethod = requestMethod;
    }

    /**
     * @return the loggedInUser
     */
    public String getLoggedInUser() {
        return loggedInUser;
    }

    /**
     * @param loggedInUser the loggedInUser to set
     */
    public void setLoggedInUser(String loggedInUser) {
        this.loggedInUser = loggedInUser;
    }

    /**
     * @return the userRoles
     */
    public List<String> getUserRoles() {
        return userRoles;
    }

    /**
     * @param userRoles the userRoles to set
     */
    public void setUserRoles(List<String> userRoles) {
        this.userRoles = userRoles;
    }

    
    /**
     * Add a user role
     */
    public void addUserRole(String r) {
        if (r != null) {
            userRoles.add(r);
        }
    }

    public ModelAndView asModelAndView() {
        MappingJacksonJsonView jsonView = new MappingJacksonJsonView();
        Map<String, WebServiceResponse> model = new HashMap<String, WebServiceResponse>();
        model.put("response", this);

        return new ModelAndView(jsonView, model);
    }

    /**
     * @return the exception
     */
    public Exception getException() {
        return exception;
    }

    /**
     * @param exception the exception to set
     */
    public void setException(Exception exception) {
        this.exception = exception;
    }

    /**
     * @return the fields
     */
    public Map<String, String> getFields() {
        return fields;
    }

    /**
     * @param fields the fields to set
     */
    public void setFields(Map<String, String> fields) {
        this.fields = fields;
    }

    /**
     * The response status
     */
    public enum Status {

        UNKNOWN, OK, ERROR
    };

    /**
     * The error type
     */
    public enum ErrorType {

        NONE, VALIDATION, EXTERNAL, API, UNHANDLED
    }

     /**
     * Builds a response objeSct
     */
    public WebServiceResponse(Status status, ErrorType errorType, String detailMessage) {

        ArrayList<String> messages = new ArrayList<String>();

        if (detailMessage != null) {
            messages.add(detailMessage);
        }

        initObject(status, errorType, messages, null,null,null);


    }
    
    
    
    /**
     * Builds a response objeSct
     */
    public WebServiceResponse(Status status, ErrorType errorType, String detailMessage, Map<String, String> fieldErrors, HttpServletRequest request, Exception exception) {

        ArrayList<String> messages = new ArrayList<String>();

        if (detailMessage != null) {
            messages.add(detailMessage);
        }

        initObject(status, errorType, messages, fieldErrors, request, exception);


    }

    /**
     * Builds a response object
     */
    public WebServiceResponse(Status status, ErrorType errorType, List<String> detailMessage, Map<String, String> fieldErrors, HttpServletRequest request, Exception exception) {

        initObject(status, errorType, detailMessage, fieldErrors, request, exception);

    }

    private void initObject(Status status, ErrorType errorType, List<String> detailMessage, Map<String, String> fields, HttpServletRequest request, Exception exception) {

        this.setStatus(status);
        this.setErrorType(errorType);

        // Detail messages
        if (detailMessage != null) {
            this.setDetailMessages(detailMessage);
        }

        // Error info
        this.setException(exception);
        this.setFields(fields);

        // Set the request-based info 
        setRequestInfo(request);

    }

    public void setRequestInfo(HttpServletRequest request) {


        if (request != null) {
            // Information about the request
            this.setCalledURL(request.getRequestURL().toString());
            this.setRequestMethod(request.getMethod());

            if (request.getUserPrincipal() != null) {
                // Set info on the logged in users permisions
                this.setLoggedInUser(request.getUserPrincipal().getName());
                this.addUserRole("ERROR TODO: IMPLEMENT");

            }

        }

    }

    public void logResponseToClientLog() {
        StringBuilder logMessage = new StringBuilder();

        logMessage.append(status.toString()).append("/").append(errorType.toString()).append(" ");

        // Add the User/URL info
        logMessage.append("[").append(this.requestMethod).append(" ").append(this.loggedInUser).append("@").append(this.calledURL).append("] ");

        // Add the detail messages
        if (detailMessages != null) {
            for (String message : detailMessages) {
                logMessage.append("[").append(message).append("]");
            }

            logMessage.append(" ");
        }

        // Add the field/object level errors
        if (fields != null) {
            for (String key : fields.keySet()) {
                logMessage.append("[").append(key).append(": ").append(fields.get(key)).append("]");
            }

            logMessage.append(" ");
        }

        // Log validation messages with a level of warn, so they can be filtered/turned off
        if (errorType.equals(ErrorType.VALIDATION)) {
            clientLog.warn(logMessage.toString());
        } else {
            clientLog.error(logMessage.toString());
        }
    }
}
