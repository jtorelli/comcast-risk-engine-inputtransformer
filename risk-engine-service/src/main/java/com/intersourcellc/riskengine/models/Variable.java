/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intersourcellc.riskengine.models;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author soumya
 */
@Entity
@Table(name = "re_variable")
public class Variable implements Serializable{
    @Id //signifies the primary key
    @Column(name = "id", nullable = false, length = 11)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column(name = "variable_name", length = 45)
    private String name;
    
    @Column(name = "variable_type_id", length = 11, insertable = false, updatable = false)
    private Long type_id;
    
    @Column(name = "variable_value", length = 500)
    private String value;
    
    @ManyToOne
    @JoinColumn(name = "variable_type_id", referencedColumnName="id")
    private VariableTypes variableType;

    public Variable() {
    }

    public Variable(Long id, String name, Long type_id, String value, VariableTypes variableType) {
        this.id = id;
        this.name = name;
        this.type_id = type_id;
        this.value = value;
        this.variableType = variableType;
    }
    
    public Variable(Long id, String name, Long type_id, String value) {
        this.id = id;
        this.name = name;
        this.type_id = type_id;
        this.value = value;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the type_id
     */
    public Long getType_id() {
        return type_id;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @return the variableType
     */
    public VariableTypes getVariableType() {
        return variableType;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param type_id the type_id to set
     */
    public void setType_id(Long type_id) {
        this.type_id = type_id;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @param variableType the variableType to set
     */
    public void setVariableType(VariableTypes variableType) {
        this.variableType = variableType;
    }
}
