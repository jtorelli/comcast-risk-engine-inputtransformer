/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intersourcellc.riskengine.models;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author soumya
 */
@Entity
@Table(name = "re_rule_variable")
public class RuleVariable implements Serializable{
    
    @EmbeddedId
    private RuleVariableCompositeKey compoKey;

    /**
     * @return the compoKey
     */
    public RuleVariableCompositeKey getCompoKey() {
        return compoKey;
    }

    /**
     * @param compoKey the compoKey to set
     */
    public void setCompoKey(RuleVariableCompositeKey compoKey) {
        this.compoKey = compoKey;
    }

}
