/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intersourcellc.riskengine.models;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author soumya
 */
@Embeddable
public class RuleVariableCompositeKey implements Serializable {
    
    
    public RuleVariableCompositeKey() {
    }

    public RuleVariableCompositeKey(final Long rule_id, final Long variable_id) {
        this.ruleId = rule_id;
        this.variableId = variable_id;
    }

    @Column(name = "rule_id", nullable = false, length = 11)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long ruleId;
    
    @Column(name = "variable_id", nullable = false, length = 11)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long variableId;

   
    /**
     * @return the ruleId
     */
    public Long getRuleId() {
        return ruleId;
    }

    /**
     * @return the variableId
     */
    public Long getVariableId() {
        return variableId;
    }

    /**
     * @param ruleId the ruleId to set
     */
    public void setRuleId(Long ruleId) {
        this.ruleId = ruleId;
    }

    /**
     * @param variableId the variableId to set
     */
    public void setVariableId(Long variableId) {
        this.variableId = variableId;
    }
    
}
