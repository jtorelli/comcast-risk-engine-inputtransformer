package com.intersourcellc.riskengine.models;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Represents a single header key value pair.
 * @author davis
 */
@Entity
@Table(name = "re_header")
public class Header implements Serializable{
    @Id //signifies the primary key
    @Column(name = "id", nullable = false, length = 11)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column(name = "request_id", length = 11, insertable = false, updatable = false)
    private Long request_id;

    @Column(name = "`token`" , length = 50)
    private String token;
    
    @Column(name = "`key`" , length = 50)
    private String key;
    
    @Column(name = "value", length = 500)
    private String value;
    
    @ManyToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinColumn(name = "request_id", referencedColumnName="id")
    private Request request;

    public Header() {
    }

    public Header(Long id, Long request_id, String token, String key, String value, Request request) {
        this.id = id;
        this.request_id = request_id;
        this.token = token;
        this.key = key;
        this.value = value;
        this.request = request;
    }
    
    public Header(Long id, Long request_id, String token, String key, String value) {
        this.id = id;
        this.request_id = request_id;
        this.token = token;
        this.key = key;
        this.value = value;
    }
    

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @return the request_id
     */
    public Long getRequest_id() {
        return request_id;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @return the request
     */
    public Request getRequest() {
        return request;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @param request_id the request_id to set
     */
    public void setRequest_id(Long request_id) {
        this.request_id = request_id;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @param request the request to set
     */
    public void setRequest(Request request) {
        this.request = request;
    }

     /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }
    
    /**
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * @param key the key to set
     */
    public void setKey(String key) {
        this.key = key;
    }

}
