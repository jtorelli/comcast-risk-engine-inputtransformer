/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intersourcellc.riskengine.models;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author soumya
 */
@Entity
@Table(name = "re_request")
public class Request implements Serializable{
    @Id //signifies the primary key
    @Column(name = "id", nullable = false, length = 11)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column(name = "variable_id", length = 11, insertable = false, updatable = false)
    private Long variable_id;
    
    @Column(name = "request_type_id", length = 11, insertable = false, updatable = false)
    private Long request_type_id;
    
    @Column(name = "url", length = 2048)
    private String url;
    
    @ManyToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinColumn(name = "request_type_id", referencedColumnName="id")
    private RequestTypes requestType;
    
    @ManyToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinColumn(name = "variable_id", referencedColumnName="id")
    private Variable variable;

    public Request() {
    }

    public Request(Long id, Long variable_id, Long request_type_id, String url, RequestTypes requestType, Variable variable) {
        this.id = id;
        this.variable_id = variable_id;
        this.request_type_id = request_type_id;
        this.url = url;
        this.requestType = requestType;
        this.variable = variable;
    }
    
    public Request(Long id, Long variable_id, Long request_type_id, String url) {
        this.id = id;
        this.variable_id = variable_id;
        this.request_type_id = request_type_id;
        this.url = url;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @return the variable_id
     */
    public Long getVariable_id() {
        return variable_id;
    }

    /**
     * @return the request_type_id
     */
    public Long getRequest_type_id() {
        return request_type_id;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @return the requestType
     */
    public RequestTypes getRequest() {
        return requestType;
    }

    /**
     * @return the variable
     */
    public Variable getVariable() {
        return variable;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @param variable_id the variable_id to set
     */
    public void setVariable_id(Long variable_id) {
        this.variable_id = variable_id;
    }

    /**
     * @param request_type_id the request_type_id to set
     */
    public void setRequest_type_id(Long request_type_id) {
        this.request_type_id = request_type_id;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @param requestType the requestType to set
     */
    public void setRequest(RequestTypes requestType) {
        this.requestType = requestType;
    }

    /**
     * @param variable the variable to set
     */
    public void setVariable(Variable variable) {
        this.variable = variable;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("Id: ");
        stringBuilder.append(String.valueOf(id));
        stringBuilder.append(", Variable Id: ");
        stringBuilder.append(String.valueOf(variable_id));
        stringBuilder.append(", Request Type Id: ");
        stringBuilder.append(String.valueOf(request_type_id));
        stringBuilder.append(", Url: ");
        stringBuilder.append(url == null ? "NULL" : url);
        stringBuilder.append(", Request Type: ");
        stringBuilder.append(requestType == null ? "NULL" : requestType);
        stringBuilder.append(", Variable: ");
        stringBuilder.append(variable == null ? "NULL" : variable);

        return stringBuilder.toString();
    }
    
}
