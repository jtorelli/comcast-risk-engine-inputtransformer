/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intersourcellc.riskengine.models;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author soumya
 */
@Entity
@Table(name = "re_procedure")
public class Procedure implements Serializable {

    @Id //signifies the primary key
    @Column(name = "id", nullable = false, length = 11)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name", length = 50)
    private String name;

    @Column(name = "script")
    @Lob // Defines this field as a large object to the DB
    private String script;

    @Column(name = "engine_type_id", length = 11, insertable = false, updatable = false)
    private Long engineTypeId;

    // BUGBUG: Seems dangerous to use cascading when configuring the database; TBD. (rmb)
    @ManyToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinColumn(name = "engine_type_id", referencedColumnName = "id")
    private EngineTypes engineTypes;

    public Procedure() {
    }

    public Procedure(Long id, String name, String script, Long engineTypeId, EngineTypes engineTypes) {
        this.id = id;
        this.name = name;
        this.script = script;
        this.engineTypeId = engineTypeId;
        this.engineTypes = engineTypes;
    }
    
    public Procedure(Long id, String name, String script, Long engineTypeId) {
        this.id = id;
        this.name = name;
        this.script = script;
        this.engineTypeId = engineTypeId;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the script
     */
    public String getScript() {
        return script;
    }

    /**
     * @return the engineTypes
     */
    public EngineTypes getEngineTypes() {
        return engineTypes;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param script the script to set
     */
    public void setScript(String script) {
        this.script = script;
    }

    /**
     * @param engineTypes the engineTypes to set
     */
    public void setEngineTypes(EngineTypes engineTypes) {
        this.engineTypes = engineTypes;
    }

    /**
     * @return the engineTypeId
     */
    public Long getEngineTypeId() {
        return engineTypeId;
    }

    /**
     * @param engineTypeId the engineTypeId to set
     */
    public void setEngineTypeId(Long engineTypeId) {
        this.engineTypeId = engineTypeId;
    }

}
