/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intersourcellc.riskengine.models;


import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author soumya
 */
@Entity
@Table(name = "re_rule")
public class Rule implements EntityWithIdentity, Serializable {
    @Id //signifies the primary key
    @Column(name = "id", nullable = false, length = 11)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "rule_name", length = 45)
    private String name;
    
    @Column(name = "procedure_id", length = 11, insertable = false, updatable = false)
    private Long procedureId;
    
    @ManyToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinColumn(name = "procedure_id", referencedColumnName="id")
    private Procedure procedure;

    public Rule() {
    }

    public Rule(Long id, String name, Long procedureId, Procedure procedure) {
        this.id = id;
        this.name = name;
        this.procedureId = procedureId;
        this.procedure = procedure;
    }
    
     public Rule(Long id, String name, Long procedureId) {
        this.id = id;
        this.name = name;
        this.procedureId = procedureId;
    }
    
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the procedure
     */
    public Procedure getProcedure() {
        return procedure;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param procedure the procedure to set
     */
    public void setProcedure(Procedure procedure) {
        this.procedure = procedure;
    }

    /**
     * @return the procedureId
     */
    public Long getProcedureId() {
        return procedureId;
    }

    /**
     * @param procedureId the procedureId to set
     */
    public void setProcedureId(Long procedureId) {
        this.procedureId = procedureId;
    }

    
}
