/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intersourcellc.riskengine.repositories;

import com.intersourcellc.exception.RiskEngineApplicationException;
import com.intersourcellc.riskengine.models.Header;
import java.util.List;
import javax.persistence.TypedQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author soumya
 */
@Repository
public class HeaderRepository extends BasicRepository<Header>{
   
    
    /**
     * The type of class upon which to operate
     */
    private Class<Header> classType;

    /**
     * The name of the entity class, representing a table, upon which to operate
     */
    private String className;
    
     public HeaderRepository() {
        super ( Header.class);
    }
    
    @Transactional
    public List<Header> findByRequestId(Long id) throws RiskEngineApplicationException {
        
        classType = Header.class;
        className = classType.getName();

        if (className.contains(".")) {
            className = className.substring(className.lastIndexOf(".") + 1);
        }
        TypedQuery query = entityManager.createQuery("from " + className + " where request_id = " + id, classType);
        if ( query.getResultList() == null ) throw new RiskEngineApplicationException( "No record found for ID " + id);
        List<Header> toReturn = query.getResultList();
        return toReturn;
    }
    
}
