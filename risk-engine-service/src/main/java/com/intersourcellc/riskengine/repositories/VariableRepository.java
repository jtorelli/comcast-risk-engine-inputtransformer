 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intersourcellc.riskengine.repositories;

import com.intersourcellc.riskengine.models.Variable;
import org.springframework.stereotype.Repository;

/**
 *
 * @author soumya
 */
@Repository
public class VariableRepository extends BasicRepository<Variable>{
    public VariableRepository() {
        super ( Variable.class);
    }
}
