/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intersourcellc.riskengine.repositories;

import com.intersourcellc.riskengine.models.Procedure;
import org.springframework.stereotype.Repository;

/**
 *
 * @author soumya
 */
@Repository
public class ProcedureRepository extends BasicRepository<Procedure>{
    public ProcedureRepository () {
        super ( Procedure.class);
    }
    
}
