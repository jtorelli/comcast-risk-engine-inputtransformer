/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intersourcellc.riskengine.repositories;

import com.intersourcellc.exception.RiskEngineApplicationException;
import com.intersourcellc.riskengine.models.Rule;
import java.util.List;
import javax.persistence.TypedQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * The CRUD interface to the Rule entity
 *
 * @author jimt
 */
@Repository
public class Rules extends BasicRepository<Rule> {
    
    /**
     * The type of class upon which to operate
     */
    private Class<Rule> classType;

    /**
     * The name of the entity class, representing a table, upon which to operate
     */
    private String className;

    public Rules () {
        super ( Rule.class);
    }
    
    /**
     * Return the specified element
     */
    @Transactional
    public Long findByName(String ruleName) throws RiskEngineApplicationException {

        classType = Rule.class;
        className = classType.getName();

        if (className.contains(".")) {
            className = className.substring(className.lastIndexOf(".") + 1);
        }
        
        //Rally -TA352735 : updated ID to id as per the new database model
        // Second arg actually returns T.class
        TypedQuery query = entityManager.createQuery("id from " + className + " where rule_name  = " + ruleName, classType);
        if ( query.getResultList() == null) throw new RiskEngineApplicationException( "No record found for ID " + ruleName  );
        Long toReturn = (Long) query.getResultList().get(0);
        return toReturn;
    }


}
