/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intersourcellc.riskengine.repositories;

import com.intersourcellc.exception.RiskEngineApplicationException;
import com.intersourcellc.riskengine.models.RuleVariable;
import java.util.List;
import javax.persistence.TypedQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author soumya
 */
@Repository
public class RuleVariableRepository extends BasicRepository<RuleVariable>{
    
     /**
     * The type of class upon which to operate
     */
    private Class<RuleVariable> classType;

    /**
     * The name of the entity class, representing a table, upon which to operate
     */
    private String className;
    
    public RuleVariableRepository() {
           super(RuleVariable.class);  
    }
    
     /**
     * Return the specified element
     */
    @Transactional
    public List<RuleVariable> findByRuleId(Long id) throws RiskEngineApplicationException {
        
        classType = RuleVariable.class;
        className = classType.getName();

        if (className.contains(".")) {
            className = className.substring(className.lastIndexOf(".") + 1);
        }
        TypedQuery query = entityManager.createQuery("from " + className + " where rule_id = " + id, classType);

        if ( query.getResultList() == null || query.getResultList().size() < 1) throw new RiskEngineApplicationException ( "No record found for ID " + id);
        List<RuleVariable> toReturn =  query.getResultList();
        return toReturn;
    }
    
}
