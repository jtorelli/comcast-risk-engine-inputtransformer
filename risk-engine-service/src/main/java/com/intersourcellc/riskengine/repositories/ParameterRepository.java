/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intersourcellc.riskengine.repositories;

import com.intersourcellc.exception.RiskEngineApplicationException;
import com.intersourcellc.riskengine.models.Parameter;
import java.util.List;
import javax.persistence.TypedQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author soumya
 */
@Repository
public class ParameterRepository extends BasicRepository<Parameter>{
    /**
     * The type of class upon which to operate
     */
    private Class<Parameter> classType;

    /**
     * The name of the entity class, representing a table, upon which to operate
     */
    private String className;
    
    public ParameterRepository() {
        super ( Parameter.class);
    }
    
    @Transactional
    public List<Parameter> findByRequestId(Long id) throws RiskEngineApplicationException {
        
        classType = Parameter.class;
        className = classType.getName();

        if (className.contains(".")) {
            className = className.substring(className.lastIndexOf(".") + 1);
        }
        TypedQuery query = entityManager.createQuery("from " + className + " where request_id = " + id, classType);
        if ( query.getResultList() == null ) throw new RiskEngineApplicationException( "No record found for ID " + id);
        List<Parameter> toReturn = query.getResultList();
        return toReturn;
    }
}
