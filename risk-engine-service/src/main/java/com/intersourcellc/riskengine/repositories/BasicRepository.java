/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intersourcellc.riskengine.repositories;

import com.intersourcellc.exception.RiskEngineApplicationException;
import com.intersourcellc.riskengine.models.EntityWithIdentity;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * Define a basic repository class
 *
 * @author jimt
 */
@Repository
public abstract class BasicRepository<T> {
     final static Logger log = LoggerFactory.getLogger(BasicRepository.class);

    /**
     * The entity manager injected by spring
     */
    protected EntityManager entityManager;

    /**
     * The type of class upon which to operate
     */
    private Class<T> classType;

    /**
     * The name of the entity class, representing a table, upon which to operate
     */
    private String className;

    /**
     * @return the entityManager
     */
    public EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * Called by the framework to set the entity manager
     *
     * @param entityManager the entityManager to set
     */
    @PersistenceContext // This will be injected by Spring
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * Create or update, based on id
     */
    public T createOrUpdate(T item) {

        if (((EntityWithIdentity) item).getId() == null) {
            return create(item);
        } else {
            return (update(item));
        }
    }

    /**
     * Create the provided element
     */
    @Transactional
    public T create(T created) {
        entityManager.persist(created);
        return created;
    }

    /**
     * Delete the specified element
     */
    @Transactional
    public void delete(Object id) {
        entityManager.remove(id);
    }

    public BasicRepository(Class<T> classType) {

        try { //Class.getName() throws NullPointerException, String.substring() throws IndexOutOfBoundsException
            this.classType = classType;
            className = classType.getName();

            if (className.contains(".")) {
                className = className.substring(className.lastIndexOf(".") + 1);
            }
        } catch (NullPointerException|IndexOutOfBoundsException e) {
            throw new RiskEngineApplicationException("All @Entity classes need a @Table tag defining a table name value", e);
        }
    }

    /**
     * Return all elements
     */
    @Transactional
    public List<T> findAll() {

        // Second arg actually returns T.class
        TypedQuery query = entityManager.createQuery("from " + className, classType);
        return query.getResultList();
    }

    /**
     * Return the specified element
     */
    @Transactional
    public T findById(Long id) throws RiskEngineApplicationException {

        //Rally -TA352735 : updated ID to id as per the new database model
        // Second arg actually returns T.class
        log.debug("********FINDBYID**********",id);
        TypedQuery query = entityManager.createQuery("from " + className + " where id = " + id, classType);
        
        if ( query.getResultList() == null || query.getResultList().size() > 1) throw new RiskEngineApplicationException("No record found for ID " + id);
        T toReturn = (T) query.getResultList().get(0);
        return toReturn;
    }

    /**
     * Update the specified element
     */
    @Transactional
    public T update(T updated) {
        entityManager.merge(updated);
        return updated;
    }
}
