/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intersourcellc.riskengine.repositories;

import com.intersourcellc.riskengine.models.RequestTypes;
import org.springframework.stereotype.Repository;

/**
 *
 * @author soumya
 */
@Repository
public class RequestTypeRepository extends BasicRepository<RequestTypes> {
    public RequestTypeRepository() {
        super ( RequestTypes.class);
    }
}
