/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intersourcellc.riskengine.repositories;

import com.intersourcellc.riskengine.models.VariableTypes;
import org.springframework.stereotype.Repository;

/**
 *
 * @author soumya
 */
@Repository
public class VariableTypesRepository extends BasicRepository<VariableTypes>{
    public VariableTypesRepository() {
        super ( VariableTypes.class);
    }
    
}