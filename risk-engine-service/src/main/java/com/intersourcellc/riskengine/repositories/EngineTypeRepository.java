/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intersourcellc.riskengine.repositories;

import com.intersourcellc.riskengine.models.EngineTypes;
import org.springframework.stereotype.Repository;

/**
 *
 * @author soumya
 */
@Repository
public class EngineTypeRepository extends BasicRepository<EngineTypes> {
    public EngineTypeRepository() {
        super ( EngineTypes.class);
    }
    
}
