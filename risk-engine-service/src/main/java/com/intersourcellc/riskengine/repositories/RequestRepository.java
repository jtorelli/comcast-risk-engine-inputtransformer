/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intersourcellc.riskengine.repositories;

import com.intersourcellc.exception.RiskEngineApplicationException;
import com.intersourcellc.riskengine.models.Request;
import java.util.List;
import javax.persistence.TypedQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author soumya
 */
@Repository
public class RequestRepository extends BasicRepository<Request>{
    
     /**
     * The type of class upon which to operate
     */
    private Class<Request> classType;

    /**
     * The name of the entity class, representing a table, upon which to operate
     */
    private String className;
    
    
     public RequestRepository () {
        super ( Request.class);
     }
    
     
    /**
     * Return the specified element
     */
    @Transactional
    public List<Request> findByVariableId(Long id) throws RiskEngineApplicationException {
        
        classType = Request.class;
        className = classType.getName();

        if (className.contains(".")) {
            className = className.substring(className.lastIndexOf(".") + 1);
        }
        TypedQuery query = entityManager.createQuery("from " + className + " where variable_id = " + id, classType);
        if ( query.getResultList() == null) throw new RiskEngineApplicationException ( "No record found for ID " + id);
        List<Request> toReturn = query.getResultList();
        return toReturn;
    }
}
