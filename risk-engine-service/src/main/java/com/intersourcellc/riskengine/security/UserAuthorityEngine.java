package com.intersourcellc.riskengine.security;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

public class UserAuthorityEngine {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    public List<GrantedAuthority> getAuthoritiesForUsername(String username) {

        List<GrantedAuthority> mappedAuthorities = new ArrayList<GrantedAuthority>();

        mappedAuthorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        mappedAuthorities.add(new SimpleGrantedAuthority("ROLE_VIEW"));
        mappedAuthorities.add(new SimpleGrantedAuthority("ROLE_EDIT"));
        mappedAuthorities.add(new SimpleGrantedAuthority("ROLE_CREATE"));

        // TODO IMPLEMENT THIS FOR REAL!

        return mappedAuthorities;
    }
}
