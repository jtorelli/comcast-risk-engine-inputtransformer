package com.intersourcellc.riskengine.config;

import javax.persistence.EntityManager;
import javax.servlet.ServletContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.ServletContextAware;

public final class EnvironmentConfig implements ServletContextAware {

    // For unit test
    private static EntityManager entityManager = null;
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    public enum Environment {
        dev, test, demo, demo_comcast, staging, prod
    }

    // LDAP Authentication
    private String ldapAuthURL;
    private String ldapDomain;

    // Version number info
    private String buildVersion;
    private int releaseNumber;
    private int iterationNumber;
    private String riskEngineDbUrl;
    private String riskEngineDbUser;
    private String riskEngineDbPass;
    private Environment currentEnvironment;
    private ServletContext servletContext;

    public EnvironmentConfig(Environment env, String buildNumber, int releaseNumber, int iterationNumber, String dbUrl, String dbUser, String dbPass) {
        this.buildVersion = buildNumber;

        this.releaseNumber = releaseNumber;
        this.iterationNumber = iterationNumber;

        ldapDomain = "cable.comcast.com";
        ldapAuthURL = "ldap://adapps.cable.comcast.com:389";

        currentEnvironment = env;
        logger.info("Starting " + getProductNameLine());

        this.riskEngineDbUrl = dbUrl;
        this.riskEngineDbUser = dbUser;
        this.riskEngineDbPass = dbPass;

        // TODO Make sure entityManager is wired

    }

    /**
     * Return true if this is a Windows machine
     */
    public boolean isWindows() {
        return (System.getProperty("os.name").toLowerCase().contains("win"));

    }

    public String getLdapAuthURL() {
        return ldapAuthURL;
    }

    public String getLdapDomain() {
        return ldapDomain;
    }

    public String getBuildVersion() {
        return buildVersion;
    }

    public String getProductName() {
        return "Comcast RiskEngine v" + releaseNumber + "." + iterationNumber;
    }

    public String getProductNameLine() {
        return getProductName() + " " + getProductBuildInfo();
    }

    public String getProductBuildInfo() {
        StringBuilder toReturn = new StringBuilder("(build " + buildVersion);

        if (currentEnvironment != Environment.prod) {
            toReturn.append("-").append(currentEnvironment).append(")");
        } else {
            toReturn.append(")");
        }

        return toReturn.toString();
    }

    public int getReleaseNumber() {

        return releaseNumber;
    }

    public int getIterationNumber() {
        return iterationNumber;
    }

    public Environment getCurrentEnvironment() {
        return currentEnvironment;
    }

    @Override
    public void setServletContext(ServletContext sc) {
        logger.info("servlet context set");
        this.servletContext = sc;
    }

    public ServletContext getServletContext() {
        return this.servletContext;
    }

    public static EntityManager getEntityManager() {

        return entityManager;
    }

    public String getRiskEngineDbUrl() {
        return riskEngineDbUrl;
    }

    public void setRiskEngineDbUrl(String dbUrl) {
        this.riskEngineDbUrl = dbUrl;
    }

    public String getRiskEngineDbUser() {
        return riskEngineDbUser;
    }

    public void setRiskEngineDbUser(String dbUser) {
        this.riskEngineDbUser = dbUser;
    }

    /**
     * @return the riskEngineDbPass
     */
    public String getRiskEngineDbPass() {
        return riskEngineDbPass;
    }

    /**
     * @param dbPass the dbPass to set
     */
    public void setRiskEngineDbPass(String dbPass) {
        this.riskEngineDbPass = dbPass;
    }
}
