USE `risk_engine`;

CREATE TABLE IF NOT EXISTS `bi_cran_engineer` (
  `Id` smallint(6) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT NULL,
  `NTLogin` varchar(25) DEFAULT NULL,
  `Devision` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `bi_division` (
  `ENTRY_ID` varchar(15) NOT NULL DEFAULT '',
  `CREATE_DATE` decimal(38,0) DEFAULT NULL,
  `SUBMITTER` varchar(254) DEFAULT NULL,
  `LAST_MODIFIED_DATE` decimal(38,0) DEFAULT NULL,
  `MODIFIED_BY` varchar(254) DEFAULT NULL,
  `STATUS` decimal(38,0) DEFAULT NULL,
  `DIVISION_ID` varchar(5) DEFAULT NULL,
  `DIVISION_NAME` varchar(25) DEFAULT NULL,
  `ORG_COMAPNY` char(1) DEFAULT NULL,
  `ORG_COMPANY_DESCR` varchar(50) DEFAULT NULL,
  `REPLICATED_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`ENTRY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `bi_market` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `region_id` varchar(10) DEFAULT NULL,
  `market_alias` varchar(100) DEFAULT NULL,
  `market_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `bi_region` (
  `REGION_ID` varchar(6) NOT NULL DEFAULT '',
  `SUBMITTER` varchar(254) DEFAULT NULL,
  `CREATE_DATE` bigint(20) DEFAULT NULL,
  `LAST_MODIFIED_BY` varchar(254) DEFAULT NULL,
  `MODIFIED_DATE` bigint(20) DEFAULT NULL,
  `STATUS` int(11) DEFAULT NULL,
  `REGION_NAME` varchar(50) DEFAULT NULL,
  `TIMEZONE` varchar(15) DEFAULT NULL,
  `OUTAGE_SCROLLER` varchar(255) DEFAULT NULL,
  `CANCEL_TR` int(11) DEFAULT NULL,
  `ROLL_CSG` int(11) DEFAULT NULL,
  `DIVISION` varchar(25) DEFAULT NULL,
  `REQUIRE_NETWORK_DR` varchar(10) DEFAULT NULL,
  `AUTO_START` int(11) DEFAULT NULL,
  `CORRELATION_PREFERENCE` int(11) DEFAULT NULL,
  `CM_PREFERENCE` int(11) DEFAULT NULL,
  `REPLICATED_DATE` datetime DEFAULT NULL,
  `ALLOWED_USAGE` varchar(255) DEFAULT NULL,
  `DIVISION_ID` varchar(5) DEFAULT NULL,
  `WHEN_CANCEL_TR` int(11) DEFAULT NULL,
  `ALLOW_CR_REOPEN` int(11) DEFAULT NULL,
  `CHANNEL_LINEUP_SOURCE` varchar(25) DEFAULT NULL,
  `CONTROLLER_ID` decimal(38,0) DEFAULT NULL,
  `MAP_ID` decimal(38,0) DEFAULT NULL,
  PRIMARY KEY (`REGION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `bi_user_region_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `region_id` varchar(10) DEFAULT NULL,
  `user_id` bigint(10) DEFAULT NULL,
  `nt_login` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `device` (
  `DID` int(11) NOT NULL,
  `HOSTNAME` varchar(100) NOT NULL,
  `SITE` int(11) NOT NULL,
  `MODEL_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`DID`),
  UNIQUE KEY `DID` (`DID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `model` (
  `ID` int(11) NOT NULL,
  `MODEL` varchar(100) NOT NULL,
  `MAKE` varchar(50) NOT NULL,
  `SLOTS` int(11) NOT NULL,
  `SLOT_START` int(11) NOT NULL DEFAULT '0',
  `MAXIMUM_CHASSIS` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `organization` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `organization_user` (
  `user_id` bigint(10) NOT NULL,
  `org_id` bigint(10) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`,`org_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `role` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `role` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `site` (
  `SID` int(11) NOT NULL,
  `SITE` varchar(120) NOT NULL,
  `MARKET` int(11) NOT NULL,
  PRIMARY KEY (`SID`),
  UNIQUE KEY `SID` (`SID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `slot` (
  `SID` int(11) NOT NULL,
  `DEVICE` int(11) NOT NULL,
  `SLOT` varchar(50) NOT NULL,
  `MODULE` int(11) NOT NULL,
  PRIMARY KEY (`SID`),
  UNIQUE KEY `SID` (`SID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `user` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `nt_login` varchar(30) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `remedy_login` varchar(30) NOT NULL,
  `data` mediumtext,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `userrole` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `user_org_view` (
	`user_id` BIGINT(10) NOT NULL,
	`org_id` BIGINT(10) NOT NULL,
	`nt_login` VARCHAR(30) NOT NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `user_org_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`riskengine`@`localhost` VIEW `user_org_view` AS select `u`.`id` AS `user_id`,`o`.`org_id` AS `org_id`,`u`.`nt_login` AS `nt_login` from (`user` `u` join `organization_user` `o` on((`u`.`id` = `o`.`user_id`)));