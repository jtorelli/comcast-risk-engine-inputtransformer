-- MySQL dump 10.13  Distrib 5.5.31, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: risk_engine
-- ------------------------------------------------------
-- Server version	5.5.31-0ubuntu0.12.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `RULE_FETCHABLES`
--

DROP TABLE IF EXISTS `RULE_FETCHABLES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RULE_FETCHABLES` (
  `RULE_ID` bigint(20) NOT NULL,
  `FETCHABLE_ID` bigint(20) NOT NULL,
  KEY `FK18CCF1029A38583E` (`FETCHABLE_ID`),
  KEY `FK18CCF10223FBBEB6` (`RULE_ID`),
  CONSTRAINT `FK18CCF10223FBBEB6` FOREIGN KEY (`RULE_ID`) REFERENCES `RULE` (`ID`),
  CONSTRAINT `FK18CCF1029A38583E` FOREIGN KEY (`FETCHABLE_ID`) REFERENCES `FETCHABLE` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RULE_FETCHABLES`
--

LOCK TABLES `RULE_FETCHABLES` WRITE;
/*!40000 ALTER TABLE `RULE_FETCHABLES` DISABLE KEYS */;
INSERT INTO `RULE_FETCHABLES` VALUES (1006,1001);
/*!40000 ALTER TABLE `RULE_FETCHABLES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FETCHABLE`
--

DROP TABLE IF EXISTS `FETCHABLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FETCHABLE` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `FETCHABLE_DESCRIPTION` varchar(250) DEFAULT NULL,
  `FETCHABLE_NAME` varchar(50) NOT NULL,
  `FETCHABLE_URL` varchar(500) NOT NULL,
  `FETCHABLE_INPUT_DATA` longtext NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1002 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FETCHABLE`
--

LOCK TABLES `FETCHABLE` WRITE;
/*!40000 ALTER TABLE `FETCHABLE` DISABLE KEYS */;
INSERT INTO `FETCHABLE` VALUES (1000,'Demo Weather','GetWeatherDemo','http://api.openweathermap.org/data/2.5/weather?id=2172797','203'),(1001,'Seattle Weather Demo','seattleWeatherJson','http://api.openweathermap.org/data/2.5/weather?q=Seattle,us','204');
/*!40000 ALTER TABLE `FETCHABLE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RULE`
--

DROP TABLE IF EXISTS `RULE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RULE` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `RULE_DESCRIPTION` varchar(250) DEFAULT NULL,
  `RULE_NAME` varchar(50) NOT NULL,
  `RULE_TEXT` longtext,
  `RULE_INPUT_PARAMETERS` longtext NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1009 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RULE`
--

LOCK TABLES `RULE` WRITE;
/*!40000 ALTER TABLE `RULE` DISABLE KEYS */;
INSERT INTO `RULE` VALUES (1006,'RuleDescriptionTest','RuleNameTetst','  var log = JsLogger.getLogger(ruleName);\n\nvar seattleWeather = JSON.parse(seattleWeatherJson);\n\nDebug.printContext(this);\n\nlog.info(\'City: \' + seattleWeather.name);\nlog.info(\'Temp: \' + kToC(seattleWeather.main.temp) + \' Deg C\');\nlog.info(\'Temp: \' + cToF(kToC(seattleWeather.main.temp)) + \' Deg F\');\n\n// Convert the temperature, but also throw the printStackTrace()\nvar currentTemp = kToF(seattleWeather.main.temp);\n\nif (currentTemp < 62) {\n   factoid.addFact(\'temperature\', \'Cold\', DataTypeSTRING);\n} else if (currentTemp >= 62 || currentTemp < 75) {\n   factoid.addFact(\'temperature\', \'Cool\', DataTypeSTRING);\n} else if (currentTemp >= 75 || currentTemp < 90) {\n   factoid.addFact(\'temperature\', \'Warm\', DataTypeSTRING);\n} else if (currentTemp >= 90) {\n   factoid.addFact(\'temperature\', \'Hot\', DataTypeSTRING);\n};\n\nfunction kToC(k, opt) {\n   opt = opt || {};\n   opt.params = opt.params || {};\n   opt.debug = opt.debug || false;\n\n   if(opt.debug) Debug.printStackTrace();\n   return k - 273.15;\n};\n\nfunction cToF(c) {\n   return 32 + (9.0/5.0) * c;\n};\n\nfunction kToF(k) {\n   return 32 + (9.0/5.0) * kToC(k, {debug:true});\n};','String liveEndpoint="http://api.openweathermap.org/data/2.5/weather?q=Seattle,us"');
/*!40000 ALTER TABLE `RULE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `META_DATA`
--

DROP TABLE IF EXISTS `META_DATA`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `META_DATA` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `METADATA_KEY` varchar(50) NOT NULL,
  `METADATA_VALUE` varchar(250) NOT NULL,
  `RULE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `RULE_ID` (`RULE_ID`),
  CONSTRAINT `META_DATA_RULE_ID_FK` FOREIGN KEY (`RULE_ID`) REFERENCES `RULE` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `META_DATA`
--

LOCK TABLES `META_DATA` WRITE;
/*!40000 ALTER TABLE `META_DATA` DISABLE KEYS */;
/*!40000 ALTER TABLE `META_DATA` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-06-25 11:06:50
