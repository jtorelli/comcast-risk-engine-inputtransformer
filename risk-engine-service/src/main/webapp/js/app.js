define (['jquery', 'underscore', 'backbone', 'app/routes', 'app/views/chromeview', 
                'jquery.qtip', 'jquery.cookie',
		'util', 'plugins','localstorage','app/rules',
		'jqplot',		
		'jqplot.linearTickGenerator',
		'jqplot.linearAxisRenderer',
		'jqplot.axisTickRenderer',
		'jqplot.axisLabelRenderer',
		'jqplot.tableLegendRenderer',
		'jqplot.lineRenderer',
		'jqplot.markerRenderer',
		'jqplot.divTitleRenderer',
		'jqplot.canvasGridRenderer',
		'jqplot.linePattern',
		'jqplot.shadowRenderer',
		'jqplot.shapeRenderer',
		'jqplot.sprintf',
		'jqplot.themeEngine',
		'jqplot.toImage',
		'jqplot.effects.core',
		'jqplot.effects.blind',
		'jqplot.pointLabels',
		'jqplot.highlighter',
		'jqplot.barRenderer',
		'jqplot.canvasTextRenderer',
		'jqplot.canvasAxisLabelRenderer',
		'jqplot.canvasAxisTickRenderer',
		'jqplot.categoryAxisRenderer',
		'jqplot.pieRenderer',
		'jqplot.donutRenderer'

		
],
function (Backbone, $, AppRouter, ChromeView) {
	$(function(){

		$.get('templates/rules.html', function (data) {
			$('body').append(data);
			$.get('templates/ticket.html', function (data) {
				$('body').append(data);
			});
		});
	});
});
