require.config({
	shim: {
		'underscore': {
			exports: '_'
		},
		'handlebars': {
			exports: 'Handlebars'
		},
		'backbone': {
			deps: [
				'underscore',
				'jquery'
			],
			exports: 'Backbone'
		},
		'jquery': {
			exports: 'jQuery'
		},
		'jquery.qtip': {
			deps: [
				'jquery'
			],
			exports: 'jQuery.fn.qtip'
		},
		'jquery.cookie': {
			deps: [
				'jquery'
			],
			exports: 'jQuery.fn.cookie'
		},
		'jqplot': {
			deps: [
				'jquery',
				'jsdate'
			],
			exports: 'jQuery.fn.jqplot'
		},
		'moment': {
			exports: 'moment'
		}

	},

	// Libraries
	paths: {
		// qtip is incompatible with 1.9.1
		'jquery': 'libs/jquery-1.8.0',
		'jquery.qtip': 'libs/jquery.qtip.1',
		'jquery.cookie': 'libs/jquery.cookie',
		'jsdate': 'libs/jqplot/src/jsdate',
		'jqplot': 'libs/jqplot/src/jqplot.core',
		'jqplot.linearTickGenerator': 'libs/jqplot/src/jqplot.linearTickGenerator',
		'jqplot.linearAxisRenderer': 'libs/jqplot/src/jqplot.linearAxisRenderer',
		'jqplot.axisTickRenderer': 'libs/jqplot/src/jqplot.axisTickRenderer',
		'jqplot.axisLabelRenderer': 'libs/jqplot/src/jqplot.axisLabelRenderer',
		'jqplot.tableLegendRenderer': 'libs/jqplot/src/jqplot.tableLegendRenderer',
		'jqplot.lineRenderer': 'libs/jqplot/src/jqplot.lineRenderer',
		'jqplot.markerRenderer': 'libs/jqplot/src/jqplot.markerRenderer',
		'jqplot.divTitleRenderer': 'libs/jqplot/src/jqplot.divTitleRenderer',
		'jqplot.canvasGridRenderer': 'libs/jqplot/src/jqplot.canvasGridRenderer',
		'jqplot.linePattern': 'libs/jqplot/src/jqplot.linePattern',
		'jqplot.shadowRenderer': 'libs/jqplot/src/jqplot.shadowRenderer',
		'jqplot.shapeRenderer': 'libs/jqplot/src/jqplot.shapeRenderer',
		'jqplot.sprintf': 'libs/jqplot/src/jqplot.sprintf',
		'jqplot.themeEngine': 'libs/jqplot/src/jqplot.themeEngine',
		'jqplot.toImage': 'libs/jqplot/src/jqplot.toImage',
		'jqplot.effects.core': 'libs/jqplot/src/jqplot.effects.core',
		'jqplot.effects.blind': 'libs/jqplot/src/jqplot.effects.blind',
		'jqplot.pointLabels': 'libs/jqplot/src/plugins/jqplot.pointLabels',
		'jqplot.highlighter': 'libs/jqplot/src/plugins/jqplot.highlighter',
		'jqplot.barRenderer': 'libs/jqplot/src/plugins/jqplot.barRenderer',
		'jqplot.canvasTextRenderer': 'libs/jqplot/src/plugins/jqplot.canvasTextRenderer',
		'jqplot.canvasAxisLabelRenderer': 'libs/jqplot/src/plugins/jqplot.canvasAxisLabelRenderer',
		'jqplot.canvasAxisTickRenderer': 'libs/jqplot/src/plugins/jqplot.canvasAxisTickRenderer',
		'jqplot.categoryAxisRenderer': 'libs/jqplot/src/plugins/jqplot.categoryAxisRenderer',
		'jqplot.pieRenderer': 'libs/jqplot/src/plugins/jqplot.pieRenderer',
		'jqplot.donutRenderer': 'libs/jqplot/src/plugins/jqplot.donutRenderer',
		'underscore': 'lib/underscore',
		'backbone': 'lib/backbone',
		'handlebars': 'libs/handlebars',
		'moment': 'libs/moment',
		'template': '../../templates',
                'localstorage': 'lib/backbone.localstorage'
                
	}
});


require(['app']);
