define([ 'underscore'],
function (_) {
	
	// TODO: split into separate files and access like validator = new require('vali../' + type)

	var validators = {};
	validators.Validator = function (){
		// put common stuff here
	};
		// TODO: validators
		//			- extract to FormView
		//			- add in tri-state: valid, invalid, unvalidated
		//			- add valid state hooks (show/hide buttons, etc.

	// TODO: get Backbone like inheritance running

	validators.LengthValidator = function () {};

	_.extend(validators.LengthValidator.prototype,{
		type: 'LengthValidator',
		validate: function(value, errors) {
			// make the value an empty string if null
			var valid = true;
			if (this.max > 0 && value.length > this.max) {
				valid = false;
				errors.push(this.message + '<br>');
			}
			if (value.length < this.min) {
				valid = false;
				errors.push(this.message + '<br>');
			}
			return valid;
		}
	});

	validators.RequiredValidator = function () {};
	_.extend(validators.RequiredValidator.prototype,{
		type: 'RequiredValidator',
		validate: function(value, errors, field) {
			var valid = true;
			if (
				// Check if value is the same as the select message value for drop downs
				(field && field.properties.showSelect && field.properties.selectValue === value)
				// or if it's empty
				|| value.length === 0
			) {
				valid = false;
				errors.push(this.message + '<br>');
			}
			return valid;
		}
	});

	validators.DateValidator = function () {};
	_.extend(validators.DateValidator.prototype,{
		type: 'DateValidator',
		validate: function(value, errors) {
			var valid = true;
			if (/Invalid|NaN/.test(value)) {
				valid = false;
				errors.push(this.message + '<br>');
			}
			return valid;
		}
	});

	validators.RegexValidator = function () {};
	_.extend(validators.RegexValidator,{
		type: 'RegexValidator',
		validate: function(value, errors) {
			var valid = true;
			// TODO: implement
			valid = false;
			errors.push('Unimplemented<br>');

			return valid;
		}
	});

	validators.NumericValidator = function () {};
	_.extend(validators.NumericValidator,{
		type: 'NumericValidator',
		validate: function(value, errors) {
			var valid = true;
			// TODO: implement
			valid = false;
			errors.push('Unimplemented<br>');

			return valid;
		}
	});
	
	return validators;

});

