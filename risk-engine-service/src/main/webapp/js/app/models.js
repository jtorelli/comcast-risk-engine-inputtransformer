define([ 'backbone', 'moment'],
function (Backbone, moment) {
	
	var models = {};
	
	// models
	models.CompletedDashboardData = Backbone.Model.extend({
		urlRoot: 'api/dashboard/completed'
	});

	models.InProgressDashboardData = Backbone.Model.extend({
		urlRoot: 'api/dashboard/inprogress'
	});

	models.EngineeringDashboardData = Backbone.Model.extend({
		urlRoot: 'api/dashboard/eng'
	});

	models.UserMarketData = Backbone.Model.extend({
		urlRoot: 'api/user/allmarkets'
	});

	models.Workflow = Backbone.Model.extend({
		urlRoot: 'api/workflow'
		// TODO: return groups
		// TODO: return field models
	});
	
	models.WorkflowTemplate = Backbone.Model.extend({
	});
	
	models.Ticket = Backbone.Model.extend({
		urlRoot: 'api/ticket',
		idAttribute: 'ticketId',
		daysSinceStepStart: function(offset) {
			var startDate = moment(this.get("stepStart"));

			if (startDate === null) return 0;

			startDate.seconds(0);
			startDate.minutes(0);
			startDate.hours(0);

			var now = moment();
			now.seconds(0);
			now.minutes(0);
			now.hours(0);

			return now.diff(startDate, 'days') + offset;
		},
		daysTicketOverdue: function() {
			var endDate = moment(this.get("plannedEnd"));
			if (endDate === null) return 0;

			endDate.seconds(0);
			endDate.minutes(0);
			endDate.hours(0);

			var now = moment();
			now.seconds(0);
			now.minutes(0);
			now.hours(0);

			return now.diff(endDate, 'days');
		},
		daysSinceSubmission: function() {
			var modifiedDate = moment(this.get("modified"));
			return new moment().diff(modifiedDate, 'days');
		}
	});

	models.Field = Backbone.Model.extend({
	});

	models.Chart = Backbone.Model.extend({
	});
	
	return models;
});
