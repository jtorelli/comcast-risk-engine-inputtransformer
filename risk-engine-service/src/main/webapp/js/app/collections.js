define([ 'backbone', 'underscore', 'app/models'],
function (Backbone, _, Models) {
	
	var collections = {};
	// collections
	collections.UserMarketDataSet = Backbone.Collection.extend({
		model: Models.UserMarketData,
		url: 'api/user/allmarkets'
	});
	collections.CompletedDashboardDataSet = Backbone.Collection.extend({
		model: Models.CompletedDashboardData,
		url: 'api/dashboard/completed'
	});

	collections.InProgressDashboardDataSet = Backbone.Collection.extend({
		model: Models.InProgressDashboardData,
		url: 'api/dashboard/inprogress'
	});

	collections.TicketSet = Backbone.Collection.extend({
		model: Models.Ticket,
		url: 'api/ticket',
		search : function(statusArray){
			if(statusArray === null) return this;

			return _(this.filter(function(data) {
				for(var i=0; i<statusArray.length; i++) {
					if (statusArray[i] === data.get('status')) return true;
				}
			}));
		},
		searchWithMethod: function(f) {
			return _(this.filter(function(data) {
				return f(data);
			}));
		}
	});

	collections.TicketSet.Aggregate = {};

	collections.TicketSet.Aggregate.ageOfOldest = function(collection) {
		var oldestAgeDays = 0;

		collection.each(function(ticket) {
			var createdDate = moment(ticket.get("createDate"));
			var ageDays = moment().diff(createdDate, 'days');

			if (ageDays > oldestAgeDays) {
				oldestAgeDays = ageDays;
			}
		});

		return oldestAgeDays;
	};

	collections.TicketSet.Aggregate.averageAge = function(collection) {
		if ((collection.size && collection.size() === 0) ||
			(collection.length && collection.length === 0)) return 0;

		var countAgeDays = 0;
		var collectionSize = collection.size?collection.size():collection.length;

		collection.each(function(ticket) {
			var createdDate = moment(ticket.get("createDate"));
			var ageDays = moment().diff(createdDate, 'days');

			countAgeDays += ageDays;
		});

		return Math.round(countAgeDays / collectionSize);
	};

	collections.TicketSet.Aggregate.countIncomplete = function(collection) {
		var countIncomplete = 0;

		collection.each(function(ticket) {
			if (
				!ticket.get('title') ||
				!ticket.get('fieldData').plannedStart ||
				!ticket.get('fieldData').remoteDevice ||
				!ticket.get('fieldData').remotePortAssignment ||
				!ticket.get('fieldData').routerPortAssignment ||
				!ticket.get('fieldData').ipAddress ||
				!ticket.get('fieldData').routerConfigs
				){
					countIncomplete++;
				}
		});

		return countIncomplete;
	};

	collections.TicketSet.Aggregate.moreThanAWeekOld = function(collection) {
		var countOverAWeekOld = 0;

		collection.each(function(ticket) {
			var createdDate = new moment(ticket.get("created"));
			var diffDays = new moment().diff(createdDate, 'days');

			if (diffDays >= 7) {
				countOverAWeekOld++;
			}
		});

		return countOverAWeekOld;
	};

	collections.TicketSet.Aggregate.createdInDateRange = function(daysStart, daysEnd) {
		return function(collection) {
			var countUnderNDaysOld = 0;

			collection.each(function(ticket) {
				var createdDate = new moment(ticket.get("created"));
				var diffDays = new moment().diff(createdDate, 'days');

				if (diffDays >= daysStart && (daysEnd === -1 || diffDays <= daysEnd)) {
					countUnderNDaysOld++;
				}
			});

			return countUnderNDaysOld;
		};
	};

	collections.TicketSet.Aggregate.getOldestAge = function(collection, period) {
		var oldestAge = 1;

		collection.each(function(ticket) {
			var createdDate = new moment(ticket.get("created"));
			var diff = new moment().diff(createdDate, period);

			if (diff > oldestAge) {
				oldestAge = diff;
			}
		});

		return oldestAge;
	};

	collections.FieldSet = Backbone.Collection.extend({
		model: Models.Field
	});
	collections.ChartSet = Backbone.Collection.extend({
		model: Models.Chart,
		url: 'api/chart'
	});

	collections.SearchResultSet = Backbone.Collection.extend({
		model: Models.Ticket,
		url: 'api/search',

		search : function(f){
			return _(this.filter(function(data) {
				return f(data);
			}));
		}
	});
	
	return collections;
});
