define (['backbone', 'underscore', 'app/views/homeview', 'app/views/fields/ticketview', 'app/models', 'app/views/autosmopview', 'app/views/overviewview'], 
function(Backbone, _, HomeView, TicketView, Models, AutoSMOPView, OverviewView) {
// routes
	var appRouter = Backbone.Router.extend({
		routes: {
			'': 'defaultRoute',
	//		'search/:q': 'defaultRoute',
			'autosmop/ticket': 'newTicket',
			'autosmop/ticket/:id': 'viewTicket',
			'autosmop/ticket/new/:cid': 'viewNewTicket',
			'autosmop': 'autosmop',
			'overview': 'overview'
		},
		defaultRoute: function (actions){
			// TODO: find a better way to distiguish if this is the dashboard or not
			if (this.views.chrome) {
				this.views.chrome.showContent(this.views.homeView || (this.views.homeView = new HomeView().render()));
			}
		},
		views: {},
		newTicketCount: 0,
		newTicket: function () {
			this.ensureAutoSMOP();
			var view = new TicketView({
				model: new Models.Ticket(),
				mode: 'Edit',
				newid: ++this.newTicketCount
			});
			this.views.autoSMOPView.ticketViews['new' + this.newTicketCount] = view;
			this.navigate('autosmop/ticket/new/' + this.newTicketCount, true);
		},
		viewTicket: function (id) {
			this.ensureAutoSMOP();
			var view = _.find(
							_.values(this.views.autoSMOPView.ticketViews), function(view){
								return view.model.get("id") == id; // === doesn't work since 10002 is not the same as "10002"
						});
			if (view) {
				this.views.autoSMOPView.showContent(view);
				return;
			}
			this.showTicketView(id);
		},
		viewNewTicket: function (newid) {
			this.ensureAutoSMOP();
			var view = this.views.autoSMOPView.ticketViews['new' + newid];
			if (view) {
				this.views.autoSMOPView.showContent(view);
				return;
			} else {
				// Unknown new ticket. Just create a new one.
				this.newTicket();
			}
		},
		showTicketView: function (id) {
			this.ensureAutoSMOP();
			var ticket = new Models.Ticket();

			var self = this;
			ticket.set({ticketId: id});
			ticket.fetch({
				success: function (model) {
					var view = new TicketView({
									model: ticket
								});
					self.views.autoSMOPView.ticketViews[view.cid] = view;
					self.views.autoSMOPView.showContent(view);
				},
				error: function (xhr) {

					createGrowlFromJSON(JSON.parse ( xhr.responseText));
				}
			});
		},
		autosmop: function() {
			this.ensureAutoSMOP();

			if (!this.views.autoSMOPView.content) {
				this.views.autoSMOPView.showContent(this.views.overviewView || (this.views.overviewView = new OverviewView()));
			}
			else {
				this.navigate(this.views.autoSMOPView.content.getUri(), true);
			}
		},
		overview: function() {
			this.ensureAutoSMOP();
			this.views.autoSMOPView.showContent(this.views.overviewView || (this.views.overviewView = new OverviewView()));
		},
		ensureAutoSMOP: function() {
			this.views.chrome.showContent(this.views.autoSMOPView || (this.views.autoSMOPView = new AutoSMOPView().render()));
		}
	});

	return appRouter;
});
