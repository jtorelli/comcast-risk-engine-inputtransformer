

$(document).ready(function () {
$(function(){
  
  var Todo = Backbone.Model.extend({
    defaults: function() {
      return {
     
        ruleName: "empty todo...",
        ruleDescription: "empty description...",   
        ruleText: "empty text...",
        ruleInputParameters: "empty inputs...",
        order: Todos.nextOrder(),
        done: false
      };
    },
    toggle: function() {
      this.save({done: !this.get("done")});
    },
     url:'/api/rules/',


  });
  var TodoList = Backbone.Collection.extend({  
    model: Todo,
    //localStorage: new Backbone.LocalStorage("todos-backbone"),
    //urlRoot
    url:'/api/rules/',
    done: function() {
      return this.where({done: true});
    },
    remaining: function() {
      return this.without.apply(this, this.done());
    },
    nextOrder: function() {
      if (!this.length) return 1;
      return this.last().get('order') + 1;
    },
    comparator: 'order'

  });
  var Todos = new TodoList;
  var TodoView = Backbone.View.extend({
tagName:  "li",
template: _.template($('#item-template').html()),
    events: {
      "click .toggle"   : "toggleDone",
      "dblclick .viewRuleName"  : "editRuleName",
      "dblclick .viewRuleText"  : "editRuleText",
      "dblclick .viewRuleDescription"  : "editRuleDescription",
      "dblclick .viewruleInputParameters"  : "editruleInputParameters",
      "click a.destroy" : "clear",
      "keypress .editRuleName"  : "updateOnEnter",
      "blur .editRuleName"      : "closeRuleName",
      "keypress .editRuleText"  : "updateOnEnter",
      "blur .editRuleText"      : "closeRuleText",
      "keypress .editRuleDescription"  : "updateOnEnter",
      "blur .editRuleDescription"      : "closeRuleDescription",
      "keypress .editruleInputParameters"  : "updateOnEnter",
      "blur .editruleInputParameters"      : "closeruleInputParameters",
      "click .execute":"execute"
    },
    initialize: function() {
      this.listenTo(this.model, 'change', this.render);
      this.listenTo(this.model, 'destroy', this.remove);
    },
    render: function() {
      this.$el.html(this.template(this.model.toJSON()));
      this.$el.toggleClass('done', this.model.get('done'));
      this.inputRuleName = this.$('.editRuleName');
      this.inputRuleText = this.$('.editRuleText');
      this.inputRuleDescription = this.$('.editRuleDescription');
      this.inputruleInputParameters = this.$('.editruleInputParameters');
      return this;
    },
    toggleDone: function() {
      this.model.toggle();
    },
    editRuleText: function() {
        this.$el.addClass("editingRuleText");
        this.inputRuleText.focus();          
    },
    editRuleName: function() {
      this.$el.addClass("editingRuleName");
      this.inputRuleName.focus();
    },
    editRuleDescription: function() {
      this.$el.addClass("editingRuleDescription");
      
      this.inputRuleDescription.focus();

    },
    editruleInputParameters: function() {
      this.$el.addClass("editingruleInputParameters");
      this.inputruleInputParameters.focus();
    },
    closeRuleName: function() {

      var valueRuleName = this.inputRuleName.val();
      if (!valueRuleName) {
        this.clear();
      } else {         
        this.model.save({id:this.model.attributes.id, ruleName: valueRuleName});//, ruleDescription:this.inputDescription.val(),ruleText:this.inputRuleText.val()});
        //this.model.save({ruleName: valueRuleName});
            this.$el.removeClass("editingRuleName");
      }
    },
            
      closeRuleDescription: function() {
      var valueRuleDesription = this.inputRuleDescription.val();
      if (!valueRuleDesription) {
//         this.clear();
      } else {
           this.model.save({id:this.model.attributes.id, ruleDescription:valueRuleDesription});//,ruleText:this.inputRuleText.val()});
       // this.model.save({ruleDescription:valueRuleDesription});
              this.$el.removeClass("editingRuleDescription");
      }
          },
          
      closeRuleText: function() {  
      var valueRuleText = this.inputRuleText.val();
      if (!valueRuleText) {
    //    this.clear();
      } else {
       this.model.save({id:this.model.attributes.id, ruleText:valueRuleText});
 //      this.model.save({ruleText:valueRuleText});
        this.$el.removeClass("editingRuleText");
      }},    

      closeruleInputParameters:function(){
      var valueruleInputParameters = this.inputruleInputParameters.val();
      if (!valueruleInputParameters) {
 //       this.clear();
      } else {
        this.model.save({id:this.model.attributes.id, ruleInputParameters: valueruleInputParameters});//, ruleDescription:this.inputDescription.val(),ruleText:this.inputRuleText.val()});
    //    this.model.save({ruleInputParameters: valueruleInputParameters});
              this.$el.removeClass("editingruleInputParameters");
      }
    }, 
//    updateOnEnter: function(e) {
//      if (e.keyCode === 13) this.close();
//    },
    clear: function() {
      this.model.destroy();
    },
    execute: function(){
    var el = this.$('.result');
        $.support.cors = true;

        $.getJSON('/api/rules/executes/' + this.model.attributes.id, {
        }, function(data){
                var html = JSON.stringify(data);       
                el.html(html);  
         }).error(function(data){
             var dat2 = data.responseText;
             var html = JSON.stringify(dat2);        
                el.html(html);});	
    }        

  });
  var AppView = Backbone.View.extend({
    el: $("#todoapp"),
    statsTemplate: _.template($('#stats-template').html()),
    events: {
      "keypress #new-todo":  "createOnEnter",
      "click #clear-completed": "clearCompleted",
      "click #toggle-all": "toggleAllComplete"
    },
    initialize: function() {

      this.input = this.$("#new-todo");
      this.allCheckbox = this.$("#toggle-all")[0];

      this.listenTo(Todos, 'add', this.addOne);
      this.listenTo(Todos, 'reset', this.addAll);
      this.listenTo(Todos, 'all', this.render);

      this.footer = this.$('footer');
      this.main = $('#main');

      Todos.fetch();
    },
    render: function() {
      var done = Todos.done().length;
      var remaining = Todos.remaining().length;

      if (Todos.length) {
        this.main.show();
        this.footer.show();
        this.footer.html(this.statsTemplate({done: done, remaining: remaining}));
      } else {
        this.main.hide();
        this.footer.hide();
      }

      this.allCheckbox.checked = !remaining;
    },

   addOne: function(todo) {
      var view = new TodoView({model: todo});
      this.$("#todo-list").append(view.render().el);
    },
    addAll: function() {
      Todos.each(this.addOne, this);
    },
    createOnEnter: function(e) {
      if (e.keyCode !== 13) return;
      if (!this.input.val()) return;

      Todos.create({ruleName: this.input.val()});
      this.input.val('');
    },
    clearCompleted: function() {
      _.invoke(Todos.done(), 'destroy');
      return false;
    },

    toggleAllComplete: function () {
      var done = this.allCheckbox.checked;
      Todos.each(function (todo) { todo.save({'done': done}); });
    }

  });

 var App = new AppView();
 
 
 
 
//     $.forum.Router = Backbone.Router.extend({
//        routes: {
//            "": "show_thread_list",
//            "thread/:_id/": "show_thread",
//        },
    
//        show_thread_list: function() {
//            var App = new AppView();
//            var thread_list_view = new $.forum.ThreadListView({el: $('#content'), 
//                                                                model: thread_collection });
//            thread_collection.fetch();
//        },
//        
//        show_thread: function(_id) {
//            var todo = new Todo({_id: _id});
//            var todoView = new TodoView(el:$('.execute'), );
//            var thread_view = new $.forum.ThreadView({el: $('#content'), model: thread});
//            thread.fetch();
//        },
//        
//    });
    
    
    // App /////////////////////////////////////////////////////////////////
    
//    $.forum.app = null;
//    
//    $.forum.bootstrap = function() {
//        $.forum.app = new $.forum.Router(); 
//        Backbone.history.start({pushState: true});
//    };
 
 

});
});