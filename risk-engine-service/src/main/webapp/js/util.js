define (['jquery', 'jquery.qtip'], 
function($) {

	window.onerror = function(message, url, lineNumber) {
		// hide loaders
		$('.loader').addClass('hidden');

		createGrowl('Error<br>message: ' + message + '<br>url: ' + url + '<br>lineNumber: ' + lineNumber);
		// TODO: send to /api/logging

	};

	window.trim = function trim(str) {
		if (!str) return '';
		str = str.replace(/^\s\s*/, '');
		var	ws = /\s/,
			i = str.length;
		while (ws.test(str.charAt(i))) {
			i--;
		}
		return str.slice(0, i + 1);
	};

	window.capitalise = function capitalise (string)
	{
		return string.charAt(0).toUpperCase() + string.slice(1);
	};

	window.lowercase = function lowercase (string)
	{
		return string.charAt(0).toLowerCase() + string.slice(1);
	};

	window.createGrowl = function createGrowl (message, title, persistent) {
		title = title || 'Attention!';
		persistent = persistent || true;
		// Use the last visible jGrowl qtip as our positioning target
		var target = $('.qtip.jgrowl:visible:last');

		// Create your jGrowl qTip...
		$(document.body).qtip({
			// Any content config you want here really.... go wild!
			content: {
				text: message,
				title: {
					text: title,
					button: true
				}
			},
			position: {
				my: 'top right',
				// Not really important...
				at: (target.length ? 'bottom' : 'top') + ' right',
				// If target is window use 'top right' instead of 'bottom right'
				target: target.length ? target : $(window),
				// Use our target declared above
				adjust: { y: 5 },
				effect: function(api, newPos) {
					// Animate as usual if the window element is the target
					$(this).animate(newPos, {
						duration: 200,
						queue: false
					});

					// Store the final animate position
					api.cache.finalPos = newPos;
				}
			},
			show: {
				event: false,
				// Don't show it on a regular event
				ready: true,
				// Show it when ready (rendered)
				effect: function() {
					$(this).stop(0, 1).fadeIn(400);
				},
				// Matches the hide effect
				delay: 0,
				// Needed to prevent positioning issues
				// Custom option for use with the .get()/.set() API, awesome!
				persistent: persistent
			},
			hide: {
				event: false,
				// Don't hide it on a regular event
				effect: function(api) {
					// Do a regular fadeOut, but add some spice!
					$(this).stop(0, 1).fadeOut(400).queue(function() {
						// Destroy this tooltip after fading out
						api.destroy();

						// Update positions
						updateGrowls();
					});
				}
			},
			style: {
				classes: 'jgrowl ui-tooltip-dark ui-tooltip-rounded',
				// Some nice visual classes
				tip: false // No tips for this one (optional ofcourse)
			},
			events: {
				render: function(event, api) {
					// Trigger the timer (below) on render
					timer.call(api.elements.tooltip, event);
				}
			}
		}).removeData('qtip');
	};

	function createGrowlFromJSON (jsonResponse, title, persistent) {

		// ERROR TODO: HOW TO WE SET THIS?  BY ENV?
		debugMode = true;

		var message = "<p>";

		// Populate the error details
		for(var i=0;i<jsonResponse.response.detailMessages.length;i++){
			message = message + "" +  jsonResponse.response.detailMessages[i] + "<br>";
		}

		message = message + "</p>";

		// Add in field level errors, if they exists
		if ( jsonResponse.response.fieldErrors !== null )
		{
			message = message + "<p><b>Validation Errors:</b></p><p>";

			for(var key in jsonResponse.response.fieldErrors){
				var attrName = key;
				var attrValue = jsonResponse.response.fieldErrors[key];
				message = message + attrName + ": " + attrValue + "<br>";
			}
		}

		// Print debug-only info
		if ( debugMode )
		{

			if ( jsonResponse.response.exception !== null )
			{

				// Get down to the root exception
				var cause = jsonResponse.response.exception;
				while ( cause.cause !== null )
				{
					cause = cause.cause;
				}

				message = message + "<p><b>Debug Information</b></p><p>";
				message = message + "Exception: " + cause.message + "<br>";
				if ( cause.stackTrace !== null )
				{
					message = message + "Line #" + cause.stackTrace[0].lineNumber + " of " + cause.stackTrace[0].className + "." + cause.stackTrace[0].methodName + "<br>";
				}
				message = message + "</p>";

			}
		}

		createGrowl(message, title, persistent);

	};



	window.updateGrowls = function updateGrowls() {
		// Loop over each jGrowl qTip
		var each = $('.qtip.jgrowl'),
			width = each.outerWidth(),
			height = each.outerHeight(),
			gap = each.eq(0).qtip('option', 'position.adjust.y'),
			pos;

		each.each(function(i) {
			var api = $(this).data('qtip');

			// Set target to window for first or calculate manually for subsequent growls
			api.options.position.target = !i ? $(window) : [
				pos.left + width, pos.top + (height * i) + Math.abs(gap * (i-1))
			];
			api.set('position.at', 'top right');

			// If this is the first element, store its finak animation position
			// so we can calculate the position of subsequent growls above
			if(!i) { pos = api.cache.finalPos; }
		});
	};

	// Setup our timer function
	window.timer = function timer(event) {
		var api = $(this).data('qtip'),
			lifespan = 5000; // 5 second lifespan

		// If persistent is set to true, don't do anything.
		if (api.get('show.persistent') === true) { return; }

		// Otherwise, start/clear the timer depending on event type
		clearTimeout(api.timer);
		if (event.type !== 'mouseover') {
			api.timer = setTimeout(api.hide, lifespan);
		}
	};

	// Utilise delegate so we don't have to rebind for every qTip!
	$(document).delegate('.qtip.jgrowl', 'mouseover mouseout', timer);

});