<%@ include file="header.jsp" %>
<body>
	<link rel="stylesheet" href="js/libs/qunit-1.9.0.css" media="screen">

	<header>
		<img src="img/logo.gif" alt="Comcast" border="0" />
	</header>

	<h1 id="qunit-header">QUnit Test Suite</h1>
	<h2 id="qunit-banner"></h2>
	<div id="qunit-testrunner-toolbar"></div>
	<h2 id="qunit-userAgent"></h2>
	<ol id="qunit-tests"></ol>
	<div id="qunit-fixture">test markup</div>

	<div id="chrome" class="autosmop" style="display: none;">

	</div>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.js"></script>
	<script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.1.js"><\/script>')</script>

	<%@ include file="jqplotscripts.jsp" %>
	<%@ include file="scripts.jsp" %>

<!-- test runner files -->
	<script type="text/javascript" src="js/libs/qunit-1.9.0.js"></script>
	<script type="text/javascript" src="js/libs/sinon-1.4.2.js"></script>
    <script type="text/javascript" src="js/libs/sinon-qunit-1.0.0.js"></script>
	<script type="text/javascript">
		QUnit.config.autostart = false;
		$.get('v/${ environmentConfig.getBuildVersion() }/templates/views.html', function (data) {
			$('body').append(data);
			$.get('v/${ environmentConfig.getBuildVersion() }/templates/ticket.html', function (data) {
				$('body').append(data);
				$.get('v/${ environmentConfig.getBuildVersion() }/templates/fields.html', function (data) {
					$('body').append(data);
					start();
				});
			});
		});
	</script>

	<script>
		ROLE_USER = 'ROLE_USER';
		ROLE_EDIT = 'ROLE_EDIT';
		ROLE_VIEW = 'ROLE_VIEW';
		ROLE_CREATE = 'ROLE_CREATE';
		ROLE_VIEWALL = 'ROLE_VIEWALL';
		ROLE_ADMIN = 'ROLE_ADMIN';
		ROLE_METRICS = 'ROLE_METRICS';
		user = {
			username: 'user2',
			roles: ['ROLE_USER','ROLE_EDIT','ROLE_CREATE']
		};
		var a = 1;
	</script>

<!--
	<script src="tests.js"></script>
	<script src="qunitTests/AuthenticationTest.js"></script>
	<script src="qunitTests/TicketControllerTests.js"></script>
	<script src="qunitTests/MockTicketController1Test.js"></script>
-->
<!--	<script src="qunitTests/OverviewViewLoadingTest.js"></script> -->
<!--	<script src="js/tests/validationtests.js"></script>-->
	<script src="js/tests/FieldTest.js"></script>
<!--	<script src="js/tests/containerfield.js"></script>-->
<!--	<script src="js/tests/SmopGeneratorControllerTests.js"></script> -->
	<script src="js/tests/CreateTicketTest.js"></script>
	<script src="js/tests/CNMDBServiceTest.js"></script>
	<%@ include file="footer.jsp" %>
</body>
</html>

