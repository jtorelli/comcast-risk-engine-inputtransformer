
<footer>
	<div>
		<div class="powered-logo">
			<a href="http://intersourcellc.com" target="_blank">
				<img src="img/pbi.jpg" alt="Powered By Intersource" />
			</a>
		</div>

		
			<div>
			   <span class="name">${environmentConfig.productName}</span><br>
			   <span class="build">${environmentConfig.productBuildInfo}</span>
			</div>
		</div>
	</div>
</footer>
