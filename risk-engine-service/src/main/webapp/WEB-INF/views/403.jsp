<%@ include file="header.jsp" %>
	<body >
		<link rel="stylesheet" href="css/style.css">
		<header class="logo-header">
			<h1><img src="img/logo.gif" alt="Comcast" border="0" /></h1>
			<section id="user">
				<a href="j_spring_security_logout">Log out</a><br />
			</section>
		</header>
		<div id="main" class="unauthorized-main" role="main">
			You are not authorized to view this page.
		</div>
 <%@ include file="footer.jsp" %>
	</body>
</html>
