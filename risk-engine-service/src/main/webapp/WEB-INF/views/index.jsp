<%@page import="java.security.Principal"%>
<%@ include file="header.jsp" %>
 <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>    
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<head>
  <title>Backbone.js list</title>
  <script src="js/lib/jquery.js"></script>
  <script src="js/lib/underscore.js"></script>
  <script src="js/lib/setting.js"></script>
  <script src="js/lib/backbone.js"></script>
  <script src="js/lib/backbone.localstorage.js"></script>
  <script src="js/app/rules.js"></script>
  <link rel="stylesheet" href="css/rules.css"/>
  

</head>


<body>


  <div id="todoapp">


    <header>
      <div class="rule">Rules</div>
      <input id="new-todo" type="text" placeholder="Rule?">

    </header>


    <section id="main">
      <input id="toggle-all" type="checkbox">
      <label for="toggle-all">Select To Remove All</label>
      <ul id="todo-list"></ul>
    </section>


    <footer>
      <a id="clear-completed">Remove completed</a>
      <div id="todo-count"></div>
      
    </footer>


  </div>


  <div id="instructions">
    Double-click to edit a rule.
  </div>

    <!-- Templates -->

  <script type="text/template" id="item-template">


   <div class="viewRuleName">
      <input class="toggle" type="checkbox" <@= done ? 'checked="checked"' : '' @> />
      <label><@- ruleName@></label>
            <a class="destroy"></a>

       <div class="execute">Execute</div>
   </div>
          
    <input class="editRuleName" type="text" value="<@- ruleName@>" />
    <br>
    <div class="viewRuleDescription">     
        <label><@- ruleDescription@></label>

    </div>
    
    <input class="editRuleDescription" type="text" value="<@- ruleDescription@>" />
    <br>
    <div class="viewRuleText">
        <label><@- ruleText@></label>
    </div>
    
    <textarea class="editRuleText" type="text"><@- ruleText @></textarea>   
    <br>
    <div class="viewruleInputParameters">
        <label><@- ruleInputParameters@></label>
    </div>
    
    <textarea class="editruleInputParameters" type="text"><@- ruleInputParameters @></textarea>
    <br/>
    <div class = "result"></div>
  </script>


  <script type="text/template" id="stats-template">
    <@ if (done) { @>
      <a id="clear-completed">Remove <@= done @> selected <@= done == 1 ? 'item' : 'items' @></a>
    <@ } @>
    <div class="todo-count"><b><@= remaining @></b> <@= remaining == 1 ? 'item' : 'items' @> left</div>
  </script>

 
  </body>
</html>


