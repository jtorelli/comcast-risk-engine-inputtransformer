<%@ include file="header.jsp" %>
<body onload='document.login.j_username.focus();'>
	<div class="compatibility">
		<div>
			<p>This site requires IE8 with Compatibility View disabled.</p>
			<p>To turn off Compatibility View go to the Tools menu and deselect Compatibility View.</p>
		
		</div>
	</div>
	<header>
		<img src="img/logo.gif" alt="Comcast" border="0" />
	</header>

	<div id="chrome" class="login-wrapper">
		<form name="login" id="login" action="<c:url value='j_spring_security_check' />" method="POST">
			<dl>
				<dt>Username:</dt>
				<dd><input type='text' name='j_username' class="j_username form-field" value=''></dd>
				<dt>Password:</dt>
				<dd><input type='password' name='j_password' class="j_password form-field" /></dd>
				<dt>&nbsp;</dt>
				<dd><input type="submit" class="button" value="Log in" /></dd>
			</dl>
		</form>
		<c:if test="${param.error != null}">
			<div class="errorblock">
				<!--Your login attempt was not successful, try again.<br /> Caused :-->
				${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
			</div>
		</c:if>

		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.js"></script>
		<script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.1.js"><\/script>')</script>

		<c:if test="${environmentConfig.currentEnvironment != 'prod'}">
			Quick login links:
			<ul>
				<li><a href="#" class="quicklogin">jpucci00</a></li>
				<li><a href="#" class="quicklogin">farcan000</a></li>
				
			</ul>
			<script>
				(function(){

					function login(username, password) {
						$('.j_username').val(username);
						$('.j_password').val(password);
						$('form').submit();
					}
					$('.quicklogin').click(function (e) {
						e.preventDefault();
						login ($(this).text(), $(this).text());
						return false;
					});
				})();
			</script>
		</c:if>
	</div>

	<%@ include file="footer.jsp" %>
	<%@ include file="jqplotscripts.jsp" %>
	
</body>
</html>
