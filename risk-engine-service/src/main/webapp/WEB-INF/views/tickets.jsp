<%@ include file="header.jsp" %>
<body>
	<div class="compatibility">
		<div>
			<p>This site requires IE8 with Compatibility View disabled.</p>
			<p>To turn off Compatibility View go to the Tools menu and deselect Compatibility View.</p>
			
		</div>
	</div>
	<div id="chrome" class="autosmop">

	</div>
	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.js"></script>
	<script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.1.js"><\/script>')</script>
	
	<%@ include file="scripts.jsp" %>
	
	<script>
		ROLE_USER = 'ROLE_USER';
		ROLE_EDIT = 'ROLE_EDIT';
		ROLE_VIEW = 'ROLE_VIEW';
		ROLE_CREATE = 'ROLE_CREATE';
		ROLE_VIEWALL = 'ROLE_VIEWALL';
		ROLE_ADMIN = 'ROLE_ADMIN';
		ROLE_METRICS = 'ROLE_METRICS';
		user = {
			username: '<sec:authentication property="principal.username" />',
			roles: eval($('<div />').html('<sec:authentication property="principal.authorities" />').text())
		};
		var a = 1;
	</script>
	<script type="text/javascript">
	$(function(){
		$.get('v/${ environmentConfig.getBuildVersion() }/templates/views.html', function (data) {
			$('body').append(data);
			$.get('v/${ environmentConfig.getBuildVersion() }/templates/ticket.html', function (data) {
				$('body').append(data);
				$.get('v/${ environmentConfig.getBuildVersion() }/templates/fields.html', function (data) {
					$('body').append(data);
					App.Routes.appRouter = new App.Routes.AppRouter();
					App.views.chrome = new App.Views.ChromeView({el: $("#chrome")}).render();
					Backbone.history.start();
				});
			});
		});
	});
	</script>

        <%@ include file="footer.jsp" %>
</body>
</html>

