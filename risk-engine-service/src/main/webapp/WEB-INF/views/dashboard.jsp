<%@ include file="header.jsp" %>
<body>
	<header>
		<img src="img/logo.gif" alt="Comcast" border="0" />
	</header>
	<div id="chrome" class="dashboard">

	</div>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.js"></script>
	<script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.1.js"><\/script>')</script>

	<%@ include file="jqplotscripts.jsp" %>
	<%@ include file="scripts.jsp" %>


	<script type="text/javascript" src="v/${ environmentConfig.getBuildVersion() }/js/app/views/dashboardview.js"></script>
	<script type="text/javascript">
	$(function(){
		$.get('v/${ environmentConfig.getBuildVersion() }/templates/dashboard.html', function (data) {
			$('body').append(data);
			App.Routes.appRouter = new App.Routes.AppRouter();
			App.views.dashboard = new App.Views.DashboardView({el: $("#chrome")});
			App.views.dashboard.render();

			Backbone.history.start();
		});
	});
	</script>
	<%@ include file="footer.jsp" %>
</body>
</html>
