<!--
<script type="text/javascript" src="js/libs/underscore-1.3.1.js"></script>
<script type="text/javascript" src="js/libs/backbone-0.9.1.js"></script>
<script type="text/javascript" src="js/libs/moment.min.js"></script>
-->
<!--<script type="text/javascript" src="js/libs/jquery-ui-1.8.23.custom.min.js"></script>-->
<!--
<script type="text/javascript" src="v/${ environmentConfig.getBuildVersion() }/js/libs/jquery.qtip.js"></script>
<script type="text/javascript" src="v/${ environmentConfig.getBuildVersion() }/js/libs/jquery.cookie.js"></script>
-->

<!--
<script type="text/javascript" src="v/${ environmentConfig.getBuildVersion() }/js/util.js"></script>
<script type="text/javascript" src="v/${ environmentConfig.getBuildVersion() }/js/plugins.js"></script>
-->
<!--
<script type="text/javascript" src="v/${ environmentConfig.getBuildVersion() }/js/app.js"></script>
<script type="text/javascript" src="v/${ environmentConfig.getBuildVersion() }/js/app/models.js"></script>
<script type="text/javascript" src="v/${ environmentConfig.getBuildVersion() }/js/app/validators.js"></script>
<script type="text/javascript" src="v/${ environmentConfig.getBuildVersion() }/js/app/views/abstractview.js"></script>
<script type="text/javascript" src="v/${ environmentConfig.getBuildVersion() }/js/app/views/autosmopview.js"></script>
<script type="text/javascript" src="v/${ environmentConfig.getBuildVersion() }/js/app/views/homeview.js"></script>
<script type="text/javascript" src="v/${ environmentConfig.getBuildVersion() }/js/app/views/tabbedview.js"></script>
<script type="text/javascript" src="v/${ environmentConfig.getBuildVersion() }/js/app/views/fields/abstractfield.js"></script>
<script type="text/javascript" src="v/${ environmentConfig.getBuildVersion() }/js/app/views/fields/containerfield.js"></script>
<script type="text/javascript" src="v/${ environmentConfig.getBuildVersion() }/js/app/views/fields/fieldviews.js"></script>
<script type="text/javascript" src="v/${ environmentConfig.getBuildVersion() }/js/app/views/fields/ticketview.js"></script>
<script type="text/javascript" src="v/${ environmentConfig.getBuildVersion() }/js/app/views/metricsview.js"></script>
<script type="text/javascript" src="v/${ environmentConfig.getBuildVersion() }/js/app/views/views.js"></script>
<script type="text/javascript" src="v/${ environmentConfig.getBuildVersion() }/js/app/views/ticketlistview.js"></script>
<script type="text/javascript" src="v/${ environmentConfig.getBuildVersion() }/js/app/views/overviewview.js"></script>
<script type="text/javascript" src="v/${ environmentConfig.getBuildVersion() }/js/app/views/chromeview.js"></script>
<script type="text/javascript" src="v/${ environmentConfig.getBuildVersion() }/js/app/views/chartview.js"></script>
<script type="text/javascript" src="v/${ environmentConfig.getBuildVersion() }/js/app/routes.js"></script>
-->

<script data-main="js/main" src="js/libs/require.js"></script>

<!-- end scripts-->


<!--[if lt IE 7 ]>
	<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
	<script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
<![endif]-->
