<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<% response.setHeader("X-UA-Compatible", "IE=edge,chrome=1");%>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<link rel="shortcut icon" media="all" type="image/vnd.microsoft.icon" href="img/favicon.ico">
		<link rel="apple-touch-icon" href="img/apple-touch-icon.png" />
		<link rel="apple-touch-icon" sizes="57x57" href="img/apple-touch-icon-57x57-precomposed" />
		<link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed" />
		<link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114-precomposed" />
		<title>Risk Engine Demo</title>
		<meta name="description" content="">
		<meta name="author" content="">

		<meta name="viewport" content="width=device-width,initial-scale=1">

		<!-- CSS concatenated and minified via ant build script-->
		<link rel="stylesheet" href="v/${ environmentConfig.getBuildVersion() }/css/style.css">
		<link rel="stylesheet" href="js/libs/jquery.qtip.css">
		<link rel="stylesheet" href="js/libs/jqplot/src/jquery.jqplot.css" />
		<link rel="stylesheet" href="css/flick/jquery-ui-1.8.23.custom.css" />
		<!-- end CSS-->

		<script src="js/libs/modernizr-2.0.6.min.js"></script>
	</head>
