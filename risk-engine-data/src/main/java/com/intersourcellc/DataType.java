
package com.intersourcellc;

public enum DataType 
{
    NONE,
    INT, 
    BOOLEAN, 
    DOUBLE, 
    STRING,
    JSON,
    FETCHABLE
}