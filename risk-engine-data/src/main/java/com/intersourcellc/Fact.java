
package com.intersourcellc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Strings.*;

public class Fact implements INamedAsset {
    static final Logger Log = LoggerFactory.getLogger(Fact.class);
    private String name;
    private String value;
    private DataType type;
    private Request request;

    public Fact(String name, String value, DataType type) {
        this.name = name;
        this.setValueType(value, type);
        Log.debug("Created new fact: {}", this);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValueType(String value, DataType type) {
        // reduce to call: 
        DataTypeValidation.tryCast(value, type);

        this.type = type;
        this.value = value;
    }

    public Object value() {
        Object result;
        switch (this.type) {
            case INT:
                result = Integer.parseInt(this.value);
                break;
            case DOUBLE:
                result = Double.parseDouble(this.value);
                break;
            case BOOLEAN:
                result = Boolean.parseBoolean(this.value);
                break;
            case STRING:
            case FETCHABLE:
            case JSON:
                result = this.value;
                break;
            case NONE:
            default:
                result = null;
                break;
        }
        return result;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public DataType getType() {
        return type;
    }

    public void setType(DataType type) {
        this.type = type;
    }

    public Request getRequest() {
        return request;
    }

    public Request setRequest(Request request) {
        Request req = request;
        if (req != null && this.type == DataType.FETCHABLE) {
            this.request = request;
        }
        return req;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("Name: ");
        stringBuilder.append(isNullOrEmpty(name) ? "NULL" : name);
        stringBuilder.append(", Value: ");
        stringBuilder.append(isNullOrEmpty(value) ? "NULL" : value);
        stringBuilder.append(", DataType: ");
        stringBuilder.append(type == null ? "NULL" : type.name());
        stringBuilder.append(", Request: ");
        stringBuilder.append(request == null ? "NULL" : request.toString());

        return stringBuilder.toString();
    }
}
