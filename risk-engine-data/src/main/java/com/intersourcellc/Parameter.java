
package com.intersourcellc;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import static com.google.common.base.Strings.isNullOrEmpty;

public class Parameter
{
    private String token; 
    private String key; 
    private String value;

    private Map<Integer, String> tagMap;
    
    public Parameter(String token, String key, String value)
    {
        this.token = token; 
        this.key = key; 
        this.value = value; 
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
    
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void applyQueryRequestMapTokens(Map<String, String[]> queryRequestMap) {
        if (tagMap == null) {
            extractTagMap();
        }

        for (Map.Entry<Integer, String> tag : tagMap.entrySet()) {
            //FIXME: Not sure why there should be more than one value for a tag. (rmb)
            Integer tagKey = tag.getKey();
            String tagValue = tag.getValue();
            String newValue = queryRequestMap.get(tagValue)[0];
            tagMap.put(tagKey, newValue);
        }

        value = String.format(value, tagMap.values().toArray());
    }

    private void extractTagMap() {
        tagMap = new TreeMap<>();
        int offset = -1;
        int onset = value.indexOf("{");
        int index = 0;

        if (onset > offset) {
            StringBuilder newValue = new StringBuilder(value.length());

            newValue.append(value.substring(0, onset));

            while (onset > offset) {
                offset = value.indexOf("}", onset);

                tagMap.put(index, value.substring(onset + 1, offset));
                newValue.append("%s");

                onset = value.indexOf("{", offset);
            }

            newValue.append(value.substring(offset + 1, value.length()));
            value = newValue.toString();
        }
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("Token: ");
        stringBuilder.append(isNullOrEmpty(token) ? "NULL" : token);
        stringBuilder.append(", Key: ");
        stringBuilder.append(isNullOrEmpty(key) ? "NULL" : key);
        stringBuilder.append(", Value: ");
        stringBuilder.append(isNullOrEmpty(value) ? "NULL" : value);

        return stringBuilder.toString();
    }

}
