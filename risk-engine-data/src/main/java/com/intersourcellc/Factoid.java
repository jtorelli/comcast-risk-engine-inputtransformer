
package com.intersourcellc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.DataFormatException;

//@PropertyReactive
public class Factoid extends HashMap<String, Fact> {
    static final Logger logger = LoggerFactory.getLogger(Factoid.class);

    protected List<String> trace;
    // Drools chaining: 
    private int step; 
    // basic chaining: 
    private String next; 

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }
    
    public Factoid()
    {
        this.trace = new ArrayList<>();
    }
    
    public HashMap<String, Fact> getFacts() {
        return this;
    }

    public HashMap<String, Fact> getFetchables() {
        HashMap<String, Fact> facts = getFacts();
        HashMap<String, Fact> out = new HashMap<>();

        for (Fact fact: facts.values()) {
            if (fact.getType() == DataType.FETCHABLE) {
                out.put(fact.getName(), fact);
            }
        }

        return out;
    }

    public void addFact(Fact fact) {
        if (this.containsKey(fact.getName())) {
            this.addTrace("Attempt to overwrite fact: " + fact.getName());
        } else {
            this.put(fact.getName(), fact);
            logger.debug("addFact: {}", fact);
        }
    }

    public void addFact(String name, String value, DataType type) {
        if (this.containsKey(name)) 
        {
            this.addTrace("Attempt to overwrite fact: " + name);
        } else {
            Fact fact = new Fact(name, value, type);
            this.put(name, fact);
            logger.debug("addFact: {}", fact);
        }
    }

    public void setFact(Fact fact) {
        if (this.containsKey(fact.getName())) {
            this.addTrace("Overwriting fact: " + fact.getName());
            Fact oldFact = this.get(fact.getName());
            this.addTrace("Changing value from " + oldFact.value() + " to " + fact.value());
        }
        this.put(fact.getName(), fact);
        logger.debug("setFact: {}", fact);
    }
    
    public void setFact(String name, String value, DataType type) {
        if (this.containsKey(name)) {
            this.addTrace("Overwriting fact: " + name);
            Fact oldFact = this.get(name);
            this.addTrace("Changing value from " + oldFact.value() + " to " + value);
        }
        Fact fact = new Fact(name, value, type);
        this.put(name, fact);
        logger.debug("setFact: {}", fact);
    }

    public Fact getFact(String name) {
        Fact fact = null;

        if (this.containsKey(name)) {
            fact = this.get(name);
        }
        
        return fact;
    }
    
    // internal trace:
    public void addTrace(String message)
    {        
        this.trace.add(message); 
    }
    
    public List<String> getTrace() {
        return trace;
    }
    
    @Override
    public String toString()
    {       
        String result = ""; 
        Fact fa;
        for (Map.Entry<String, Fact> entry : this.entrySet()) {
            fa = entry.getValue();
            result += fa.getType().toString() + " " + fa.getName() + " = " + fa.value().toString() + ";\n";
        }        
        return result;  
    }

    public void merge(Factoid another) {
        try
        {
            this.putAll(another);
            this.trace.addAll(another.getTrace());
        }
        catch (NullPointerException npex)
        {
            throw new RuntimeException(npex); //TODO: (rmb) Fix exception handling
        }
    }

}
