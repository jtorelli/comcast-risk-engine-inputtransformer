
package com.intersourcellc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Request extends HashMap<String, Parameter> 
{
    private RequestType requesttype; 
    private String body; 
    private String url; 
//    private Header header; 
    List<Header> headers; 
     
    public String getUrl() {
        return url;
    }
    
    public void setUrl(String url) {
        this.url = url;
    }  
    
//    public Header getHeader() {
//        return header;
//    }
    public List<Header> getHeaders()
    {
        return headers; 
    }

//    public void setHeader(Header header) {
//        this.header = header;
//    }
    
    public void addHeader(Header header)
    {
        if (this.headers == null)
        {
            this.headers = new ArrayList<>();
        }
         this.headers.add(header);
    }
    
    public Request(RequestType requesttype, String body)
    {
        this.requesttype = requesttype; 
        this.body = body; 
    }
    
    public Request(RequestType requesttype, String body, String url)
    {
        this.requesttype = requesttype; 
        this.body = body; 
        this.url = url; 
    }

    public RequestType getRequestType() {
        return requesttype;
    }

    public void setRequesttype(RequestType requesttype) {
        this.requesttype = requesttype;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
    
    public HashMap<String, Parameter> getParameters()
    {
        return this; 
    }
    
    public void addParameter(Parameter parameter) {
        if (!this.containsKey(parameter.getKey())) {            
            this.put(parameter.getKey(), parameter);
        }
    }
    
    public void addParameter(String token, String key, String value) {
        if (!this.containsKey(key)) { 
            this.put(key, new Parameter(token, key, value));
        }
    }
    
     public void setParameter(Parameter parameter) {
        this.put(parameter.getKey(), parameter);
    }

    public void setParameter(String token, String key, String value) {
        this.put(key, new Parameter(token, key, value));
    }
    
    public Parameter getParameter(String name) {
        Parameter parameter = null;

        if (this.containsKey(name)) {
            parameter = this.get(name);
        }
        
        return parameter;
    }
}
