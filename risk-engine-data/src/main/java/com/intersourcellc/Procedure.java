package com.intersourcellc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Ryan on 6/14/13.
 */
public class Procedure implements INamedAsset {
    final static Logger log = LoggerFactory.getLogger(Procedure.class);

    private final String name;
    private final String content;
    private EngineType enginetype; 
    
    public Procedure(String name, String content, EngineType enginetype) {
        this.name = name;
        this.content = content;
        this.enginetype = enginetype; 
    }

    public Procedure(File file, EngineType engineType) {
        this.name = file.getName();
        this.enginetype = engineType;

        BufferedReader bufferedReader = null;
        StringBuilder stringBuilder = new StringBuilder();

        try {
            String currentLine;

            bufferedReader = new BufferedReader(new FileReader(file));

            while ((currentLine = bufferedReader.readLine()) != null) {
                stringBuilder.append(currentLine);
            }

        } catch (IOException e) {
            throw new RuntimeException("I/O Exception", e); //TODO: (rmb) Need to replace with better exception handling
        } finally {
            try {
                if (bufferedReader != null)bufferedReader.close();
            } catch (IOException e) {
                throw new RuntimeException("I/O Exception", e); //TODO: (rmb) Need to replace with better exception handling
            }
        }

        this.content = stringBuilder.toString();
    }

    @Override
    public String getName() {
        return this.name;
    }

    public String getContent() {
        return this.content;
    }
    
    public EngineType getEnginetype() {
        return enginetype;
    }

    public void setEnginetype(EngineType enginetype) {
        this.enginetype = enginetype;
    }
}
