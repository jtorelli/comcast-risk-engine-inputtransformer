
package com.intersourcellc;

import com.intersourcellc.exception.RiskEngineApplicationException;
import java.util.zip.DataFormatException;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class DataTypeValidation 
{
    public static boolean isCastable(String value, DataType type) throws DataFormatException 
    {
        boolean result = true; 
        
        try 
        {
            switch (type)
            {
                case INT:  
                    Integer.parseInt(value) ; 
                    break; 
                case DOUBLE: 
                    Double.parseDouble(value); 
                    break; 
                case BOOLEAN:
                    if (!value.equals("true") && !value.equals("false"))
                    {
                        throw new DataFormatException ("Value " + value + " cannot be set to DataType " + type.toString() + ". "); 
                    }                    
                    break;                
                case JSON:          
                    JSONParser parser = new JSONParser();
                    try
                    {
                      parser.parse(value);
                    }
                    catch(ParseException pe)
                    {                      
                      // make specific: 
                      throw new DataFormatException ("Value " + value + " violates JSON syntax: " + pe + ". "); 
                    }
                case STRING:
                case FETCHABLE:     // custom validation? 
                case NONE:          // value data type not set; exception?                               
                default: 
                    break;
            } 
        }
        catch (DataFormatException ex)
        {
            result = false; 
        }
        
        return result; 
    }
    
    // throw new RiskEngineApplicationException();
    public static void tryCast(String value, DataType type) throws RiskEngineApplicationException
    {
        switch (type)
            {
                case INT:  
                    Integer.parseInt(value) ; 
                    break; 
                case DOUBLE: 
                    Double.parseDouble(value); 
                    break; 
                case BOOLEAN:
                    if (!value.equals("true") && !value.equals("false"))
                    {
//                        throw new DataFormatException ("Value " + value + " cannot be set to DataType " + type.toString() + ". "); 
                        throw new RiskEngineApplicationException("Value " + value + " cannot be set to DataType " + type.toString() + ". "); 
                    }                    
                    break;                
                case JSON:          
                    JSONParser parser = new JSONParser();
                    try
                    {
                      parser.parse(value);
                    }
                    catch(ParseException pe)
                    {                      
                      // make specific: 
//                      throw new DataFormatException ("Value " + value + " violates JSON syntax: " + pe + ". ", pe); 
//                        throw pe;
                        throw new RiskEngineApplicationException("Value " + value + " cannot be set to DataType " + type.toString() + ". ", pe); 
                    }
                case STRING:
                case FETCHABLE:     // custom validation? 
                case NONE:          // value data type not set; exception?                               
                default: 
                    break;
            }
    }
}
