
package com.intersourcellc;

import java.util.HashMap;

//public class Header extends HashMap<String, String>
public class Header extends HashMap<String, Parameter>
{
    public Header()
    {
        
    }
     
//    public void addKeyValue(String key, String value)
//    {
//        if (!this.containsKey(key))
//        {
//            this.put(key, value); 
//        }
//    }
//    
//    public String getValue(String key)
//    {
//        return this.get(key); 
//    }
    
    public void addParameter(Parameter parameter) {
        if (!this.containsKey(parameter.getKey())) {            
            this.put(parameter.getKey(), parameter);
        }
    }
    
    public void addParameter(String token, String key, String value) {
        if (!this.containsKey(key)) { 
            this.put(key, new Parameter(token, key, value));
        }
    }
    
    public void setParameter(Parameter parameter) {
        this.put(parameter.getKey(), parameter);
    }

    public void setParameter(String token, String key, String value) {
        this.put(key, new Parameter(token, key, value));
    }
    
    public Parameter getParameter(String name) {
        Parameter parameter = null;

        if (this.containsKey(name)) {
            parameter = this.get(name);
        }
        
        return parameter;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        for (String name : this.keySet()) {
            stringBuilder.append("Name: ");
            stringBuilder.append(name);
            stringBuilder.append(", Value: ");
            stringBuilder.append(this.getParameter(name).toString());
            stringBuilder.append(System.getProperty("line.separator"));
        }

        return stringBuilder.toString();
    }
}
