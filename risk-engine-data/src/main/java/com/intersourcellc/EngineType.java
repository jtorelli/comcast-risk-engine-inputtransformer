/*
 * Classification of engines to run rules. 
 */
package com.intersourcellc;

public enum EngineType 
{
    NONE,
    DROOLS, 
    JAVASCRIPT;

    @Override public String toString() {
        String s = super.toString();
        return s.substring(0,1) + s.substring(1).toLowerCase();
    }
}
