
package com.intersourcellc;

import com.intersourcellc.exception.RiskEngineApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.zip.DataFormatException;

public class CoreRule implements INamedAsset {
    final static Logger log = LoggerFactory.getLogger(Factoid.class);
    private String name;
    private Factoid factoid;
    private Procedure procedure;

    public CoreRule(String name, Procedure procedure) {
        this.name = name;
        this.procedure = procedure;
        this.factoid = new Factoid();
    }

    public CoreRule(String name, Procedure procedure, Factoid factoid) {
        this.name = name;
        this.procedure = procedure;
        this.factoid = factoid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EngineType getEngineType() {
        return procedure.getEnginetype();
    }

    public String getProcedureContent() {
        return this.procedure.getContent();
    }

    public void setProcedureContent(String ruleScript) {
        this.procedure = new Procedure(this.procedure.getName(), ruleScript, this.procedure.getEnginetype());
    }

    public Factoid getFactoid() {
        return factoid;
    }

    //Easier to manage Factoid helper functions
    public void mergeFactoid(Factoid another) {
        factoid.merge(another);
    }

    public void addFact(Fact fact) {
        factoid.addFact(fact);
    }

    public void addFact(String name, String value, DataType type) throws DataFormatException {
        factoid.addFact(name, value, type);
    }

    public void setFact(Fact fact) {
        factoid.setFact(fact);
    }

    public void setFact(String name, String value, DataType type) throws DataFormatException {
        factoid.addFact(name, value, type);
    }

    public void addTrace(String message) {
        factoid.addTrace(message);
    }

//    public void seedDataPoints(String queryParameters)
//    {
//        HashMap<String, Parameter> tokens = tokenize(queryParameters);
//        seedDataPoints(tokens); 
//    }

//    public void seedDataPoints(HashMap<String, Parameter> tokens)
//    {      
//        Parameter p; 
//        Request rq; 
//        Map<String, Parameter> params; 
//        
//        if (tokens.size() > 0)
//        {
//            for (Fact fact : this.factoid.values())
//            {
//                if (fact.getType() != DataType.FETCHABLE && tokens.keySet().contains(fact.getName()))
//                {
//                    // set value: 
//                    p = tokens.get(fact.getName()); 
//                    fact.setValue(p.getValue());                    
//                }
//                else if (fact.getType() == DataType.FETCHABLE)
//                {
//                    rq = fact.getRequest(); 
//                    params = rq.getParameters();
//                    for (Parameter par : params.values())
//                    {
//                        if (tokens.keySet().contains(par.getToken()))
//                        {
//                            par.setValue(tokens.get(par.getToken()).getValue());
//                        }
//                    }
//                }
//            }
//        }        
//    }

    public void seedDataPoints(HashMap<String, String> tokens) throws RiskEngineApplicationException 
    {
        String p;
        Request rq;
        HashMap<String, Parameter> params;

//        HashMap<String, Parameter> header;
//        List<Header> headers; 

        if (tokens.size() > 0) {
            for (Fact fact : this.factoid.values()) {
                // to be set to castable DataType: 
                if (fact.getType() != DataType.FETCHABLE && tokens.keySet().contains(fact.getName())) 
                {
                    // set value: 
                    p = tokens.get(fact.getName());
                    // throws RiskEngineApplicationException:
                    fact.setValueType(p, fact.getType());
                } else if (fact.getType() == DataType.FETCHABLE) {
                    rq = fact.getRequest();
                    params = rq.getParameters();
                    for (Parameter par : params.values()) {
                        if (tokens.keySet().contains(par.getToken())) {
                            // set value:
                            par.setValue(tokens.get(par.getToken()));
                        }
                    }

                    if (rq.getHeaders() != null)
                    {
                        for (Header h : rq.getHeaders())
                        {
                            if (h != null) 
                            {
                                for (Parameter hPar : h.values()) 
                                {
                                    if (tokens.keySet().contains(hPar.getToken())) 
                                    {
                                        // set value: 
                                        hPar.setValue(tokens.get(hPar.getToken()));
                                    }
                                }
                            }
                        } 
                    }                                       
                }
            }
        }
    }

    private HashMap<String, Parameter> tokenize(String queryParameters) {
        String[] parmsStrings = queryParameters.split("&");
        String[] tokenString;
        String token, value;

        HashMap<String, Parameter> tokens = new HashMap();

        for (String par : parmsStrings) {
            tokenString = par.split("=");
            if (tokenString.length == 2) {
                token = tokenString[0].trim();
                value = tokenString[1].trim();

                if (token.length() > 0 && value.length() > 0) {
                    tokens.put(token, new Parameter(token, "", value));
                }
            }
        }

        return tokens;
    }
}
