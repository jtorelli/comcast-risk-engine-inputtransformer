package com.intersourcellc;

/**
 * Created by Ryan on 6/14/13.
 */
public interface INamedAsset {
    String getName();
}
