/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intersourcellc;

/**
 *
 * @author v-albelo
 */
public enum RequestType 
{
    NONE,
    GET, 
    POST, 
    PUT
}
