package com.intersourcellc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Ryan on 6/20/13.
 */
public class ProcedureTest {
    final static Logger log = LoggerFactory.getLogger(ProcedureTest.class);

    @Test
    public void testGetName() throws Exception {
        String expected = "testGetName";
        Procedure procedure = new Procedure(expected, "", EngineType.NONE);

        String actual = procedure.getName();
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testGetContent() throws Exception {
        String expected = "content";
        Procedure procedure = new Procedure("", expected, EngineType.NONE);

        String actual = procedure.getContent();
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testGetEnginetype() throws Exception {
        EngineType expected = EngineType.JAVASCRIPT;
        Procedure procedure = new Procedure("", "", expected);

        EngineType actual = procedure.getEnginetype();
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testSetEnginetype() throws Exception {
        EngineType expected = EngineType.JAVASCRIPT;
        Procedure procedure = new Procedure("", "", EngineType.NONE);

        procedure.setEnginetype(expected);

        EngineType actual = procedure.getEnginetype();
        Assert.assertEquals(actual, expected);
    }
}
