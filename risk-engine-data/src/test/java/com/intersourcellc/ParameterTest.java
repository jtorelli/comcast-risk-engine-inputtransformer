package com.intersourcellc;

import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashMap;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

/**
 * Created by Ryan on 8/5/13.
 */
public class ParameterTest {
    @Mock
    Parameter mockParameter;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test(enabled = false)
    void testApplyQueryRequestMapTokens_CallsExtractTagMapOnce() {
        //Given
        //doNothing().when(mockParameter).extractTagMap();
        doCallRealMethod().when(mockParameter).applyQueryRequestMapTokens(anyMapOf(String.class, String[].class));
        // mockQueryRequestMap is a generic which cannot be described by Mockito without causing Lint warnings
        @SuppressWarnings("unchecked") HashMap<String, String[]> mockQueryRequestMap = mock(HashMap.class);

        //When
        mockParameter.applyQueryRequestMapTokens(mockQueryRequestMap);

        //Then
        //verify(mockParameter, times(1)).extractTagMap();
    }

}
