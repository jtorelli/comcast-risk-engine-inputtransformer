package com.intersourcellc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;
import java.util.zip.DataFormatException;

/**
 * Created by Ryan on 6/17/13.
 */
public class FactoidTest {
    static final Logger log = LoggerFactory.getLogger(FactoidTest.class);
    Factoid factoid;

    @BeforeMethod
    public void setup() {
        factoid = new Factoid();
    }

    @Test
    public void AddFact_FromFact_SavedInFactoid() {
        boolean expectedResult = true;

        Fact testFact = new Fact("testFact", String.valueOf(expectedResult), DataType.BOOLEAN);
        factoid.addFact(testFact);

        Assert.assertTrue(factoid.containsKey("testFact"));

        boolean actualResult = (boolean) factoid.get("testFact").value();
        Assert.assertEquals(actualResult, expectedResult);
        log.debug(factoid.getTrace().toString());
    }

    @Test
    public void AddFact_FromTuple_SavedInFactoid() {
        boolean expectedResult = true;

        factoid.addFact("testFact", String.valueOf(expectedResult), DataType.BOOLEAN);

        Assert.assertTrue(factoid.containsKey("testFact"));

        boolean actualResult = (boolean) factoid.get("testFact").value();
        Assert.assertEquals(actualResult, expectedResult);
        log.debug(factoid.getTrace().toString());
    }

    @Test
    public void AddFact_AlreadyInFactoid_NotSavedInFactoidAndAttemptToReplaceAddedToTrace() {
        boolean expectedResult = true;

        boolean testFact = false;
        factoid.addFact("testFact", String.valueOf(testFact), DataType.BOOLEAN);

        Assert.assertTrue(factoid.containsKey("testFact"));

        factoid.addFact("testFact", String.valueOf(expectedResult), DataType.BOOLEAN);

        Assert.assertTrue(factoid.containsKey("testFact"));

        boolean actualResult = (boolean) factoid.get("testFact").value();
        Assert.assertNotEquals(actualResult, expectedResult, "Fact was overwritten");

        List<String> trace = factoid.getTrace();
        boolean traceHasItem = false;
        for (String item : trace) {
            if (item.contains("Attempt to overwrite fact: testFact")) {
                traceHasItem = true;
                break;
            }
        }
        Assert.assertTrue(traceHasItem, "Trace did not indicate attempt to overwrite fact");
        log.debug(factoid.getTrace().toString());
    }

    @Test
    public void SetFact_FromFact_SavedInFactoid() {
        boolean expectedResult = true;

        Fact testFact = new Fact("testFact", String.valueOf(expectedResult), DataType.BOOLEAN);
        factoid.setFact(testFact);

        Assert.assertTrue(factoid.containsKey("testFact"));

        boolean actualResult = (boolean) factoid.get("testFact").value();
        Assert.assertEquals(actualResult, expectedResult);
        log.debug(factoid.getTrace().toString());
    }

    @Test
    public void SetFact_FromTuple_SavedInFactoid() {
        boolean expectedResult = true;

        factoid.setFact("testFact", String.valueOf(expectedResult), DataType.BOOLEAN);

        Assert.assertTrue(factoid.containsKey("testFact"));

        boolean actualResult = (boolean) factoid.get("testFact").value();
        Assert.assertEquals(actualResult, expectedResult);
        log.debug(factoid.getTrace().toString());
    }

    @Test
    public void SetFact_AlreadyInFactoid_SavedInFactoidAndReplacementMessageAddedToTrace() {
        boolean expectedResult = true;

        boolean testFact = false;
        factoid.setFact("testFact", String.valueOf(testFact), DataType.BOOLEAN);

        Assert.assertTrue(factoid.containsKey("testFact"));

        factoid.setFact("testFact", String.valueOf(expectedResult), DataType.BOOLEAN);

        Assert.assertTrue(factoid.containsKey("testFact"));

        boolean actualResult = (boolean) factoid.get("testFact").value();
        Assert.assertEquals(actualResult, expectedResult, "Fact was not overwritten");

        List<String> trace = factoid.getTrace();
        boolean traceHasItem = false;
        for (String item : trace) {
            if (item.contains("Overwriting fact: testFact")) {
                traceHasItem = true;
                break;
            }
        }
        Assert.assertTrue(traceHasItem, "Trace did not indicate it was overwriting a fact");
        log.debug(factoid.getTrace().toString());
    }

    @Test
    public void GetFact_FromDoubleFactoid_RetrievesDoubleFact() {
        Double expectedResult = Math.PI;

        factoid.setFact("testFact", String.valueOf(expectedResult), DataType.DOUBLE);

        Assert.assertTrue(factoid.containsKey("testFact"));

        Double actualResult = (Double) factoid.get("testFact").value();
        Assert.assertEquals(actualResult, expectedResult);
        log.debug(factoid.getTrace().toString());
    }

    @Test
    public void GetFetchables_FromFetchablesFactoid_RetrievesFetchableFacts() {
        String expectedResult = "http://example.com";

        factoid.setFact("testFact1", String.valueOf(Math.PI), DataType.DOUBLE);
        factoid.setFact("testFact2", expectedResult, DataType.FETCHABLE);
        factoid.setFact("testFact3", "String Fact", DataType.STRING);

        Map<String, Fact> fetchables = factoid.getFetchables();

        Assert.assertTrue(fetchables.size() == 1, "Wrong number of fetchables received");
        Assert.assertTrue(fetchables.containsKey("testFact2"), "Did not find expected fetchable");

        String actualResult = (String) fetchables.get("testFact2").value();
        Assert.assertEquals(actualResult, expectedResult, "Did not recover the correct fetchable");
    }

    @Test
    public void AddTrace_AppendsTraceMessage_SavedInFactoid() {
        String expectedResult = "Testing";

        factoid.addTrace(expectedResult);

        List<String> trace = factoid.trace;
        boolean traceHasItem = false;
        for (String item : trace) {
            if (item.contains(expectedResult)) {
                traceHasItem = true;
                break;
            }
        }
        Assert.assertTrue(traceHasItem, "Trace did not store the trace message");
        log.debug(factoid.getTrace().toString());
    }

    @Test
    public void GetTrace_FromFactoid_RetrievesSavedTraceMessages() {
        String expectedResult = "Testing";

        factoid.trace.add(expectedResult);

        List<String> trace = factoid.getTrace();
        boolean traceHasItem = false;
        for (String item : trace) {
            if (item.contains(expectedResult)) {
                traceHasItem = true;
                break;
            }
        }
        Assert.assertTrue(traceHasItem, "Trace did not store the trace message");
        log.debug(factoid.getTrace().toString());
    }
}
