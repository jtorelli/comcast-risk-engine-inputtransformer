package com.intersourcellc;

import com.intersourcellc.exception.RiskEngineApplicationException;
import java.util.HashMap;
import java.util.zip.DataFormatException;
import org.testng.Assert;
import org.testng.annotations.Test;

public class RuleTest {
    @Test
    public void testGetName() throws Exception {
        String expected = "testGetName";

        CoreRule rule = new CoreRule(expected, new Procedure("ProcedureName", "", EngineType.NONE));

        String actual = rule.getName();

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testSetName() throws Exception {
        String expected = "testSetName";

        CoreRule rule = new CoreRule("ruleNameToBeReplaced", new Procedure("ProcedureName", "", EngineType.NONE));

        rule.setName(expected);
        String actual = rule.getName();

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testGetEngineType() throws Exception {
        EngineType expected = EngineType.JAVASCRIPT;

        CoreRule rule = new CoreRule("testGetEngineType", new Procedure("ProcedureName", "", expected));

        EngineType actual = rule.getEngineType();

        Assert.assertEquals(actual, expected);
    }

//    @Test
//    public void testSetEngineType() throws Exception {
//        EngineType expected = EngineType.JAVASCRIPT;
//
//        CoreRule rule = new CoreRule("testSetEngineType", new Procedure("ProcedureName", "", EngineType.NONE));
//
//        rule.setEngineType(expected);
//        EngineType actual = rule.getEngineType();
//
//        Assert.assertEquals(actual, expected);
//    }

    @Test
    public void testGetRuleScript() throws Exception {
        String expected = "foo";

        CoreRule rule = new CoreRule("testGetRuleScript", new Procedure("ProcedureName", expected, EngineType.NONE));

        String actual = rule.getProcedureContent();

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testSetRuleScript() throws Exception {
        String expected = "foo";

        CoreRule rule = new CoreRule("testSetRuleScript", new Procedure("ProcedureName", "bar", EngineType.NONE));

        rule.setProcedureContent(expected);
        String actual = rule.getProcedureContent();

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testGetFactoid() throws Exception {
        CoreRule rule = new CoreRule("testGetFactoid", new Procedure("ProcedureName", "", EngineType.NONE));

        Factoid factoid = rule.getFactoid();

        Assert.assertNotNull(factoid);
    }

    @Test
    public void testMergeFactoid() {
        CoreRule rule = new CoreRule("testMergeFactoid", new Procedure("ProcedureName", "", EngineType.NONE));

        Factoid factoidOne = new Factoid();
        Factoid factoidTwo = new Factoid();
        Factoid factoidThree = new Factoid();

        Assert.assertEquals(rule.getFactoid().size(), 0, "Factoid was not empty to start with");
        
        factoidOne.setFact("test1Boolean", "false", DataType.BOOLEAN);
        factoidOne.setFact("test1Double", String.valueOf(Math.PI), DataType.DOUBLE);
        factoidOne.setFact("test1Int", "123", DataType.INT);
        factoidOne.setFact("test1String", "Foobar", DataType.STRING);
        factoidOne.setFact("test1Json", "{\"json\":\"test\"}", DataType.JSON);

        rule.mergeFactoid(factoidOne);

        Assert.assertEquals(rule.getFactoid().size(), 5, "Not all 5 facts were stored");

        factoidTwo.setFact("test1Boolean", "true", DataType.BOOLEAN);
        factoidTwo.setFact("test1Double", String.valueOf(Math.E), DataType.DOUBLE);
        factoidTwo.setFact("test1Int", "321", DataType.INT);
        factoidTwo.setFact("test1String", "Foobaz", DataType.STRING);
        factoidTwo.setFact("test1Json", "{\"json\":\"test2\"}", DataType.JSON);

        rule.mergeFactoid(factoidTwo);

        Assert.assertEquals(rule.getFactoid().size(), 5, "Didn't replace facts");

        factoidThree.setFact("test2Boolean", "false", DataType.BOOLEAN);
        factoidThree.setFact("test2Double", String.valueOf(Math.PI), DataType.DOUBLE);
        factoidThree.setFact("test2Int", "123", DataType.INT);
        factoidThree.setFact("test2String", "Foobar", DataType.STRING);
        factoidThree.setFact("test2Json", "{\"json\":\"test\"}", DataType.JSON);

        rule.mergeFactoid(factoidThree);

        Assert.assertEquals(rule.getFactoid().size(), 10, "Not all facts were merged correctly");
        //TODO: Should actually inspect result values, but count tests should reveal any problems.
    }
    
//    @Test
//    public void testSeedingFromString()
//    {
//        String inputString = "& token1 = Seattle& token2 = WA & var_int = 111"; 
//        
//        CoreRule rule = getRuleSample(); 
//        // set tokens-values and parameters: 
//        rule.seedDataPoints(inputString);
//        
//        // get int type value: 
//        int int_result = (int)rule.getFactoid().getFact("var_int").value(); 
//        String city_result = rule.getFactoid().getFact("fetch1").getRequest().getParameter("par_key1").getValue();
//        String state_result = rule.getFactoid().getFact("fetch1").getRequest().getParameter("par_key2").getValue();
//        
//        Assert.assertTrue(int_result == 111 && city_result.equals("Seattle") && state_result.equals("WA") );
//    }
    
//    @Test
//    public void testSeedingFromTokensMap()
//    {
//        HashMap<String, Parameter> tokens = new HashMap<String, Parameter>(); 
//        tokens.put("token1", new Parameter("token1", "", "Seattle")); 
//        tokens.put("token2", new Parameter("token2", "", "WA")); 
//        tokens.put("var_int", new Parameter("var_int", "", "111"));
//        
//        CoreRule rule = getRuleSample();
//        rule.seedDataPoints(tokens);
//        
//        // get int type value: 
//        int int_result = (int)rule.getFactoid().getFact("var_int").value(); 
//        String city_result = rule.getFactoid().getFact("fetch1").getRequest().getParameter("par_key1").getValue();
//        String state_result = rule.getFactoid().getFact("fetch1").getRequest().getParameter("par_key2").getValue();
//        
//        Assert.assertTrue(int_result == 111 && city_result.equals("Seattle") && state_result.equals("WA") );
//    }
    
    @Test
    public void testSeedingFromTokensMap()
    {
        HashMap<String, String> tokens = new HashMap<String, String>(); 
        tokens.put("token1", "Seattle"); 
        tokens.put("token2", "WA"); 
        tokens.put("var_int", "111");
        
        CoreRule rule = getRuleSample();
        
        try
        {
            rule.seedDataPoints(tokens);
        } 
        catch (RiskEngineApplicationException dfe)
        {
            Assert.fail("Rule.seedDataPoints() method exception: " + dfe);
        }
        
        // get int type value: 
        int int_result = (int)rule.getFactoid().getFact("var_int").value(); 
        String city_result = rule.getFactoid().getFact("fetch1").getRequest().getParameter("par_key1").getValue();
        String state_result = rule.getFactoid().getFact("fetch1").getRequest().getParameter("par_key2").getValue();
        
        Assert.assertTrue(int_result == 111 && city_result.equals("Seattle") && state_result.equals("WA") );
    }
    
    private CoreRule getRuleSample()
    {
        CoreRule rule = new CoreRule("rule_name", new Procedure("proc_name", "rule_script", EngineType.JAVASCRIPT)); 
        // fill factoid:
        try
        {
            rule.addFact("var_int", "11", DataType.INT);
            rule.addFact("var_string", "abcde", DataType.STRING);
        }
        catch (DataFormatException dfe)
        {
            Assert.fail("Rule.addFact() method exception: " + dfe);
        }
        
        Fact fetch1 = null;
        
        try
        {
            fetch1 = new Fact("fetch1", "fetch_value", DataType.FETCHABLE); 
        } 
        catch (RiskEngineApplicationException dfe)
        {
            Assert.fail("Fact coonstructor exception: " + dfe);
        }
        
        Request request1 = new Request(RequestType.GET, "body_req1"); 
        request1.addParameter("token1", "par_key1", "city");
        request1.addParameter("token2", "par_key2", "state");
//        
        fetch1.setRequest(request1); 
        rule.addFact(fetch1);
        return rule; 
    }
    
    @Test
    public void testHeaderSeeding()
    {
        Header h = new Header();         
        //addKeyValue("abc", "xyz");
        h.addParameter("abc", "key1", "xyz");  
        
//        h.addKeyValue("abc", "xxx");  
//        String value = h.get("abc");
//        value = h.getValue("aaa"); 
        
        CoreRule rule = getRuleSample();
        // add / set header: 
        Fact fe = rule.getFactoid().getFact("fetch1"); 
//        fe.getRequest().setHeader(h);
        fe.getRequest().addHeader(h);
        
        // set tokens: 
         HashMap<String, String> tokens = new HashMap<String, String>(); 
        tokens.put("token1", "Seattle"); 
        tokens.put("token2", "WA"); 
        tokens.put("var_int", "111");
        tokens.put("abc", "123"); 
        
         try
        {
            rule.seedDataPoints(tokens);
        } 
        catch (RiskEngineApplicationException dfe)
        {
            Assert.fail("Rule.seedDataPoints() method exception: " + dfe);
        }
        
        // get int type value: 
        int int_result = (int)rule.getFactoid().getFact("var_int").value(); 
        String city_result = rule.getFactoid().getFact("fetch1").getRequest().getParameter("par_key1").getValue();
        String state_result = rule.getFactoid().getFact("fetch1").getRequest().getParameter("par_key2").getValue();
        
//        Header hdr = rule.getFactoid().getFact("fetch1").getRequest().getHeader();
        Header hdr = rule.getFactoid().getFact("fetch1").getRequest().getHeaders().get(0);
        String header_Value = hdr.getParameter("key1").getValue();
        
        Assert.assertTrue(int_result == 111 && 
                city_result.equals("Seattle") && 
                state_result.equals("WA") && 
                header_Value.equals("123"));        
    }
}
