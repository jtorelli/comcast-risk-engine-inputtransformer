if (typeof Debug !== 'object') {
    Debug = {};
}

(function() {
    'use strict';

    Debug.printContext = function(obj) {
        var variables = {}; // We need a map for our variables to store the key value pair
        var functions = []; // We only need the names for our functions

        for (var name in obj) {
            var type = _getType(obj[name]);
            switch (type)
            {
                case "Function" :
                    functions.push(name);
                    break;
                default :
                    variables[name] = [type, obj[name]];
                    break;
            }
        }

        variables = _sortArrayByKeys(variables);
        functions.sort();

        println("");
        println(" ==== Objects ==== ");
        for (var name in variables) {
            println(name + " = (" + variables[name][0] + ") " + variables[name][1]);
        }

        println("");
        println(" ==== Methods ==== ");
        for (var index in functions) {
            println(functions[index]);
        }

        println("");
        println("=========================")
    }

    function _getType(thing) {
        if (thing===null) return "Null"; //Special case
        return Object.prototype.toString.call(thing).match(/^\[object (.*)\]$/)[1];
    }

    function _sortArrayByKeys(inputArray) {
        var arrayKeys=[];
        for(var k in inputArray) {arrayKeys.push(k);}
        arrayKeys.sort();

        var outputArray=[];
        for(var i=0; i<arrayKeys.length; i++) {
            outputArray[arrayKeys[i]]=inputArray[arrayKeys[i]];
        }
        return outputArray;
    }

    Debug.printStackTrace = function() {
        var callStack = [];

        try {
            $nosuchvariable$/0;                 //This will throw an exception
        } catch(e) {
            var lines = e.stack.split("\n");    //Split the callstack by individual lines
            for (var i = 0, len = lines.length; i < len; i++) {
                callStack.push(lines[i]);       //And add them to an array
            }
            callStack.shift();                  //Now we can remove the Debug.printStackTrace method call
            _printStackTraceOutput(callStack);  //And output
        }
    }

    function _printStackTraceOutput(arr) {
        println("");
        println("Received Debug.printStackTrace() call");
        println(arr.join("\n"));
        println("=========================")
    }

}());
