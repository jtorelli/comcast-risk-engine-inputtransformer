if (typeof JsLogger !== 'object') {
    JsLogger = {};
}

(function() {
    var JsLoggerLib = new JavaImporter(org.slf4j);

    with (JsLoggerLib) {
        JsLogger.getLogger = function(name) {
            return LoggerFactory.getLogger(name);
        }
    }
}());
