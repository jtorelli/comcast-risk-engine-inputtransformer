
package RiskAssessmentCore.DroolsEngine;

public class Predicate 
{
    private String name; 
    private PredicateSign predicatesign; 
    private String value; 
    private String key;     
    
    public Predicate(String name, PredicateSign predicatesign, String value)
    {
        this.name = name; 
        this.predicatesign = predicatesign; 
        this.value = value; 
        this.key = this.name + "_" + this.predicatesign.name() + "_" + this.value; 
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PredicateSign getPredicatesign() {
        return predicatesign;
    }

    public void setPredicatesign(PredicateSign predicatesign) {
        this.predicatesign = predicatesign;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }   
    
    public String getKey() {
        return key;
    }
}
