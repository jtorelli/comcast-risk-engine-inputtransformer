
package RiskAssessmentCore.DroolsEngine;

public enum TokenType 
{
    NONE,
    LEFT, 
    RIGHT,
    PLUS, 
    MINUS, 
    MULTIPLY, 
    DEVIDE, 
    NUMERIC,
    STRING, 
    BOOLEAN
}
