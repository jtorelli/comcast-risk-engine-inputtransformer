
package RiskAssessmentCore.DroolsEngine;

import com.intersourcellc.DataType;

import java.io.Serializable;

public class Attribute implements Serializable
{
    private String name;     
    private Object value; 
    private DataType type;
    
    public Attribute(String name, Object value, DataType type)
    {
        this.name = name; 
        this.value = value; 
        this.type = type; 
    }
    
    public String getName() {
        return name;
    }

    public Object getValue() 
    {
        Object result = null; 
        switch (this.type)
        {
            case INT:  
                result = (int)this.value; 
                break; 
            case DOUBLE: 
                result = (double)this.value; 
                break; 
            case BOOLEAN:
                result = (Boolean)this.value; 
                break;
            case STRING: 
                result = (String)this.value; 
                break;
            case NONE: 
            default: 
                break; 
        }
        return result;
    }

    public DataType getType() {
        return this.type;
    }
    
    public int getInt()
    {
        if (this.type == DataType.INT)
        {
            return (int)this.value;            
        }
        else
        {
            throw new NumberFormatException(this.type.toString() + " to Integer conversion."); 
        }
    }
    
    public double getDouble()
    {
        if (this.type == DataType.DOUBLE)
        {
            return (double)this.value;            
        }
        else
        {
            throw new NumberFormatException(this.type.toString() + " to Double conversion."); 
        }
    }
    
    public Boolean getBoolean()
    {
        if (this.type == DataType.BOOLEAN)
        {
            return (Boolean)this.value;            
        }
        else
        {
            throw new NumberFormatException(this.type.toString() + " to Boolean conversion."); 
        }
    }
    
    public String getString() throws Exception
    {
        if (this.type == DataType.STRING)
        {
            return this.value.toString();            
        }
        else
        {
            throw new Exception(this.type.toString() + " to String conversion."); 
        }
    }
}

