
package RiskAssessmentCore.DroolsEngine;

import RiskAssessmentCore.DroolsEngine.Predicate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Conjunction 
{
    private HashMap<String, Predicate> predicates;
    
    public Conjunction()
    {
        this.predicates = new HashMap<>();
    }
    
    public void AddPredicate(Predicate predicate)
    {
        if (!this.predicates.containsKey(predicate.getKey()))
        {
            this.predicates.put(predicate.getKey(), predicate); 
        }
    }
    
    public Predicate GetPredicate(String key)
    {
        Predicate term = null; 
        
        if (this.predicates.containsKey(key))
        {
            term = (Predicate)this.predicates.get(key); 
        }
        
        return term;
    }
    
    public List<Predicate> GetPredicates()
    {
        //return this.predicates.
        return new ArrayList<Predicate>(this.predicates.values());
    }
    
    public String toString()
    {
        StringBuilder sb = new StringBuilder(); 
        for (Predicate pred : this.predicates.values())
        {
            sb.append(pred.getName() + " " + pred.getPredicatesign() + " " + pred.getValue() + "\n"); 
        }
        return sb.toString(); 
    }
}
