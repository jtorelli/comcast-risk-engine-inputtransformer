
package RiskAssessmentCore.DroolsEngine;

public enum PredicateSign 
{
    LESS("<"),
    LESS_OR_EQUAL("<="), 
    EQUAL("="), 
    MORE_OR_EQUAL(">="), 
    MORE(">"); 
    
    private final String sign; 
    
    PredicateSign(String sign)
    {
        this.sign = sign; 
    }
    
    public String Sign()
    {
        return this.sign; 
    }
    
//    @Override
//    public String toString()
//    {
//        return this.sign;
//    }
}
