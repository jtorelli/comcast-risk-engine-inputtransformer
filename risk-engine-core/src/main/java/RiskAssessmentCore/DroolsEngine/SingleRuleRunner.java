
package RiskAssessmentCore.DroolsEngine;

import com.intersourcellc.DataType;
import com.intersourcellc.Fact;
import com.intersourcellc.Factoid;
import com.intersourcellc.exception.RiskEngineApplicationException;
import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.Resource;
import org.drools.io.ResourceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.zip.DataFormatException;

public class SingleRuleRunner {
    final static Logger log = LoggerFactory.getLogger(SingleRuleRunner.class);

    private Factoid factoid;
    private Conjunction conjunction;
    private String ruleDRLtext;
    private String ruleName;
    private boolean dataConsistent;

    public boolean isDataConsistent() {
        return dataConsistent;
    }

    private String validationMessage;

    public String getValidationMessage() {
        return validationMessage;
    }

    public SingleRuleRunner(String ruleName, String inputFactoid, String inputRule) {
        this.ruleName = ruleName;
        this.factoid = GetFactoidFromStringInput(inputFactoid);
        this.conjunction = GetLogicFromStringInput(inputRule);
        this.ruleDRLtext = GenerateDRL(ruleName, this.conjunction);
        // validate input consistency: 
        ValidateInput();
    }

    public SingleRuleRunner(String ruleName, String inputFactoid, String inputRule, boolean getElsePart) {
        this.ruleName = ruleName;
        this.factoid = GetFactoidFromStringInput(inputFactoid);
        this.conjunction = GetLogicFromStringInput(inputRule);
        this.ruleDRLtext = GenerateDRL(ruleName, this.conjunction, getElsePart);
        // validate input consistency: 
        ValidateInput();
    }

    private Factoid GetFactoidFromStringInput(String input) {
        ArrayList<String> lines = GetLines(input);
        String regexp = "[ \\s,;=\\n\\t]+";

        String[] tokens;
        Factoid ftd = new Factoid();
        Fact fct;

        for (String line : lines) {
            tokens = line.split(regexp);

            if (tokens.length == 3) {
                DataType dt = getEnumFromString(DataType.class, tokens[0]);
                if (dt != null) 
                {
                    
                    // check types: 
                    try 
                    {
                        fct = new Fact(tokens[1], tokens[2], dt);
                        if (fct.value() != null) {
                            ftd.addFact(fct);
                        }
                    } catch (RiskEngineApplicationException ex) {
                    }
                }
            }
        }
        return ftd;
    }

    public static <T extends Enum<T>> T getEnumFromString(Class<T> c, String string) {
        if (c != null && string != null) {
            try {
                return Enum.valueOf(c, string.trim().toUpperCase());
            } catch (IllegalArgumentException ex) {
            }
        }
        return null;
    }

    private static ArrayList<String> GetLines(String input) {
        ArrayList<String> lines = new ArrayList();
        String regexp = "[\\r\\n;]+"; //"[;\\n]+\\s+";
        String[] output = input.trim().split(regexp);

        if (output.length > 0) {
            // translate lines: 
            for (int i = 0; i < output.length; i++) {
                if (!output[i].trim().equals("")) {
                    lines.add(output[i].trim());
                }
            }
        }
        return lines;
    }

    private Conjunction GetLogicFromStringInput(String input) {
        ArrayList<String> lines = GetLines(input);
        ArrayList tokens;
        Predicate term;
        Conjunction conjunction = new Conjunction();

        for (String line : lines) {
            tokens = GetTermTokens(line);
            String sign;
            if (tokens.size() == 3) {
                sign = tokens.get(1).toString();
                for (PredicateSign ps : PredicateSign.values()) {
                    if (sign.equals(ps.Sign())) {
                        term = new Predicate(tokens.get(0).toString(), ps, tokens.get(2).toString());
                        conjunction.AddPredicate(term);
                        break;
                    }
                }
            }
        }
        return conjunction;
    }

    private ArrayList<String> GetTermTokens(String line) {
        ArrayList<String> tokens = new ArrayList<>();
        //PredicatSign
        ArrayList<PredicateSign> predicate_signs = new ArrayList<>(Arrays.asList(PredicateSign.values()));

        String line_trimmed = line.trim();
        String sign;
        int position = 0;
        int position_found = 0;
        int sign_length = 0;
        int sign_length_max = 0;

        for (PredicateSign ps : predicate_signs) {
            sign = ps.Sign();
            position = line_trimmed.indexOf(sign, 0);
            sign_length = sign.length();

            if (position > 0 && sign_length > sign_length_max) {
                sign_length_max = sign_length;
                position_found = position;
            }
        }

        if (position_found > 0 && sign_length_max > 0) {
            tokens.add(line_trimmed.substring(0, position_found).trim());
            tokens.add(line_trimmed.substring(position_found, position_found + sign_length_max));
            tokens.add(line_trimmed.substring(position_found + sign_length_max).trim());

        }

        return tokens;
    }

    //    // can use like this:
//    String template = "Hello {0} Please find attached {1} which is due on {2}";
//    String[] values = {"John Doe", "invoice #123", "2009-06-30"};
//    log.debug(MessageFormat.format(template, values));
    private String GenerateDRL(String ruleName, Conjunction conjunction) {
        // frame: 
        StringBuilder sb = new StringBuilder();
        sb.append("import com.intersourcellc.Factoid \n");
        sb.append("dialect \"mvel\" \n");
        sb.append("global com.intersourcellc.FeedBack feedback; \n");
        sb.append("rule ");
        sb.append("\"");
        sb.append(ruleName);
        sb.append("\"\n");
        sb.append("when \n");
        sb.append("\t$ff :\n");

        for (Predicate pre : conjunction.GetPredicates()) {
            sb.append("\tFactoid(getFact(\"");
            sb.append(pre.getName());
            sb.append("\"");
            sb.append(").value() ");
            sb.append(pre.getPredicatesign().Sign().equals("=") ? "==" : pre.getPredicatesign().Sign());
            sb.append(" \"");
            sb.append(pre.getValue());
            sb.append("\")\n");
        }
        sb.append("then \n");

        // trace template: 
//        feedback.addTrace($ff.getFact("f_boolean").value().toString());
        for (Predicate pre : conjunction.GetPredicates()) {
            sb.append("\tfeedback.addTrace(\"");
            sb.append(pre.getName());
            sb.append(" ");
            sb.append(pre.getPredicatesign().Sign());
            sb.append(" ");
            sb.append(pre.getValue());
            sb.append("\")\n");
        }
        // result(s) template: feedback.setBooleanResult(true); 
        sb.append("\tfeedback.setBooleanResult(true);\n ");
        sb.append("end \n");

        return sb.toString();
    }

    private String GenerateDRL(String ruleName, Conjunction conjunction, boolean getElsePart) {
        StringBuilder sb = new StringBuilder();
        sb.append("import com.intersourcellc.Factoid \n");
        sb.append("dialect \"mvel\" \n");
        sb.append("global com.intersourcellc.FeedBack feedback; \n");

        sb.append("rule ");
        sb.append("\"");
        sb.append(ruleName);
        sb.append("\"\n");
        sb.append("no-loop\n");
        sb.append("when \n");
        sb.append("\t$ff :\n");

        sb.append("\tFactoid(");
        for (Predicate pre : conjunction.GetPredicates()) {
            sb.append("(getFact(\"");
            sb.append(pre.getName());
            sb.append("\"");
            sb.append(").value() ");
            sb.append(pre.getPredicatesign().Sign().equals("=") ? "==" : pre.getPredicatesign().Sign());
            sb.append(" \"");
            sb.append(pre.getValue());
            sb.append("\") ");
            sb.append("&&");
            sb.append(" ");
        }
        sb.delete(sb.length() - 3, sb.length());
        sb.append(")\n");
        sb.append("then \n");

        // trace template: 
        for (Predicate pre : conjunction.GetPredicates()) {
            sb.append("\tfeedback.addTrace(\"");
            sb.append(pre.getName());
            sb.append(" ");
            sb.append(pre.getPredicatesign().Sign());
            sb.append(" ");
            sb.append(pre.getValue());
            sb.append("\")\n");
        }
        // result(s) template: feedback.setBooleanResult(true); 
        sb.append("\tfeedback.setBooleanResult(true);\n ");
        sb.append("end \n");

        // negation part (alternative rule)
        if (getElsePart) {
            // negation-specific: 
            sb.append("\n");
            sb.append("rule ");
            sb.append("\"");
            // alternative rule name: 
            sb.append("NOT_");
            sb.append(ruleName);
            sb.append("\"\n");
            sb.append("no-loop\n");
            sb.append("when \n");
            sb.append("\t$ff :\n");
            sb.append("\tFactoid(");
            for (Predicate pre : conjunction.GetPredicates()) {
                sb.append("!(getFact(\"");
                sb.append(pre.getName());
                sb.append("\"");
                sb.append(").value() ");
                sb.append(pre.getPredicatesign().Sign().equals("=") ? "==" : pre.getPredicatesign().Sign());
                sb.append(" \"");
                sb.append(pre.getValue());
                sb.append("\") ");
                sb.append("||");
                sb.append(" ");
            }
            //String sbContents = sb.toString(); 
            sb.delete(sb.length() - 4, sb.length());
            sb.append(")\n");
            sb.append("then \n");

            // trace template: 
            sb.append("\tfeedback.addTrace(\"");
            sb.append("NOT_");
            sb.append(ruleName);
            sb.append(": rule ELSE condition fired.");
            sb.append("\")\n");

            // result(s) template: feedback.setBooleanResult(true); 
            sb.append("\tfeedback.setBooleanResult(true);\n ");
            sb.append("end \n");
        }

        return sb.toString();
    }

    private void ValidateInput() {
        StringBuilder sb = new StringBuilder();
        int count = 0;
        Set names = this.factoid.getFacts().keySet();
        List<Predicate> predicates = this.conjunction.GetPredicates();
        for (Predicate pr : predicates) {
            if (!names.contains(pr.getName())) {
                count += 1;
                sb.append(pr.getName());
                sb.append(" ");
            }
        }

        if (count > 0) {
            this.validationMessage = "Variable(s) not found in input: " + sb.toString();
        } else {
            this.dataConsistent = true;
        }
    }

    private KnowledgeBase CreateKnowledgeBase(String rulesText) {
        KnowledgeBuilder kBuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();

        Resource myResource = ResourceFactory.newReaderResource((Reader) new StringReader(rulesText));
        try {
            kBuilder.add(myResource, ResourceType.DRL);
        } catch (Exception ex) {
            this.validationMessage = ex.getMessage();
            return null;
        }
        // Create the Knowledge Base:
        KnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();
        // Transfer compiled Knowledge Packages:
        kbase.addKnowledgePackages(kBuilder.getKnowledgePackages());
        return kbase;
    }

    public String getRuleDRLtext() {
        return ruleDRLtext;
    }
}
