
package RiskAssessmentCore;

import com.intersourcellc.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.script.*;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RuleEngineJS implements IRuleEngine
{
    final static Logger log = LoggerFactory.getLogger(RuleEngineJS.class);
    
    private CoreRule rule;
    private ScriptEngineManager scriptEngineManager;
    private ScriptEngine engine;
    private ScriptContext scriptContext;
    private Bindings globalScope;
    private Bindings engineScope;

    public RuleEngineJS(CoreRule ruleObject)
    {
        this.rule = ruleObject;

        scriptEngineManager = new ScriptEngineManager();

        List<ScriptEngineFactory> engines = scriptEngineManager.getEngineFactories();
        if (engines.isEmpty()) {
            log.debug("No scripting engines were found");
            return;
        }
        int size = engines.size();
        switch (size) {
            case 0 :
                log.debug("No scripting engines were found, please check library configurations");
                break;
            case 1 :
                log.debug("The following scripting engine was found");
                break;
            default:
                log.debug("The following " + size + " scripting engines were found");
                break;
        }
        log.debug("");
        for (ScriptEngineFactory engine : engines) {
            log.debug("Engine name: " + engine.getEngineName());
            log.debug("\tVersion: " + engine.getEngineVersion());
            log.debug("\tLanguage: " + engine.getLanguageName());
            List<String> extensions = engine.getExtensions();
            if (extensions.size() > 0) {
                log.debug("\tEngine supports the following extensions:");
                for (String e : extensions) {
                    log.debug("\t\t" + e);
                }
            }
            List<String> shortNames = engine.getNames();
            if (shortNames.size() > 0) {
                log.debug("\tEngine has the following short names:");
                for (String n : engine.getNames()) {
                    log.debug("\t\t" + n);
                }
            }
            log.debug("=========================");
        }

        // This is RuleEngineJS, so instantiate that engine
        engine = scriptEngineManager.getEngineByName("ECMAScript");

        scriptContext = engine.getContext();
        globalScope = scriptContext.getBindings(ScriptContext.GLOBAL_SCOPE);
        engineScope = scriptContext.getBindings(ScriptContext.ENGINE_SCOPE);

        //Load our debug and logger libraries
        loadJsLibrary(new NamedResource("RiskAssessmentCore/debug.js"), globalScope);
        loadJsLibrary(new NamedResource("RiskAssessmentCore/logger.js"), globalScope);
        loadJsLibrary(new NamedResource("RiskAssessmentCore/windowTimers.js"), engineScope);
    }

    private void loadJsLibrary(NamedResource library, Bindings scope) {
        try {
            engine.put(ScriptEngine.FILENAME, library.getName()); //Need to provide the JS filename before we actually load it so that the callstack works
            engine.eval(new InputStreamReader(library.getInputStream()), scope); //Load debug and tracing functions
        } catch (ScriptException e) {
            throw new RuntimeException("Couldn't load library: " + library.getName(), e);
        }
    }

    public Factoid Run()
    {
        Bindings bindings = engine.createBindings();
        Factoid factoid = rule.getFactoid();

        //Unbox the values and transform them into something the ScriptEngine can use
//        for (Map.Entry<String, Fact> entry : factoid.getFacts().entrySet()) {
//            bindings.put(entry.getValue().getName(), entry.getValue().value());
//        }
        for (Fact entry : factoid.getFacts().values()) 
        {
            bindings.put(entry.getName(), entry.value());
        }
        
        //Bind feedback and factoid to the ScriptEngine
        bindings.put("factoid", factoid);

        //Bind datatypes FIXME: We probably should have a structure not something flat. TBD.
        for (DataType dtp : DataType.values()) {
            bindings.put("DataType" + dtp.name(), dtp);
        }

        engine.put(ScriptEngine.FILENAME, rule.getName()); //Need to provide the JS filename before we actually load it so that the callstack works
        bindings.put("ruleName", rule.getName()); //Because we can't access javax.script.ScriptEngine.FILENAME from execution context, we need to provide this variable too

        try {
            engine.eval(rule.getProcedureContent(), bindings);
        } catch (ScriptException e) {
            factoid.addTrace(e.getMessage());
            throw new RuntimeException(e);
        }

        return factoid;
    }
    
    public EngineType getEngineType()
    {
        return this.rule.getEngineType();
    }

    private class NamedResource implements INamedAsset {
        private final String name;
        private final InputStream inputStream;

        public NamedResource(String resource) {
            this.name = new File(resource).getName();
            this.inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(resource);

            if (this.inputStream == null) {
                log.warn("Resource stream for {} was null", resource);
                if (log.isDebugEnabled()) {
                    log.debug("Check classpath:");

                    ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
                    URL[] urls = ((URLClassLoader)classLoader).getURLs();

                    for (URL url: urls) {
                        log.debug(url.getPath());
                    }
                }

                throw new RuntimeException("Unable to find NamedResource: " + resource);
            }
        }

        public InputStream getInputStream() {
            return inputStream;
        }

        @Override
        public String getName() {
            return name;
        }
    }
}
