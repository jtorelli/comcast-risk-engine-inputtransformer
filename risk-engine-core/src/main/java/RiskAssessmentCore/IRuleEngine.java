package RiskAssessmentCore;

import com.intersourcellc.EngineType;
import com.intersourcellc.Factoid;


public interface IRuleEngine
{
    EngineType getEngineType();
    Factoid Run();
}
