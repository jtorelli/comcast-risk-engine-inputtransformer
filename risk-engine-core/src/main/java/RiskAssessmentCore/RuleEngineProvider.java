package RiskAssessmentCore;

import com.intersourcellc.CoreRule;

public class RuleEngineProvider
{
    private CoreRule rule;
    
    public RuleEngineProvider(CoreRule rule)
    {
        this.rule = rule; 
    }
    
    public IRuleEngine getEngine()
    {
        IRuleEngine engine = null;
        switch (this.rule.getEngineType())
        {
            case NONE: 
                engine = null;  // or exception???               
                break; 
            case DROOLS: 
                engine = new RuleEngineDrools(this.rule);
                break;
            case JAVASCRIPT: 
                engine = new RuleEngineJS(this.rule);                 
                break; 
        }
        
        return engine; 
    }
}
