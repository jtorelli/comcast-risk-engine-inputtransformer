
package RiskAssessmentCore;

import com.intersourcellc.CoreRule;
import com.intersourcellc.EngineType;
import com.intersourcellc.Factoid;
import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.Resource;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatelessKnowledgeSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Reader;
import java.io.StringReader;

public class RuleEngineDrools implements IRuleEngine
{
    final static Logger log = LoggerFactory.getLogger(RuleEngineDrools.class);

    private CoreRule rule;
    
    public RuleEngineDrools(CoreRule ruleObject)
    {
        this.rule = ruleObject; 
    }
    
    @Override
    public Factoid Run()
    {
        Factoid factoid = null; 
        String script; 
        KnowledgeBase knowledgeBase;
        StatelessKnowledgeSession session; 
        
        // process factoid: 
        if (this.rule.getFactoid().getFacts().size() > 0)
        {
            script = this.rule.getProcedureContent();
            
            if (script.trim().length() > 0)
            {
                factoid = rule.getFactoid();
                knowledgeBase = CreateKnowledgeBase(this.rule.getProcedureContent());
                session = knowledgeBase.newStatelessKnowledgeSession();  
                session.execute(factoid);
            }
        } 
        
        return factoid;
    }
    
    @Override
    public EngineType getEngineType()
    {
        return this.rule.getEngineType();
    }
    
    private KnowledgeBase CreateKnowledgeBase(String rulesText) 
    {
        KnowledgeBuilder kBuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
        Resource myResource = ResourceFactory.newReaderResource((Reader) new StringReader(rulesText));
        
        try 
        {
            kBuilder.add(myResource, ResourceType.DRL);
        } 
        catch (Exception ex) 
        {
            log.error("Error adding resource to KnowledgeBuilder", ex);
            return null;
        }
        // Create the Knowledge Base:
        KnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();
        // Transfer compiled Knowledge Packages:
        kbase.addKnowledgePackages(kBuilder.getKnowledgePackages());
        return kbase;
    }
}
