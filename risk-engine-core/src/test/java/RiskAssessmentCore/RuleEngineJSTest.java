package RiskAssessmentCore;

import com.intersourcellc.*;
import com.intersourcellc.exception.RiskEngineApplicationException;
import java.util.zip.DataFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Ryan on 6/20/13.
 */
public class RuleEngineJSTest {
    final static Logger log = LoggerFactory.getLogger(RuleEngineJSTest.class);

    @Test
    public void Run_ValidFactoidWithJson_CanCorrectlyCreateJsonObject() {
        String jsonResponse ="{\n" +
"\"employees\": \n" +
"[\n" +
"{ \"firstName\":\"John\" , \"lastName\":\"Doe\" },\n" +
"{ \"firstName\":\"Anna\" , \"lastName\":\"Smith\" },\n" +
"{ \"firstName\":\"Peter\" , \"lastName\":\"Jones\" }\n" +
"]\n" +
"}"; 

        final String script =
                "var jsonEval = eval('(' + jsonResponse + ')');\n" +
                "\n" +
                "factoid.addFact('actual', jsonEval.employees[1].firstName, DataTypeSTRING);"; //Should select Anna from jsonResponse object

        String expected = "Anna";

        CoreRule rule = new CoreRule(Utility.methodName(), new Procedure("<script:" + Utility.methodName() + ">", script, EngineType.JAVASCRIPT));
        
        try
        {
            rule.addFact(new Fact("jsonResponse", jsonResponse, DataType.JSON));
        }
        catch (RiskEngineApplicationException dfe)
        {
//            Assert.fail("Rule.addFact() method exception: " + dfe);
        }
        
        //Fetcher goes here        
        RuleEngineProvider provider = new RuleEngineProvider(rule);
        IRuleEngine engine = provider.getEngine();

        Factoid result = engine.Run();
        String actual = (String) result.getFact("actual").value();

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void Run_JsonObject_ReturnedAsJsonInFactoidResult() {
        //JSON representation of a JavaScript object
        String json =
                "{\"bindings\": [\n" +
                "        {\"ircEvent\": \"PRIVMSG\", \"method\": \"newURI\", \"regex\": \"^http://.*\"},\n" +
                "        {\"ircEvent\": \"PRIVMSG\", \"method\": \"deleteURI\", \"regex\": \"^delete.*\"},\n" +
                "        {\"ircEvent\": \"PRIVMSG\", \"method\": \"randomURI\", \"regex\": \"^random.*\"}\n" +
                "    ]\n" +
                "}";

        //In-memory script to execute
        String script =
                "var json_parsed = JSON.parse(json); //Parse JSON object and create JS Object\n" +
                "json_stringified = JSON.stringify(json_parsed);\n" +
                "factoid.addFact('actual', json_stringified, DataTypeJSON);";

        String expected = //Same as above json, but line endings and extraneous whitespace has been stripped
                "{\"bindings\":[" +
                        "{\"ircEvent\":\"PRIVMSG\",\"method\":\"newURI\",\"regex\":\"^http://.*\"}," +
                        "{\"ircEvent\":\"PRIVMSG\",\"method\":\"deleteURI\",\"regex\":\"^delete.*\"}," +
                        "{\"ircEvent\":\"PRIVMSG\",\"method\":\"randomURI\",\"regex\":\"^random.*\"}" +
                    "]" +
                "}";

        CoreRule rule = new CoreRule(Utility.methodName(), new Procedure("<script:" + Utility.methodName() + ">", script, EngineType.JAVASCRIPT));

        //Add to the factoid so it is visible from the ScriptEngine
        try
        {
            rule.addFact("json", json, DataType.JSON);
        } 
        catch (DataFormatException dfe)
        {
            Assert.fail("Rule.addFact() method exception: " + dfe);
        }
        
        RuleEngineProvider provider = new RuleEngineProvider(rule);
        IRuleEngine engine = provider.getEngine();

        Factoid result = engine.Run();
        String actual = (String) result.getFact("actual").value();

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void Run_JavaBooleanTrue_MarshaledAsJavascriptTrue() {
        String script =
                "factoid.addFact('result', 'false', DataTypeBOOLEAN);\n" +
                "\n" +
                "if (expected) {\n" +
                "   factoid.setFact('result', 'true', DataTypeBOOLEAN);\n" +
                "};";

        boolean expected = true;

        CoreRule rule = new CoreRule(Utility.methodName(), new Procedure("<script:" + Utility.methodName() + ">", script, EngineType.JAVASCRIPT));

        try
        {
            rule.addFact("expected", String.valueOf(expected), DataType.BOOLEAN);
        }
        catch (DataFormatException dfe)
        {
            Assert.fail("Rule.addFact() method exception: " + dfe);
        }
        
        RuleEngineProvider provider = new RuleEngineProvider(rule);
        IRuleEngine engine = provider.getEngine();

        Factoid result = engine.Run();
        boolean actual = (boolean) result.getFact("result").value();

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void Run_JavaBooleanFalse_MarshaledAsJavaScriptFalse() {
        String script =
                "factoid.addFact('result', 'true', DataTypeBOOLEAN);\n" +
                "\n" +
                "if (!expected) {\n" +
                "   factoid.setFact('result', 'false', DataTypeBOOLEAN);\n" +
                "};";

        boolean expected = false;

        CoreRule rule = new CoreRule(Utility.methodName(), new Procedure("<script:" + Utility.methodName() + ">", script, EngineType.JAVASCRIPT));

        try
        {
            rule.addFact("expected", String.valueOf(expected), DataType.BOOLEAN);
        }
        catch (DataFormatException dfe)
        {
            Assert.fail("Rule.addFact() method exception: " + dfe);
        }
        
        RuleEngineProvider provider = new RuleEngineProvider(rule);
        IRuleEngine engine = provider.getEngine();

        Factoid result = engine.Run();
        boolean actual = (boolean) result.getFact("result").value();

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void Run_JavaOnePointOneGreaterThanOnePointZero_MarshaledAsJavascriptNumberGreaterThanOnePointZero() {
        String script =
                "factoid.addFact('result', 'false', DataTypeBOOLEAN);\n" +
                "\n" +
                "if (x > 1.0) {\n" +
                "   factoid.setFact('result', 'true', DataTypeBOOLEAN);\n" +
                "};" +
                "factoid.setFact('value', x, DataTypeDOUBLE);";

        Double x = 1.1;
        boolean expected = true;

        CoreRule rule = new CoreRule(Utility.methodName(), new Procedure("<script:" + Utility.methodName() + ">", script, EngineType.JAVASCRIPT));

        try
        {
            rule.addFact("x", String.valueOf(x), DataType.DOUBLE);
        }
        catch (DataFormatException dfe)
        {
            Assert.fail("Rule.addFact() method exception: " + dfe);
        }
        
        RuleEngineProvider provider = new RuleEngineProvider(rule);
        IRuleEngine engine = provider.getEngine();

        Factoid result = engine.Run();

        Fact actual;
        Fact value;
        Assert.assertNotNull(actual = result.getFact("result"), "Unable to retrieve result");
        Assert.assertNotNull(value = result.getFact("value"), "Unable to retrieve value");

        Assert.assertEquals(actual.value(), expected, "Result indicates that 1.1 !> 1.0");
        Assert.assertEquals(value.value(), 1.1, "value was not interpreted as the correct value");
    }

    @Test
    public void Run_JavaZeroPointNineLessThanOnePointZero_MarshaledAsJavascriptNumberLessThanOnePointZero() {
        String script =
                "factoid.addFact('result', 'false', DataTypeBOOLEAN);\n" +
                "\n" +
                "if (x < 1.0) {\n" +
                "   factoid.setFact('result', 'true', DataTypeBOOLEAN);\n" +
                "};" +
                "factoid.setFact('value', x, DataTypeDOUBLE);";

        Double x = 0.9;
        boolean expected = true;

        CoreRule rule = new CoreRule(Utility.methodName(), new Procedure("<script:" + Utility.methodName() + ">", script, EngineType.JAVASCRIPT));

        try
        {
            rule.addFact("x", String.valueOf(x), DataType.DOUBLE);
        } 
        catch (DataFormatException dfe)
        {
            Assert.fail("Rule.addFact() method exception: " + dfe);
        }
        
        RuleEngineProvider provider = new RuleEngineProvider(rule);
        IRuleEngine engine = provider.getEngine();

        Factoid result = engine.Run();

        Fact actual;
        Fact value;
        Assert.assertNotNull(actual = result.getFact("result"), "Unable to retrieve result");
        Assert.assertNotNull(value = result.getFact("value"), "Unable to retrieve value");

        Assert.assertEquals(actual.value(), expected, "Result indicates that 0.9 !< 1.0");
        Assert.assertEquals(value.value(), 0.9, "value was not interpreted as the correct value");
    }

    @Test
    public void Run_LoadLoggerJavascriptLibrary_LoggerWritesToJavaLogSystem() {
        String script =
                "var log = JsLogger.getLogger(ruleName);\n" + //Set logger to script context
                "\n" +
                "log.trace('Trace level log message');\n" + //Probably swallowed, but a valid test nonetheless
                "log.debug('Debug level log message');\n" +
                "log.info('Info level log message');\n" +
                "log.warn('Warn level log message');\n" +
                "log.error('Error level log message');\n" +
                "\n" +
                "Debug.printContext(this);"; //We can see that this creates two classes in the global context, Log and LoggerFactory

        CoreRule rule = new CoreRule(Utility.methodName(), new Procedure("<script:" + Utility.methodName() + ">", script, EngineType.JAVASCRIPT));

        RuleEngineProvider provider = new RuleEngineProvider(rule);
        IRuleEngine engine = provider.getEngine();

        engine.Run();

        //Assume that if we don't throw a scripting error that logging works,
        // you can look at the logs to confirm. To test this correctly, you would
        // need to create an SLF4J compatible logger, which seems more trouble than it's worth.
    }

    @Test
    public void testGetEngineType() throws Exception {
        EngineType expected = EngineType.JAVASCRIPT;
        CoreRule rule = new CoreRule("testGetEngineTypeRule", new Procedure("ProcedureName", "", expected));

        EngineType actual = rule.getEngineType();
        Assert.assertEquals(actual, expected);
    }
}
