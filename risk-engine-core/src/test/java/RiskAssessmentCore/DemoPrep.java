package RiskAssessmentCore;

import com.intersourcellc.*;
import com.intersourcellc.exception.RiskEngineApplicationException;
import java.util.HashMap;
import java.util.List;
import java.util.zip.DataFormatException;

import junit.framework.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

/**
 * Created by Ryan on 6/24/13.
 */
public class DemoPrep {
    final static Logger log = LoggerFactory.getLogger(DemoPrep.class);

    @Test(enabled = false)
    public void demoPrep() {
        final String seattleWeatherJson =
                "{\"coord\":{\"lon\":-122.332069,\"lat\":47.606209},\"sys\":{\"country\":\"US\",\"sunrise\":1372162384,\"sunset\":1372219878},\"weather\":[{\"id\":500,\"main\":\"Rain\",\"description\":\"light rain\",\"icon\":\"10d\"}],\"base\":\"global stations\",\"main\":{\"temp\":295.24,\"humidity\":54,\"pressure\":1005,\"temp_min\":292.15,\"temp_max\":297.59},\"wind\":{\"speed\":3.6,\"gust\":6.17,\"deg\":172},\"rain\":{\"1h\":0.51},\"clouds\":{\"all\":48},\"dt\":1372195840,\"id\":5809844,\"name\":\"Seattle\",\"cod\":200}";

        final String script =
                "var log = JsLogger.getLogger(ruleName);\n" +
                "\n" +
                "var seattleWeather = JSON.parse(seattleWeatherJson);\n" +
                "\n" +
                "Debug.printContext(this);\n" +
                "\n" +
                "log.info('City: ' + seattleWeather.name);\n" +
                "log.info('Temp: ' + kToC(seattleWeather.main.temp) + ' Deg C');\n" +
                "log.info('Temp: ' + cToF(kToC(seattleWeather.main.temp)) + ' Deg F');\n" +
                "\n" +
                "// Convert the temperature, but also throw the printStackTrace()\n" +
                "var currentTemp = kToF(seattleWeather.main.temp);\n" +
                "\n" +
                "if (currentTemp < 62) {\n" +
                "   log.info('Cold');\n" +
                "   factoid.addFact('temperature', 'Cold', DataTypeSTRING);\n" +
                "} else if (currentTemp >= 62 && currentTemp < 75) {\n" +
                "   log.info('Cool');\n" +
                "   factoid.addFact('temperature', 'Cool', DataTypeSTRING);\n" +
                "} else if (currentTemp >= 75 && currentTemp < 90) {\n" +
                "   log.info('Warm');\n" +
                "   factoid.addFact('temperature', 'Warm', DataTypeSTRING);\n" +
                "} else {\n" +
                "   log.info('Hot');\n" +
                "   factoid.addFact('temperature', 'Hot', DataTypeSTRING);\n" +
                "}\n" +
                "\n" +
                "function kToC(k, opt) {\n" +
                "   opt = opt || {};\n" +
                "   opt.params = opt.params || {};\n" +
                "   opt.debug = opt.debug || false;\n" +
                "\n" +
                "   if(opt.debug) Debug.printStackTrace();\n" +
                "   return k - 273.15;\n" +
                "};\n" +
                "\n" +
                "function cToF(c) {\n" +
                "   return 32 + (9.0/5.0) * c;\n" +
                "};\n" +
                "\n" +
                "function kToF(k) {\n" +
                "   return 32 + (9.0/5.0) * kToC(k, {debug:true});\n" +
                "};\n";

        CoreRule rule = new CoreRule(Utility.methodName(), new Procedure("<script:" + Utility.methodName() + ">", script, EngineType.JAVASCRIPT));

        //Fetcher goes here
        try
        {
            rule.addFact(new Fact("seattleWeatherJson", seattleWeatherJson, DataType.JSON));
        }
        catch (RiskEngineApplicationException dfe)
        {
            Assert.fail("Rule.addFact() method exception: " + dfe);
        }
        //Fetcher goes here

        RuleEngineProvider provider = new RuleEngineProvider(rule);
        IRuleEngine engine = provider.getEngine();

        Factoid result = engine.Run();

        log.info("Today in Seattle is: {}", result.get("temperature").value());
    }

    @Test(enabled = true)
    public void TestMemoryLeak1() {
        final String script =
                "var log = JsLogger.getLogger(ruleName);\n" +
                "\n" +
                "var timerLoop = makeWindowTimer(this, java.lang.Thread.sleep);\n" +
                "\n" +
                "var run = function () {\n" +
                "   var str = new Array(1000000).join('*');\n" +
                "   var doSomethingWithStr = function () {\n" +
                "       if (str === 'something')\n" +
                "           log.info(\"str was something\");\n" +
                "   };\n" +
                "   doSomethingWithStr();\n" +
                "};\n" +
                "\n" +
                "timerLoop.setInterval(run, 1000);";

        CoreRule rule = new CoreRule(Utility.methodName(), new Procedure("<script:" + Utility.methodName() + ">", script, EngineType.JAVASCRIPT));

        RuleEngineProvider provider = new RuleEngineProvider(rule);
        IRuleEngine engine = provider.getEngine();

        Factoid result = engine.Run();
    }    
        
    @Test
    public void testCompareTwoStringsEqual() {
        
        String script = "if (string1 == string2)\n" +
                        "{\n" +
                        "	factoid.setFact('result', 'true', DataTypeBOOLEAN);\n" +
                        "	factoid.addTrace('equal'); \n" +
                        "}; ";

        boolean expected = true;
        
        // rule preparation: 
        CoreRule rule = new CoreRule("compare_strings", new Procedure("string_comparizon", script, EngineType.JAVASCRIPT));
        // adding facts:
        try
        {
            rule.addFact("string1", "", DataType.STRING);
            rule.addFact("string2", "", DataType.STRING);
            // result fact: 
            rule.addFact("result", "false", DataType.BOOLEAN);
        }
        catch (DataFormatException dfe)
        {
            Assert.fail("Rule.addFact() method exception: " + dfe);
        }
        // seed parrameters: 
        HashMap<String, String> tokens = new HashMap(); 
        tokens.put("string1", "abc"); 
        tokens.put("string2", "abc");
        
        try
        {
            rule.seedDataPoints(tokens);
        }
        catch (RiskEngineApplicationException dfe)
        {
            Assert.fail("Rule.addFact() method exception: " + dfe);
        }
        
        RuleEngineProvider provider = new RuleEngineProvider(rule);
        IRuleEngine engine = provider.getEngine();

        Factoid result = engine.Run();
        List<String> trace = result.getTrace();
        
        // assert: 
        boolean actual = (boolean) result.getFact("result").value();
        Assert.assertEquals(actual, expected);
    }
    
    @Test
    public void testCompareTwoStringsNotEqual() {
        
        String script = "if (string1 == string2)\n" +
                        "{\n" +
                        "	factoid.setFact('result', 'true', DataTypeBOOLEAN);\n" +
                        "	factoid.addTrace('same'); \n" +
                        "}\n" +
                        "else\n" +
                        "{\n" +
                        "	factoid.setFact('result', 'false', DataTypeBOOLEAN);\n" +
                        "	factoid.addTrace('different'); \n" +
                        "}; ";

        boolean expected = false;
        
        // rule preparation: 
        CoreRule rule = new CoreRule("compare_strings", new Procedure("string_comparizon", script, EngineType.JAVASCRIPT));
        // adding facts:
        try
        {
            rule.addFact("string1", "", DataType.STRING);
            rule.addFact("string2", "", DataType.STRING);
            // result fact: 
            rule.addFact("result", "true", DataType.BOOLEAN);
        }
        catch (DataFormatException dfe)
        {
            Assert.fail("Rule.addFact() method exception: " + dfe);
        }
        
        // seed parrameters: 
        HashMap<String, String> tokens = new HashMap(); 
        tokens.put("string1", "abc"); 
        tokens.put("string2", "xyz");
        
        try
        {
            rule.seedDataPoints(tokens);
        }
        catch (RiskEngineApplicationException dfe)
        {
            Assert.fail("Rule.seedDataPoints() method exception: " + dfe);
        }
        
        RuleEngineProvider provider = new RuleEngineProvider(rule);
        IRuleEngine engine = provider.getEngine();

        Factoid result = engine.Run();
        List<String> trace = result.getTrace();
        
        // assert: 
        boolean actual = (boolean) result.getFact("result").value();
        Assert.assertEquals(actual, expected);
    }   
    
    @Test
    public void testLeveshteinDistanceAndSimilarity() {
        
        String script = "var distance = levenshtein(string1.toLowerCase(), string2.toLowerCase()); \n" +
"factoid.addTrace('distance = ' + distance.toString()); \n" +
"factoid.addFact('distance', distance.toString(), DataTypeINT);\n" +
"var similarity = 1.0 - distance / Math.max(string1.length, string2.length); \n" +
"factoid.addTrace('similarity = ' + similarity.toString()); \n" +
"factoid.addFact('similarity', similarity.toString(), DataTypeDOUBLE);\n" +
"\n" +
"function levenshtein (s1, s2) {\n" +
"  if (s1 == s2) {\n" +
"    return 0;\n" +
"  }\n" +
"\n" +
"  var s1_len = s1.length;\n" +
"  var s2_len = s2.length;\n" +
"  if (s1_len === 0) {\n" +
"    return s2_len;\n" +
"  }\n" +
"  if (s2_len === 0) {\n" +
"    return s1_len;\n" +
"  }\n" +
"\n" +
"  // BEGIN STATIC\n" +
"  var split = false;\n" +
"  try {\n" +
"    split = !('0')[0];\n" +
"  } catch (e) {\n" +
"    split = true; \n" +
"  }\n" +
"  // END STATIC\n" +
"  if (split) {\n" +
"    s1 = s1.split('');\n" +
"    s2 = s2.split('');\n" +
"  }\n" +
"\n" +
"  var v0 = new Array(s1_len + 1);\n" +
"  var v1 = new Array(s1_len + 1);\n" +
"\n" +
"  var s1_idx = 0,\n" +
"    s2_idx = 0,\n" +
"    cost = 0;\n" +
"  for (s1_idx = 0; s1_idx < s1_len + 1; s1_idx++) {\n" +
"    v0[s1_idx] = s1_idx;\n" +
"  }\n" +
"  var char_s1 = '',\n" +
"    char_s2 = '';\n" +
"  for (s2_idx = 1; s2_idx <= s2_len; s2_idx++) {\n" +
"    v1[0] = s2_idx;\n" +
"    char_s2 = s2[s2_idx - 1];\n" +
"\n" +
"    for (s1_idx = 0; s1_idx < s1_len; s1_idx++) {\n" +
"      char_s1 = s1[s1_idx];\n" +
"      cost = (char_s1 == char_s2) ? 0 : 1;\n" +
"      var m_min = v0[s1_idx + 1] + 1;\n" +
"      var b = v1[s1_idx] + 1;\n" +
"      var c = v0[s1_idx] + cost;\n" +
"      if (b < m_min) {\n" +
"        m_min = b;\n" +
"      }\n" +
"      if (c < m_min) {\n" +
"        m_min = c;\n" +
"      }\n" +
"      v1[s1_idx + 1] = m_min;\n" +
"    }\n" +
"    var v_tmp = v0;\n" +
"    v0 = v1;\n" +
"    v1 = v_tmp;\n" +
"  }\n" +
"  return v0[s1_len];\n" +
"}"; 

        // rule preparation: 
        CoreRule rule = new CoreRule("levenshtein", new Procedure("string_distance", script, EngineType.JAVASCRIPT));
        // adding facts:
        try
        {
            rule.addFact("string1", "", DataType.STRING);
            rule.addFact("string2", "", DataType.STRING);
        }
        catch (DataFormatException dfe)
        {
            Assert.fail("Rule.addFact() method exception: " + dfe);
        }
        // seed parrameters: 
        HashMap<String, String> tokens = new HashMap(); 
        tokens.put("string1", "NumberOfSomething"); 
        tokens.put("string2", "number_something");
        
//        tokens.put("string1", "HERE is the EXAMPLE"); 
//        tokens.put("string2", "IS IT an example");
        
        try
        {
            rule.seedDataPoints(tokens);
        }
        catch (RiskEngineApplicationException dfe)
        {
            Assert.fail("Rule.seedDataPoints() method exception: " + dfe);
        }
        RuleEngineProvider provider = new RuleEngineProvider(rule);
        IRuleEngine engine = provider.getEngine();

        Factoid result = engine.Run();
        List<String> trace = result.getTrace();
        
        // assert:
        boolean conclusion = Integer.parseInt(result.getFact("distance").value().toString()) >= 0; 
        conclusion = conclusion && Double.parseDouble(result.getFact("similarity").value().toString()) >= 0.0;
        
        Assert.assertTrue(conclusion); 
    }    
    @Test
    public void testJsonInputToTokenizableString() 
    {
        String script = "var arr = JSON.parse(input); \n" +
                        "var out  = ''; \n" +
                        "    for(var i=0;i<arr.length;i++)\n" +
                        "    {\n" +
                        "        var obj = arr[i];\n" +
                        "        for(var key in obj)\n" +
                        "        {\n" +
                        "            var attrName = key;\n" +
                        "            var attrValue = obj[key];\n" +
                        "            out = out + attrName + '=' + attrValue + '&'; \n" +
                        "        }\n" +
                        "    }\n" +
                        "out = out.substring(0, out.length - 1); \n" +
                        "factoid.addFact('output', out.toString(), DataTypeSTRING);"; 
        
        CoreRule rule = new CoreRule("jsontostring", new Procedure("json_to_string", script, EngineType.JAVASCRIPT));
        // adding facts:
        try
        {
            rule.addFact("input", "", DataType.JSON);
        }
        catch (DataFormatException dfe)
        {
            Assert.fail("Rule.addFact() method exception: " + dfe);
        }
        // seed parrameters: 
        HashMap<String, String> tokens = new HashMap(); 
        tokens.put("input", " [{\"name1\":\"value1\"}, {\"name2\":\"value2\"}, {\"name3\":\"value3\"}]"); 
         
        try
        {
            rule.seedDataPoints(tokens);
        }
        catch (RiskEngineApplicationException dfe)
        {
            Assert.fail("Rule.seedDataPoints() method exception: " + dfe);
        }
        RuleEngineProvider provider = new RuleEngineProvider(rule);
        IRuleEngine engine = provider.getEngine();

        Factoid result = engine.Run(); 
        Assert.assertTrue(result.getFact("output").value().toString().length() > 0); 
    }
}
