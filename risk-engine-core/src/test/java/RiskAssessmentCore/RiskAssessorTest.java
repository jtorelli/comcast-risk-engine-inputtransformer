package RiskAssessmentCore;

import RiskAssessmentCore.DroolsEngine.*;
import com.intersourcellc.DataType;
import com.intersourcellc.Fact;
import com.intersourcellc.Factoid;
import com.intersourcellc.exception.RiskEngineApplicationException;
import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.Resource;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatelessKnowledgeSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.zip.DataFormatException;

public class RiskAssessorTest
{
    static final Logger log = LoggerFactory.getLogger(RiskAssessorTest.class);

    // files locations: 
    private String drlTraversal = null;
    private String drlTypeCoercion = null; 
    private String drlGlobalFeedback = null; 
    private String knowledgeBaseSerialized = null;
    private String packagesSerialization = null; 
    

    public RiskAssessorTest() 
    {
        // DRL files: 
        drlTraversal = "target/classes/Traversal.drl";
        drlTypeCoercion = "target/classes/TypeCoercion.drl";
        drlGlobalFeedback = "target/classes/GlobalFeedback.drl";

        // serialized KB and packages:
        knowledgeBaseSerialized = "target/classes/KnowledgeBase.txt";
        packagesSerialization = "target/classes/Packages.txt";
    }
    
    // reserved for lexical analysis:
    @Test
    public void testLexicalAnalysis()
    {
        StringTokenizer tokenizer; 
        Vector tokens = null; 
        
        String expression = "( var1 + var2 * 3.5)/ 2.75 + var3 ;";
        //expression = " var1 + var2  * 3.5 / 2.75 + var3 ;";
        String input = expression.trim(); 
        
        // input length: 
        if (input.length() > 0)
        {
            // nesting balance 
            if (parenthesesBalanced(input))
            {
                tokenizer = new StringTokenizer(expression, "();+-*/", true);
                tokens = new Vector();
                if (tokenizer.countTokens() > 0)
                {
                    while (tokenizer.hasMoreTokens()) 
                    {
                        tokens.add(tokenizer.nextToken().trim());
                    }
                }
                else
                {
                    Assert.assertTrue(false, "No tokens found.");
                }
            }
            else
            {
                Assert.assertTrue(false, "Parentheses disbalance.");
            }
        }
        else
        {
            Assert.assertTrue(false, "No input.");
        }
        
        @SuppressWarnings({"null", "ConstantConditions"})
        String strTokens = tokens.toString(); 

    }
    
    // rserved for lexical analysis: 
    private TokenType GetTokenType(String token)
    {
        TokenType type = null; 
        
//        NONE,
        
//        LEFT, 
//        RIGHT,
//        PLUS, 
//        MINUS, 
//        MULTIPLY, 
//        DEVIDE, 
        
//        NUMERIC,
//        STRING, 
//        BOOLEAN
         
        return type; 
    }
    
    // rserved for lexical analysis: 
    private static boolean parenthesesBalanced(String s) 
    {
        int nesting = 0;
        for (int i = 0; i < s.length(); ++i)
        {
            char c = s.charAt(i);
            switch (c) {
                case '(':
                    nesting++;
                    break;
                case ')':
                    nesting--;
                    if (nesting < 0) {
                        return false;
                    }
                    break;
            }
        }
        return nesting == 0;
    }
    
    @Test
    public void testGetNormalizedRuleText()
    {
        // logic: 
        String logic = "numberOfMarketsAffected < 3; expectedDowntime = 0; numberOfCustomersAffected > 1000;estimatedWeeklyRevenue > 0;"; 
        // get conjuunction: 
        Conjunction conjunction = GetLogicFromStringInput(logic);
        // rule name: 
        String ruleName = "rule_name"; 
        // frame: 
        StringBuilder sb = new StringBuilder(); 
        sb.append("import com.intersourcellc.Factoid \n");
        sb.append("dialect \"mvel\" \n"); 
        sb.append("global com.intersourcellc.FeedBack feedback; \n");
        sb.append("rule "); 
        sb.append("\" "); 
        sb.append(ruleName); 
        sb.append("\"\n");         
        sb.append("when \n");         
        sb.append("\t$ff :\n");     
        
        for (Predicate pre : conjunction.GetPredicates())
        {
            sb.append("\tFactoid(getFact(\"");
            sb.append(pre.getName()); 
            sb.append("\"");
            sb.append(").value() ");
            sb.append(pre.getPredicatesign().Sign().equals("=") ? "==" : pre.getPredicatesign().Sign());
            sb.append(" \"");
            sb.append(pre.getValue()); 
            sb.append("\")\n");
        }        
        sb.append("then \n");   
        
        // trace template: 
        // feedback.addTrace($ff.getFact("f_int").value().toString());
        for (Predicate pre : conjunction.GetPredicates())
        {
            sb.append("\tfeedback.addTrace($ff.getFact(");
            sb.append("\"");
            sb.append(pre.getName());
            sb.append("\"");
            sb.append(").value().toString());\n ");
        } 
        // result(s) template: 
        //feedback.setBooleanResult(true); 
        sb.append("\tfeedback.setBooleanResult(true);\n "); 
        sb.append("end \n"); 
    
        String result = sb.toString(); 
        log.debug(result);
    }
    
    private Conjunction GetLogicFromStringInput(String input)
    {
        ArrayList<String> lines = GetLines(input); 
        ArrayList tokens;         
        Predicate term; 
        Conjunction conjunction = new Conjunction(); 
        
        for (String line : lines)
        {            
            tokens = GetTermTokens(line); 
            String sign; 
            if (tokens.size() == 3)
            {
                sign = tokens.get(1).toString(); 
                for (PredicateSign ps : PredicateSign.values())
                {
                    if (sign.equals(ps.Sign()))
                    {                        
                        term = new Predicate(tokens.get(0).toString(), ps, tokens.get(2).toString()); 
                        conjunction.AddPredicate(term);
                        break;
                    }                    
                }       
            }
        }
        return conjunction; 
    }
    
    private ArrayList<String> GetTermTokens(String line)
    {
        ArrayList<String> tokens = new ArrayList<>(); 
        //PredicatSign
        ArrayList<PredicateSign> predicate_signs = new ArrayList<>(Arrays.asList(PredicateSign.values()));
        
        String line_trimmed = line.trim(); 
        String sign; 
        int position = 0; 
        int position_found = 0; 
        int sign_length = 0; 
        int sign_length_max = 0; 
        
        for (PredicateSign ps : predicate_signs)
        {
            sign = ps.Sign(); 
            position = line_trimmed.indexOf(sign, 0);
            sign_length = sign.length(); 
            
            if (position > 0 && sign_length > sign_length_max)
            {
                sign_length_max = sign_length; 
                position_found = position; 
            }
        }
        
        if (position_found > 0 && sign_length_max > 0)
        {
            tokens.add(line_trimmed.substring(0, position_found).trim());
            tokens.add(line_trimmed.substring(position_found, position_found + sign_length_max));
            tokens.add(line_trimmed.substring(position_found + sign_length_max).trim());
        }
        
        return tokens; 
    }
    
    @Test
    public void testGetFactoidFromStringInput()
    {
        String input = "int estimatedWeeklyRevenue = 200000;int numberOfCustomersAffected = 300000;int numberOfMarketsAffected = 1;int expectedDowntime = 0;";
        Factoid fctd = GetFactoidFromStringInput(input); 
        
        String factoidText = fctd.toString();
        log.debug(factoidText);
        Assert.assertTrue(factoidText != "" && fctd.getFacts().size() == 4, "Factoid created.");
    }
    
    public Factoid GetFactoidFromStringInput(String input)
    {
        ArrayList<String> lines = GetLines(input); 
        String regexp = "[ \\s,;=\\n\\t]+";
        
        String[] tokens; 
        Factoid ftd = new Factoid(); 
        Fact fct; 
        
        for (String line : lines)
        {            
            tokens = line.split(regexp); 
            
            if (tokens.length == 3)
            {
                DataType dt = getEnumFromString(DataType.class, tokens[0]);
                if (dt != null)
                {                    
                    // check types: 
                    try
                    {
                        fct = new Fact(tokens[1], tokens[2], dt); 
                        if (fct.value() != null) 
                        {
                            ftd.addFact(fct);
                        }
                    }
                    catch (RiskEngineApplicationException ex) { }
                }         
            }
        }
        return ftd; 
    }
    
    private static ArrayList<String> GetLines(String input)
    {
        ArrayList<String> lines = new ArrayList();
        String regexp = "[;\\n]+";
        
        String[] output = input.trim().split(regexp);
        
        if (output.length > 0)
        {
            // translate lines: 
            for (int i = 0; i < output.length; i++)
            {
                //lines.add(output[i].trim() + ";");
                lines.add(output[i].trim());
            }
        }
        return lines; 
    }
    
    public static <T extends Enum<T>> T getEnumFromString(Class<T> c, String string)
    {
        if( c != null && string != null )
        {
            try
            {
                return Enum.valueOf(c, string.trim().toUpperCase());
            }
            catch(IllegalArgumentException ex)
            {
            }
        }
        return null;
    }
    
    @Test
    public void testDRLFromString()
    {
        //TODO: Drools probably shouldn't be calling System.out.println directly. Can we add SLF4J? (rmb)
        // get text of rule(s): 
        String rulesText = "import RiskAssessmentCore.DroolsEngine.Message rule \"Hello World 2\" when message:Message (type==\"Test\") then System.out.println(\"Test, Drools!\"); end";
        // create KB and session: 
        StatelessKnowledgeSession session = CreateStatelessSession(rulesText); 
        // create object (factoid) to insert:         
        Message msg = new Message();
        // set attributes: 
        msg.setType("Test");
        // run KB against factoid: 
        session.execute(msg); 
        // second run: 
        session.execute(msg);
        // third run: 
        session.execute(msg); 
    }
    
    private StatelessKnowledgeSession CreateStatelessSession(String rulesText)
    {
        KnowledgeBuilder kBuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
        
        Resource myResource = ResourceFactory.newReaderResource((Reader) new StringReader(rulesText));
        try
        {
            kBuilder.add(myResource, ResourceType.DRL);
        }
        catch (Exception ex)
        { 
            Assert.assertTrue(false, "DRL compilation file failed: " + ex.getMessage());
        } 
        // Create the Knowledge Base:
        KnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();
        // Transfer compiled Knowledge Packages:
        kbase.addKnowledgePackages( kBuilder.getKnowledgePackages() );
        // create and hold session: 
        return kbase.newStatelessKnowledgeSession();
    }
}