package com.intersourcellc.exception;

/**
 * Created by Ryan on 8/1/13.
 */
public class RiskEngineApplicationException extends RiskEngineException {
    public RiskEngineApplicationException() { super(); }
    public RiskEngineApplicationException(String message) { super(message); }
    public RiskEngineApplicationException(String message, Throwable cause) { super(message, cause); }
    public RiskEngineApplicationException(Throwable cause) { super(cause); }
}
