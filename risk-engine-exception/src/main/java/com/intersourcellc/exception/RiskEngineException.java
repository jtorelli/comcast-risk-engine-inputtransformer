package com.intersourcellc.exception;

/**
 * Created by Ryan on 8/1/13.
 */
public class RiskEngineException extends RuntimeException {
    public RiskEngineException() { super(); }
    public RiskEngineException(String message) { super(message); }
    public RiskEngineException(String message, Throwable cause) { super(message, cause); }
    public RiskEngineException(Throwable cause) { super(cause); }
}
