package com.intersourcellc.exception;

/**
 * Created by Ryan on 8/1/13.
 */
public class RiskEngineScriptException extends RiskEngineException {
    public RiskEngineScriptException() { super(); }
    public RiskEngineScriptException(String message) { super(message); }
    public RiskEngineScriptException(String message, Throwable cause) { super(message, cause); }
    public RiskEngineScriptException(Throwable cause) { super(cause); }
}
